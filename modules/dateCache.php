<?php

class kore_dateCache extends kore_date
{
    /**
     * Crée un objet date de toute pièce.
     * Note : l'ordre des paramètres est le même que celui de mktime.
     *
     * @param  $hour
     * @param  $minute
     * @param  $second
     * @param  $month
     * @param  $day
     * @param  $year
     * @return kore_dateCache
     */
    public static function make($hour, $minute, $second, $month, $day, $year)
    {
        static $_cache = array();
        $idCache = "$hour:$minute:$second/$month:$day:$year";
        if (isset($_cache[$idCache]))
            return $_cache[$idCache];

        $obj = parent::make($hour, $minute, $second, $month, $day, $year);

        $_cache[$idCache] = $obj;
        return $obj;
    }


    /**
     * Crée un objet date à partir d'un timestamp Unix.
     *
     * @param  $date integer
     * @return kore_dateCache
     */
    public static function fromTimestamp($date)
    {
        static $_cache = array();
        $idCache = $date;
        if (isset($_cache[$idCache]))
            return $_cache[$idCache];

        $obj = parent::fromTimestamp($date);

        $_cache[$idCache] = $obj;
        return $obj;
    }


    /**
     * Crée un objet date à partir d'une date YYYY-MM-DD [HH:MM:SS].
     * Si la date fournie est NULL, alors retourne NULL.
     *
     * @param  $date string
     * @return kore_dateCache
     */
    public static function fromSQL($date)
    {
        static $_cache = array();
        $idCache = $date;
        if (isset($_cache[$idCache]))
            return $_cache[$idCache];

        $obj = parent::fromSQL($date);

        $_cache[$idCache] = $obj;
        return $obj;
    }


    /**
     * Crée un objet date à partir du nombre de jour depuis la date de
     * référence Unix (01/01/1970).
     *
     * @param  $date integer
     * @return kore_dateCache
     */
    public static function fromDays($date)
    {
        static $_cache = array();
        $idCache = $date;
        if (isset($_cache[$idCache]))
            return $_cache[$idCache];

        $obj = parent::fromDays($date);

        $_cache[$idCache] = $obj;
        return $obj;
    }



    /**
     * Crée un objet date à partir d'une expression régulière
     * exemples d'expression avec sous-masques nommés :
     * #^(?<day>\d{1,2})/(?<month>\d{1,2})/(?<year>\d{2,4})$#
     * #^(?<day>\d{1,2})/(?<month>\d{1,2})/(?<year>\d{2,4})(?:\\s+(?<hour>\d{1,2}):(?<minute>\d{1,2})(?::(?<second>\d{1,2}))?)?$#
     *
     * Si l'expression régulière ne correspond pas, une exception sera
     * levée.
     * Si la date obtenue ne correspond pas à la date fournie, une
     * exception sera également levée, sauf si $ignoreErrors est à true.
     *
     * @param   string  $date
     * @param   string  $preg
     * @param   boolean $ignoreModification
     * @return  kore_dateCache
     * @throws  Exception
     */
    static public function fromReg($date, $preg, $ignoreModification = false)
    {
        static $_cache = array();
        $idCache = "$date|$preg|$ignoreModification";
        if (isset($_cache[$idCache]))
            return $_cache[$idCache];

        $obj = parent::fromReg($date, $preg, $ignoreModification);

        $_cache[$idCache] = $obj;
        return $obj;
    }

}
