<?php

class kore_db_mapping_auto
{
    static public function detectPHPType(kore_db_schema_column $column)
    {
        switch ($column->dataType){
            case 'tinyint':
                if (fnmatch('tinyint(1)*', $column->type))
                    return 'boolean';
            case 'int':
            case 'bigint':
            case 'mediumint':
            case 'smallint':
                return 'integer';

            case 'date':
            case 'datetime':
            case 'timestamp':
                return 'date';

            case 'char':
            case 'varchar':
            case 'varbinary':
            case 'tinytext':
            case 'text':
            case 'mediumtext':
            case 'longtext':
            case 'tinyblob':
            case 'blob':
            case 'mediumblob':
            case 'longblob':
                return 'string';

            default:
                return 'unknown';
        }
    }

    static public function detectProperties(kore_db_schema_table $table)
    {
        $props = array();

        $columns = $table->getColumns();
        foreach ($columns as $columnName => $column)
            $props[$columnName] = static::detectProperty($column);

        return $props;
    }

    static public function detectProperty(kore_db_schema_column $column)
    {
        $castType = self::detectPHPType($column);
        if ($castType === 'string' or $castType === 'unknown')
            $castType = false;

        $isKey = $column->isPrimaryKey;
        $read  = false;
        $write = false;

        if (!$isKey){
            foreach ($column->getTable()->getUniqueKeys() as $cols){
                if (isset($cols[$column->getName()])){
                    $isKey = true;
                    break;
                }
            }
        }

        if ($isKey){
            if ($column->getTable()->haveUniqueKeys())
                $read = '_load';
        } else {
            $read  = '_load';
            $write = true;
        }

        $p = kore_db_mapping_element::_newProperty($read, $write, $castType);

        return $p;
    }


}