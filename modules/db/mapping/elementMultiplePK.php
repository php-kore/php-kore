<?php
abstract class kore_db_mapping_elementMultiplePK extends kore_db_mapping_element
{
    /**
     * La clé doit impérativement être redéfinie : il s'agit de la liste des
     * colonnes, séparées par des virgules, *sans* espace.
     *
     * @var string
     */
    const PRIMARY_KEY = 'undefined';

    /**
     * Crée une instance de l'objet, à partir de sa clé. Cette clé étant
     * multiple, $id est nécessairement un tableau.
     *
     * @param  array  $id
     * @return kore_db_mapping_element
     */
    static public function fromId( $id )
    {
        $key = '';
        $keys = explode(',', static::PRIMARY_KEY);
        foreach ($keys as $colName){
            if (!isset($id[$colName]))
                throw new InvalidArgumentException("column $colName is mandatory to instanciate ".get_called_class());
            $key .= '|' . $id[$colName];
        }

        if (static::keptInMemory()) {
            $obj = static::_deduplicatedInstance($key, $isNewInstance);
        } else {
            $obj = new static;
            $isNewInstance = true;
        }

        if ($isNewInstance){
            $props = static::getProperties();
            foreach ($keys as $colName){
                if (!isset($props[$colName]))
                    throw new UnexpectedValueException("the column $colName from key is not defined in properties of ".get_called_class());

                $obj->_originalData[$colName] = $obj->_castToProperty(
                        $id[$colName], $props[$colName]->cast);
            }
        }

        return $obj;
    }

    /**
     * Retourne un tableau associatif contenant les clés de l'objet, afin de
     * manipuler la BDD.
     *
     * @return array
     */
    protected function _getPrimaryKey()
    {
        $pk = array();
        foreach (explode(',', static::PRIMARY_KEY) as $colName)
            $pk[$colName] = $this->_originalData[$colName];

        return $pk;
    }

    /**
     * Retourne la clé de l'objet (utilisée par exemple dans les collections).
     *
     * @return mixed
     */
    public function getKey()
    {
        $key = '';
        foreach (explode(',', static::PRIMARY_KEY) as $colName)
            $key .= $this->_originalData[$colName].'|';
        return $key;
    }

    /**
     * Tente de créer l'objet en base de données, à partir d'une requête SQL
     * fournie.
     *
     * @param  string  $query
     * @param  array   $params
     * @param  mixed   $useAutoIncrement indique si la valeur de l'autoIncrement doit être récupérée pour l'ID.
     * @return boolean
     */
    protected function _createSQL($query, $params, $useAutoIncrement)
    {
        if ($useAutoIncrement)
            throw new LogicException("multiple primary key can't be auto_increment");

        return parent::_createSQL($query, $params, false);
    }


}