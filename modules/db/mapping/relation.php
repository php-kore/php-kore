<?php
class kore_db_mapping_relation implements IteratorAggregate, Countable
{
    protected $_rels = array();
    protected $_preloaded = false;

    /**
     * Instancie l'objet.
     *
     * @param  PDOStatement $stmt
     * @param  string  $objectName
     * @param  mixed   $keys
     * @param  array   $cols
     */
    public function __construct(PDOStatement $stmt, $objectName, $keys, array $cols)
    {
        $keys = (array) $keys;
        $cols = (array) $cols;

        $fct = ( count($keys) > 1 ) ? 'fromKey' : 'fromId';
        $callback = array($objectName, $fct);

        while( ( $row = $stmt->fetch(PDO::FETCH_ASSOC) ) !== false ){
            $k = array();
            foreach( $keys as $key ) {
                $k[] = $row[$key];
                unset($row[$key]);
            }

            $l = new kore_db_mapping_relationHelper($callback, $k);
            $this->_rels[] = $l;
            foreach( $cols as $col ) {
                $l->$col = $row[$col];
                unset($row[$col]);
            }

            if( !empty($row) ) $l->_data = $row;
        }
    }

    /**
     * Initialise de suite tous les objets.
     */
    public function preload()
    {
        if( $this->_preloaded === true ) return;
        $this->_preloaded = true;

        foreach( $this->_rels as $rel )
            $rel->object = $rel->object;
    }

    /**
     * Convertie l'objet en courant en une collection d'objets.
     *
     * @return array
     */
    public function getCollection()
    {
        $list = array();
        foreach( $this->_rels as $rel ){
            $obj = $rel->object;
            $list[$obj->getKey()] = $obj;
        }
        return $list;
    }

    /**
     * Retourne un itérateur permettant à PHP de manipuler la classe comme un
     * tableau "normal".
     *
     * @return ArrayIterator
     */
    public function getIterator()
    {
        return new ArrayIterator($this->_rels);
    }

    /**
     * Retourne le nombre de relations présentes.
     *
     * @return integer
     */
    public function count()
    {
        return count($this->_rels);
    }

}

/**
 * Classe permettant à kore_db_mapping_relation de procéder aux instanciations
 * à la demande uniquement.
 */
class kore_db_mapping_relationHelper
{
    public $_callback;
    public $_keys;
    public $_data;

    public function __construct($callback, $keys)
    {
        $this->_callback = $callback;
        $this->_keys = $keys;
    }

    public function __get($name)
    {
        if( $name !== 'object' ) return null;

        $obj = call_user_func_array($this->_callback, $this->_keys);
        $obj->prefetchData($this->_data, true);
        return $obj;
    }
}
