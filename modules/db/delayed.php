<?php
/**
 * Module fournissant la classe { @link kore_db_delayed }.
 *
 * @package db
 */

/**
 * Classe de temporisation de traitement SQL : évite de se
 * connecter à la base de données pour des petits traitements
 * pouvant attendre.
 *
 * Si en fin de script il se trouve que la connexion a été
 * établie, alors le traitement sera lancé. Si toutefois ce
 * n'est pas le cas, alors les données relatives au traitement
 * seront stockées dans un fichier dédié et rechargées via
 * "cron", sous 2 minutes.
 *
 * @package db
 *
 */
abstract class kore_db_delayed
{
    /**
     * Profil DB dont dépend l'objet.
     * Cette propriété doit impérativement être surchargée.
     *
     * @var string
     */
    protected $neededDbProfil = NULL;

    /**
     * Données necessaires au traitement.
     *
     * @var mixed
     */
    public $data = NULL;

    protected $forceDelay = false;

    /**
     * Traitement dépendant d'un profil DB et ne devant
     * être exécuté que si celui ci existe.
     *
     * @param kore_db_pdo $db
     */
    abstract protected function execute( $db );


    /**
     * Crée un objet kore_db_delayed
     *
     * @param mixed  $data      données necessaires au traitement
     */
    public function __construct( $data = NULL )
    {
        if ($this->neededDbProfil === NULL)
            throw new LogicException('$neededDbProfil was not set in class '.get_class($this));

        if ($data !== NULL)
            $this->data = $data;
    }

    /**
     * Destructeur : si le profil DB est actif lance le traitement de suite,
     * sinon le stocke en fichier.
     */
    public function __destruct()
    {
        $bench = kore::$debug->benchInit('dbDelayed');
        if ( $this->data !== NULL ) {
            $execute = true;

            /**
             * Profil non actif, donc tente l'écriture disque.
             */
            if (! kore::$db->isActive($this->neededDbProfil) || $this->forceDelay ) {
                if( $this->writeToFile() ) {
                    $execute = false;
                    $debugStatus = 'writed';
                }
            }
            if( $execute === true ) {
                $db = kore::$db->instance($this->neededDbProfil);
                $this->execute($db);
                $debugStatus = 'executed';
            }
        }
        else $debugStatus = 'skipped';

        $bench->setFinalStatus($debugStatus);
    }

    /**
     * Force l'initialisation de la connexion à la DB lors du rechargement
     * de l'objet depuis le fichier ; afin que cette fois le destructeur
     * puisse lancer l'exécution.
     */
    protected function __wakeup()
    {
        $db = kore::$db->instance($this->neededDbProfil);
        $this->forceDelay = false;
    }

    /**
     * Parcourt tous les fichiers temporaires pour les ré-executer.
     */
    static public function browseFiles()
    {
        $filePath = kore::getStorePath('koreDbDelayed/current');

        $path = dirname($filePath);

		if( !($dir = opendir($path)) ) {
            /**
             * Si le dossier n'existe pas force sa création.
             */
            @mkdir($path, kore::$conf->storageDirMod, true);

		} else {
			while( ( $file = readdir($dir) ) !== false ) {
				if( $file === '.' or $file === '..' )
					continue;
				if( $file === 'current' )
					continue;
				if( fnmatch('old.*', $file) )
					self::redoFile($path.'/'.$file);
			}
			closedir($dir);
		}

		if( file_exists($filePath) )
			rename($filePath, $path.'/old.' . kore::getUniqPid());
    }

    /**
     * Recharge le contenu d'un fichier
     *
     * @param string  $filename
     */
    static protected function redoFile( $filename )
    {
        if( $fp = fopen($filename, 'rb') ) {
            while( !feof($fp) ) {
                /**
                 * Cette fonction devrait être appelée en cron, via
                 * CLI et donc avec un time_limit à 0. Toutefois dans
                 * le doute on prolonge le time limit avant chaque traitement.
                 */
                set_time_limit(30);

                $line = rtrim(fgets($fp, 32768), "\n");
                if( $line === '' ) continue;

                /**
                 * Se contente d'instancier l'objet. Le destructeur déclenchera
                 * l'exécution.
                 */
                $delayedObject = unserialize($line);
                $delayedObject = null;
            }
            fclose($fp);

            /**
             * Efface ce fichier, le contenu étant maintenant soit traité soit
             * à nouveau dans "current".
             */
            @unlink($filename);
        }
    }

    /**
     * Mémorise l'objet dans un fichier dédié, pour utilisation future.
     */
    protected function writeToFile()
    {
        static $fp = NULL;
        if( $fp === NULL ) {
            $filePath = kore::getStorePath('koreDbDelayed/current');

            $fp = @ fopen($filePath, 'ab');
            if (!$fp) {
                @mkdir(dirname($filePath), kore::$conf->storageDirMod, true);
                $fp = fopen($filePath, 'ab');
            }
        }

        if( ! $fp )
            $result = false;
        else {
            $data = serialize($this);
            $result = (bool) fwrite($fp, $data . "\n");
        }

        return $result;
    }

}
