<?php

class kore_db_schema_foreignKey
{
    static private $_cacheInstances  = array();

    /**
     *
     * @var kore_db_pdo
     */
    protected $_db;

    /**
     *
     * @var string
     */
    protected $_schema;


    /**
     *
     * @var string
     */
    protected $_name;


    protected $_originSchema;
    protected $_originTableName;
    protected $_originColumnsName = array();

    protected $_dependentSchema;
    protected $_dependentTableName;
    protected $_dependentColumnsName = array();

    /**
     * Instancie la classe actuelle, en s'assurant de ne pas créer de doublon.
     *
     * @param  kore_db_pdo $db
     * @param  string $schema
     * @param  string $name
     * @return kore_db_schema_foreignKey
     */
    static public function instanciate(kore_db_pdo $db, $schema, $name)
    {
        $idInstance = $db->identifier.':'.$schema.'.'.$name;
        if (isset(self::$_cacheInstances[$idInstance]))
            return self::$_cacheInstances[$idInstance];

        $obj = new static;
        self::$_cacheInstances[$idInstance] = $obj;

        $obj->_db     = $db;
        $obj->_schema = $schema;
        $obj->_name   = $name;

        return $obj;
    }

    /**
     * Purge le cache d'instance de PDO.
     */
    static public function purgeInstanceCache()
    {
        self::$_cacheInstances = array();
    }

    /**
     * Retourne le nom de la foreign key complet et protégé
     *
     * @return string
     */
    public function getFullName()
    {
        return $this->getSchema()->getFullName() .'.'.
                $this->_db->quoteKeyword($this->_name);
    }

    /**
     * Retourne le nom de la foreign key
     *
     * @return string
     */
    public function getName()
    {
        return $this->_name;
    }

    /**
     * Retourne l'instance du schéma correspondant.
     *
     * @return kore_db_schema_schema
     */
    public function getSchema()
    {
        return kore_db_schema_schema::instanciate($this->_db, $this->_schema);
    }

    /**
     * Initialise certaines variables, en fonction du contenu trouvé dans
     * INFORMATION_SCHEMA.KEY_COLUMN_USAGE.
     *
     * @param array $record
     */
    public function fetchProperties(array $record)
    {
        $this->_originSchema    = $record['REFERENCED_TABLE_SCHEMA'];
        $this->_originTableName = $record['REFERENCED_TABLE_NAME'];

        $this->_dependentSchema    = $record['TABLE_SCHEMA'];
        $this->_dependentTableName = $record['TABLE_NAME'];

        $this->_dependentColumnsName[$record['COLUMN_NAME']] =
                $record['REFERENCED_COLUMN_NAME'];

        $this->_originColumnsName[$record['REFERENCED_COLUMN_NAME']] =
                $record['COLUMN_NAME'];
    }

    /**
     * Retourne une instance de la table d'origine.
     *
     * @return kore_db_schema_table
     */
    public function getOriginTable()
    {
        return kore_db_schema_table::instanciate($this->_db,
                $this->_originSchema, $this->_originTableName);
    }

    /**
     * Retourne la liste des colonnes d'origine, avec le nom de la dépendance
     * pour clé.
     *
     * @return array
     */
    public function getOriginColumns()
    {
        $columns = array();
        $originTable = $this->getOriginTable();

        foreach ($this->_originColumnsName as $originColName => $dependentColName)
            $columns[$dependentColName] = $originTable->getColumn($originColName);

        return $columns;
    }

    /**
     * Retourne une instance de la table dépendante.
     *
     * @return kore_db_schema_table
     */
    public function getDependentTable()
    {
        return kore_db_schema_table::instanciate($this->_db,
                $this->_dependentSchema, $this->_dependentTableName);
    }

    /**
     * Retourne la liste des colonnes dépendantes, avec le nom d'origine.
     *
     * @return array
     */
    public function getDependentColumns()
    {
        $columns = array();
        $dependentTable = $this->getDependentTable();

        foreach ($this->_dependentColumnsName as $dependentColName => $originColName)
            $columns[$originColName] = $dependentTable->getColumn($dependentColName);

        return $columns;
    }

    /**
     * Retourne la liste des colonnes.
     *
     * @return array
     */
    public function getColumns()
    {
        $columns = array();
        $originTable = $this->getOriginTable();
        $dependentTable = $this->getDependentTable();

        foreach ($this->_originColumnsName as $originColName => $dependentColName){
            $col = new stdClass;
            $col->origin    = $originTable->getColumn($originColName);
            $col->dependent = $dependentTable->getColumn($dependentColName);

            $columns[] = $col;
        }

        return $columns;
    }

}