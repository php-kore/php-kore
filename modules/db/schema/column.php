<?php

class kore_db_schema_column
{
    static private $_cacheInstances  = array();

    /**
     *
     * @var kore_db_pdo
     */
    protected $_db;

    /**
     *
     * @var string
     */
    protected $_schema;

    /**
     *
     * @var string
     */
    protected $_tableName;


    /**
     *
     * @var string
     */
    protected $_name;


    /**
     * Instancie la classe actuelle, en s'assurant de ne pas créer de doublon.
     *
     * @param  kore_db_pdo $db
     * @param  string $schema
     * @param  string $tableName
     * @param  string $columnName
     * @return kore_db_schema_column
     */
    static public function instanciate(kore_db_pdo $db, $schema, $tableName, $columnName)
    {
        $idInstance = $db->identifier.':'.$schema.'.'.$tableName.':'.$columnName;
        if (isset(self::$_cacheInstances[$idInstance]))
            return self::$_cacheInstances[$idInstance];

        $obj = new static;
        self::$_cacheInstances[$idInstance] = $obj;

        $obj->_db        = $db;
        $obj->_schema    = $schema;
        $obj->_tableName = $tableName;
        $obj->_name      = $columnName;

        return $obj;
    }

    /**
     * Purge le cache d'instance de PDO.
     */
    static public function purgeInstanceCache()
    {
        self::$_cacheInstances = array();
    }

    /**
     * Instancie une colonne, à partir du contenu trouvé dans
     * INFORMATION_SCHEMA.COLUMNS.
     *
     * @param  kore_db_pdo $db
     * @param  array $record
     * @return kore_db_schema_column
     */
    static public function fetch(kore_db_pdo $db, array $record)
    {
        $schema     = $record['TABLE_SCHEMA'];
        $tableName  = $record['TABLE_NAME'];
        $columnName = $record['COLUMN_NAME'];

        $res = static::instanciate($db, $schema, $tableName, $columnName);
        $res->fetchProperties($record);
        return $res;
    }

    /**
     * Retourne le nom de la colonne complet et protégé
     *
     * @return string
     */
    public function getFullName()
    {
        return $this->getTable()->getFullName() .'.'.
                $this->_db->quoteKeyword($this->_name);
    }

    /**
     * Retourne le nom de la colonne
     *
     * @return string
     */
    public function getName()
    {
        return $this->_name;
    }

    /**
     * Retourne l'instance de la table correspondante.
     *
     * @return kore_db_schema_table
     */
    public function getTable()
    {
        return kore_db_schema_table::instanciate($this->_db, $this->_schema,
                $this->_tableName);
    }

    /**
     * Initialise certaines variables, en fonction du contenu trouvé dans
     * INFORMATION_SCHEMA.COLUMNS.
     *
     * @param array $record
     */
    public function fetchProperties(array $record)
    {
        foreach ($record as $property => $value){
            switch ($property){
                case 'COLUMN_KEY':
                    $this->isPrimaryKey = ($value === 'PRI');
                    break;
                case 'COLUMN_DEFAULT':
                    $this->default = $value;
                    break;
                case 'IS_NULLABLE':
                    $this->nullable = ($value === 'YES');
                    break;

                case 'COLUMN_TYPE':
                    $this->type = $value;
                    break;
                case 'DATA_TYPE':
                    $this->dataType = $value;
                    break;

                case 'CHARACTER_MAXIMUM_LENGTH':
                    $this->txtMaxChar = $value;
                    break;
                case 'CHARACTER_MAXIMUM_LENGTH':
                    $this->txtMaxBytes = $value;
                    break;
                case 'CHARACTER_SET_NAME':
                    $this->charset = $value;
                    break;
                case 'COLLATION_NAME':
                    $this->collation = $value;
                    break;

                case 'NUMERIC_PRECISION':
                    $this->numPrecision = $value;
                    break;
                case 'NUMERIC_SCALE':
                    $this->numScale = $value;
                    break;

                case 'DATETIME_PRECISION':
                    $this->datePrecision = $value;
                    break;

                case 'COLUMN_COMMENT':
                    $this->comment = $value;
                    break;
            }
        }
    }

}