<?php

class kore_db_schema_schema
{
    static private $_cacheInstances  = array();

    /**
     *
     * @var kore_db_pdo
     */
    protected $_db;

    /**
     *
     * @var string
     */
    protected $_name;


    /**
     * Instancie la classe actuelle, en s'assurant de ne pas créer de doublon.
     *
     * @param  kore_db_pdo $db
     * @param  string $name
     * @return kore_db_schema_schema
     */
    static public function instanciate(kore_db_pdo $db, $name)
    {
        $idInstance = $db->identifier.':'.$name;
        if (isset(self::$_cacheInstances[$idInstance]))
            return self::$_cacheInstances[$idInstance];

        $obj = new static;
        self::$_cacheInstances[$idInstance] = $obj;

        $obj->_db     = $db;
        $obj->_name   = $name;

        return $obj;
    }

    /**
     * Purge le cache d'instance de PDO.
     */
    static public function purgeInstanceCache()
    {
        kore_db_schema_table::purgeInstanceCache();

        self::$_cacheInstances = array();
    }

    /**
     * Détecte le schéma courant d'une instance de PDO.
     *
     * @param  kore_db_pdo $db
     * @return kore_db_schema_schema
     */
    static public function fromCurrent(kore_db_pdo $db)
    {
        if (!($row = $db->selectFirst("select SCHEMA() as `schema`")))
            return null;

        $schema = $row['schema'];
        return static::instanciate($db, $schema);
    }

    /**
     * Retourne la liste de tous les schemas disponibles.
     *
     * @param  kore_db_pdo $db
     * @return array
     */
    static public function getAll(kore_db_pdo $db)
    {
        $query =
               "SELECT *
                FROM information_schema.schemata
                ORDER BY schema_name";

        $list = array();
        if ($res = $db->query($query)){
            foreach ($res as $row){
                $schema = static::instanciate($db, $row['SCHEMA_NAME']);
                $list[$schema->getName()] = $schema;
            }
        }

        return $list;
    }

    /**
     * Retourne le nom du schema complet et protégé
     *
     * @return string
     */
    public function getFullName()
    {
        return $this->_db->quoteKeyword($this->_name);
    }

    /**
     * Retourne le nom du schema
     *
     * @return string
     */
    public function getName()
    {
        return $this->_name;
    }

    /**
     * Retourne une instance d'une table
     *
     * @return kore_db_schema_table
     */
    public function getTable($name)
    {
        return kore_db_schema_table::instanciate($this->_db, $this->_name,
                $name);
    }

    /**
     * Retourne la liste des tables
     *
     * @return array
     */
    public function getTables()
    {
        $query =
               "SELECT *
                FROM information_schema.tables
                WHERE table_schema = :schema
                ORDER BY table_name";

        $params = array(
                'schema' => $this->_name
                );

        $list = array();
        if ($res = $this->_db->q($query, $params)){
            foreach ($res as $row){
                $table  = kore_db_schema_table::instanciate($this->_db,
                        $this->_name, $row['TABLE_NAME']);
                $list[$table->getName()] = $table;
            }
        }

        return $list;
    }




}