<?php
/**
 * Class to represent database sorting rules, with some sanity checks if needed.
 *
 * @package db
 */
class kore_db_sort
{
    protected $_sql = '';

    /**
     * Instanciate a sort object from a mixed source. The best appropriate
     * factory will be used.
     *
     * @param  mixed $input
     * @param  kore_db_pdo $db
     * @throws InvalidArgumentException
     * @return kore_db_sort
     */
    static public function fromMixed($input, kore_db_pdo $db)
    {
        /*
         * We do not try to modified existing instance of kore_db_sort
         */
        if ($input instanceOf kore_db_sort)
            return $input;

        /*
         * Handle null, false, empty strings and empty arrays
         */
        if (empty($input))
            return new static();

        if (is_string($input))
            return static::fromString($input, $db);
        if (is_array($input))
            return static::fromArray($input, $db);

        throw new InvalidArgumentException("unable to instanciate kore_db_sort from that kind of input");
    }

    /**
     * Instanciate a sort object from a string. Some sanity checks will be done
     * before accepting the string, and an exception will be throws in case of
     * error.
     *
     * @param  string $string
     * @param  kore_db_pdo $db
     * @throws InvalidArgumentException
     * @return kore_db_sort
     */
    static public function fromString($string, kore_db_pdo $db = null)
    {
        $sort = new static;

        if (empty($string))
            return $sort;

        $sql = '';

        /*
         * If the kore_db_pdo connection is not available, we can only accept
         * single word syntax.
         */
        $word  = '[a-zA-Z0-9_]+';
        if (!$db) {
            $preg = "#^($word)?$word\$#Su";
        } else {
            $words = '[a-zA-Z0-9_ \\-]+';
            $o = preg_quote($db->openKeyword, '#');
            $c = preg_quote($db->closeKeyword, '#');
            $words = "{$o}$words{$c}";

            $preg = "#^(($words|$word)\\.)?($words|$word)\$#Su";
        }

        /*
         * Check each sort parameter
         */
        foreach (explode(',', $string) as $sortParam){

            $tmp = explode(' ', trim($sortParam));
            $colName = $tmp[0];

            if (!preg_match($preg, $colName))
                throw new InvalidArgumentException("invalid character in [$colName]");

            if (isset($tmp[1])) {
                $order = strtoupper($tmp[1]);
                if(!in_array($order, array('ASC', 'DESC')))
                    throw new InvalidArgumentException("can't sort columns in order [$order]");
            } else {
                $order = NULL;
            }

            if ($sql !== '')
                $sql .= ', ';
            $sql .= $colName;
            if ($order)
                $sql .= ' '.$order;
        }

        $sort->_sql = 'ORDER BY ' . $sql;
        return $sort;
    }

    /**
     * Instanciate a sort object from an array. Two kind of array accepted :
     * [ 'col1', 'col2'] or [ 'col1' => 'asc', 'col2' => 'desc' ]
     *
     * You can also mix both : [ 'col1', 'col2' => 'desc' ]
     *
     * @param  array $array
     * @param  kore_db_pdo $db
     * @throws InvalidArgumentException
     * @return kore_db_sort
     */
    static public function fromArray($array, kore_db_pdo $db)
    {
        $sort = new static;

        if (empty($array))
            return $sort;

        $sql = '';
        foreach ($array as $colName => $order){
            if (is_int($colName)){
                $colName = $order;
                $order = 'ASC';

            } else {
                $order = strtoupper($order);

                if (!in_array($order, array('ASC', 'DESC')))
                    throw new InvalidArgumentException("can't sort columns in order [$order]");
            }

            if ($pos = strpos($colName, '.'))
                $colName = $db->quoteKeyword(substr($colName, $pos)).'.'.
                        $db->quoteKeyword(substr($colName, $pos+1));
            else
                $colName = $db->quoteKeyword($colName);

            if ($sql !== '')
                $sql .= ', ';
            $sql .= $colName.' '.$order;
        }

        $sort->_sql = 'ORDER BY ' . $sql;
        return $sort;
    }

    /**
     * Instanciate a sort object from a string without applying any
     * security/sanity check or filter. *You* have to handle it.
     *
     * @param  string $string
     * @return kore_db_sort
     */
    static public function fromRawString($string)
    {
        $obj = new static;
        $obj->_sql  = $string;

        return $obj;
    }

    /**
     * Return the string version of this sort parameters.
     *
     * @return string
     */
    public function __toString()
    {
        return $this->_sql;
    }
}