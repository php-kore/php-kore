<?php

class kore_db_lock
{
    protected $_uid;

    protected $_db;
    protected $_idLock;

    private $_weLocked;
    private $_debugMsg;

    public function __construct(kore_db_pdo $db, $idLock)
    {
        static $uid = 0;
        $this->_uid = '#'.++$uid;

        if ($db->getAttribute(PDO::ATTR_PERSISTENT))
            throw new LogicException("kore_db_lock n'est pas compatible avec les connexions persistantes ({$db->identifier})");

        $this->_db = $db;
        $this->_idLock = $idLock;
    }

    public function __destruct()
    {
        if( $this->_weLocked ){
            /**
             * Aïe, fin de traitement sans libérer le verrou !?
             */
            kore::$error->track("destructeur appelé alors que le verrou n'a pas été libéré [".$this->getName()."]");
            $this->unlock();
        }
    }

    /**
     * Retourne le nom du verrou.
     *
     * @return string
     */
    public function getName()
    {
        return $this->_idLock . $this->_uid;
    }

    /**
     * Indique si le verrou est actuellement posé par nous.
     *
     * @return boolean
     */
    public function isLockedByUs()
    {
        return $this->_weLocked;
    }

    /**
     * Indique si le verrou est actuellement posé par quelqu'un d'autre.
     *
     * @return boolean
     */
    public function isLocked( $debugMsg = null )
    {
        $query = "select is_used_lock(?) as id, connection_id() as ourid";
        $params = array($this->_idLock);

        $locked = false;
        if( $row = $this->_db->selectFirst($query, $params, PDO::FETCH_OBJ) ){
            $locked = ( $row->id !== null and $row->id !== $row->ourid );
            $weLock = ( $row->id !== null and $row->id === $row->ourid );

            if ($this->_weLocked and !$weLock)
                throw new UnexpectedValueException("kore_db_lock : verrou perdu [".$this->getName()."]");

            if (!$this->_weLocked and $weLock){
                $this->_weLocked = true;
                $this->_debugMsg .= 'auto?:' . $debugMsg;
                kore::$error->track("verrou déjà posé par notre propre connexion [".$this->getName()."]");
            }
        }

        return $locked;
    }

    /**
     * Ping régulièrement la connexion afin de ne pas perdre le verrou.
     */
    public function ping( $debugMsg = null )
    {
        if ($debugMsg === null)
            $debugMsg = '->ping()';
        $this->isLocked($debugMsg);

        return $this->isLockedByUs();
    }

    /**
     * Tente de poser le verrou.
     *
     * @param  integer $timeout timeout, en secondes
     * @param  integer $expireTimeout temps d'inactivité après lequel
     *                                       MySQL coupera la connexion.
     * @param  string  $debugMsg message utilisé lors du lock()
     * @return boolean
     */
    public function lock( $timeout = 5, $expireTimeout = 3600, $debugMsg = null )
    {
        if( $this->_weLocked ) return true;

        /**
         * MySQL relachant le verrou lorsqu'on essaye de poser un second
         * verrou, on empêche se comportement à notre niveau en posant
         * un "verrou" dans la kore_db_pdo.
         */
        if( isset($this->_db->dbLock) ){
            $msg = "tentative de pose du verrou [".$this->getName()."] alors qu'on possède déjà le verrou [{$this->_db->dbLock}]";
            if ($this->_debugMsg)
                $msg .= ' (debug: '.$this->_debugMsg.')';

            kore::$error->track($msg);
            return false;
        }

        $query = "select get_lock(?, ?) as result";
        $params = array($this->_idLock, $timeout);

        if( $row = $this->_db->selectFirst($query, $params, PDO::FETCH_OBJ) ){
            $this->_weLocked = (bool) $row->result;
            if( $this->_weLocked ) {
                $this->_db->dbLock = $this->getName();
                $this->_debugMsg = $debugMsg;
                $this->_db->exec('set wait_timeout := ' . (int) $expireTimeout);
            }
        }

        return $this->_weLocked;
    }

    /**
     * Libère le verrou.
     */
    public function unlock()
    {
        if( ! $this->_weLocked ) return;

        /**
         * Supprime le "verrou" posé sur kore_db_pdo
         */
        if( isset($this->_db->dbLock) ){
            if( $this->_db->dbLock === $this->getName() )
                unset($this->_db->dbLock);
            else {
                $msg = "demande d'unlock de [".$this->getName()."] alors qu'on semble posséder [{$this->_db->dbLock}]";
                if( $this->_debugMsg )
                    $msg .= ' (debug: '.$this->_debugMsg.')';

                kore::$error->track($msg);
            }
        }

        $this->_debugMsg = false;

        $query = "select release_lock(?) as result";
        $params = array($this->_idLock);

        $row    = $this->_db->selectFirst($query, $params, PDO::FETCH_OBJ);
        $result = (bool) $row->result;

        $this->_weLocked = !$result;

        return $result;
    }

}
