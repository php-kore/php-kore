<?php
/**
 * Audit des résultats PDO { @link kore_db_pdoAuditStatement }.
 *
 * @package db
 */


/**
 * Surcharge de l'objet PDOStatement créé par PDO afin de traiter un jeu de
 * résultat.
 *
 * @link kore_db_pdoAudit
 * @package db
 */
class kore_db_pdoAuditStatement extends PDOStatement
{
    protected $_id;
    protected $_parentClass;
    protected $_parentId;
    protected $_queryId;
    
    public $executionTime = 0;
    public $executionCount = 0;
    

    public $benchQueries = true;

    public $currentInputParameters;
    public $currentBench;

    /**
     * Constructeur, non appelable directement. Seul PDO est capable
     * d'instancier cette classe.
     */
    protected function __construct($parentClass, $parentId)
    {
        static $idInstance = 0;
        $this->_id = ++$idInstance;

        $this->_parentClass = $parentClass;
        $this->_parentId = $parentId;

        $this->_queryId = trim(preg_replace('#\\s+#', ' ', $this->queryString));
    }

    public function getId()
    {
        return $this->_id;
    }

    protected function _getPDOInfos()
    {
        return kore_db_pdoAudit::$channel[$this->_parentId];
    }

    protected function _callStaticPDO($function)
    {
        $callback = array($this->_parentClass, $function);
        $args = func_get_args();
        array_shift($args);

        return call_user_func_array($callback, $args);
    }

    protected function _benchQuery($function)
    {
        if (!$this->benchQueries)
            return null;

        $infos = $this->_getPDOInfos();

        $class = 'db_'.$infos->identifier.':'.$infos->id;

        $statement = $function.' '.kore_db_pdoAudit::buildBenchStatement(
                $this->queryString);

        return kore::$debug->benchInit($class, $statement);
    }



    /**
     * Execute une requête préparée.
     *
     * @param array $inputParameters
     * @return boolean
     */
    public function execute( $inputParameters = NULL )
    {
        $this->currentBench = $this->_benchQuery(__FUNCTION__);
        $this->currentInputParameters = $inputParameters;

        $startTime = kore::time();
        $result = parent::execute($inputParameters);
        $this->currentBench = null;
        if( $result !== false )
            $this->eventExecuted($startTime);

        return $result ;
    }


    public function eventExecuted($startTime)
    {
        $this->executionTime = bcsub(kore::time(), $startTime, 6);
        $this->executionCount++;

        $this->haveReturnResults = ( $this->columnCount() > 0 );
        $this->endOfFetchReached = false;

        $this->_callStaticPDO('logQuery', 'execute', $this->_id,
                $this->currentInputParameters, $this->executionTime);


/*        if( $this->haveReturnResults === false ){
            $this->rowCount = $this->_rowCount();
            $pdo->realLastInsertId();
            $this->queryEnd();
        } elseif( $pdo->getAttribute(PDO::ATTR_DRIVER_NAME) === 'mysql' and
                  $pdo->getAttribute(PDO::MYSQL_ATTR_USE_BUFFERED_QUERY) ) {
            $this->queryEnd();
        }
*/

    }


}

















/*




    /**
     * Retourne le nombre de lignes affectées par l'exécution de la requete
     *
     * @return integer
     * /
    public function rowCount()
    {
        return $this->rowCount;
    }

    private function _rowCount()
    {
        return parent::rowCount();
    }


    /**
     * Récupère une ligne depuis le jeu de résultats
     *
     * @link http://fr.php.net/manual/fr/function.PDOStatement-fetch.php
     *
     * @param integer $fetch_style
     * @param integer $cursor_orientation
     * @param integer $cursor_offset
     * @return mixed
     * /
    public function fetch( $fetch_style = NULL, $cursor_orientation = NULL, $cursor_offset = NULL )
    {
        if( $fetch_style === NULL and $cursor_orientation === NULL and $cursor_offset === NULL )
            $result = parent::fetch();
        elseif( $cursor_orientation === NULL and $cursor_offset === NULL )
            $result = parent::fetch($fetch_style);
        elseif( $cursor_offset === NULL )
            $result = parent::fetch($fetch_style, $cursor_orientation);
        else
            $result = parent::fetch($fetch_style, $cursor_orientation, $cursor_offset);

        if( $result === false )
            $this->fetchEnd();

        return $result ;
    }

    /**
     * Récupère un tableau contenant toutes les lignes du jeu de résultats
     *
     * @link http://fr.php.net/manual/fr/function.PDOStatement-fetchAll.php
     *
     * @param integer $fetch_style
     * @param integer $column_index
     * @param array   $ctor_args
     * @return mixed
     * /
    public function fetchAll( $fetch_style = NULL, $column_index = NULL, $ctor_args = NULL )
    {
        if( $fetch_style === NULL and $column_index === NULL and
                $ctor_args === NULL )
            $result = parent::fetchAll();
        elseif( $column_index === NULL and $ctor_args === NULL )
            $result = parent::fetchAll($fetch_style);
        elseif( $ctor_args === NULL )
            $result = parent::fetchAll($fetch_style, $column_index);
        else
            $result = parent::fetchAll($fetch_style, $column_index, $ctor_args);

        $this->fetchEnd();

        return $result ;
    }

    /**
     * Retourne les données depuis une colonne du jeu de résultats
     *
     * @param integer $column_number
     * @return string
     * /
    public function fetchColumn( $column_number = NULL )
    {
        if( $column_number === NULL )
            $result = parent::fetchColumn();
        else
            $result = parent::fetchColumn($column_number);

        if( $result === false )
            $this->fetchEnd();

        return $result ;
    }

    /**
     * Récupère la prochaine ligne et la retourne en tant qu'objet.
     *
     * @param string  $class_name
     * @param array   $ctor_args
     * @return object
     * /
    public function fetchObject( $class_name = NULL, $ctor_args = NULL )
    {
        if( $class_name === NULL and $ctor_args === NULL )
           $result = parent::fetchObject();
        elseif( $ctor_args === NULL )
           $result = parent::fetchObject($class_name);
        else
            $result = parent::fetchObject($class_name, $ctor_args);

        if( $result === false )
            $this->fetchEnd();

        return $result ;
    }

    /**
     * Appelée lorsque tous les enregistrements ont été récupérés
     * /
    protected function fetchEnd()
    {
        if( $this->endOfFetchReached )
            return;

        $this->endOfFetchReached = true;
        $this->queryEnd();
    }

    /**
     * Appelée lorsque la requête est considérée comme terminée
     * (le canal est sensé être libre, et on peut executer une autre
     * requête).
     * /
    protected function queryEnd()
    {
        if( $this->doAudit === false )
            return;

        $backupHandlerAudit = $this->pdoHandler->doAudit;
        $this->pdoHandler->doAudit = false;

        $alertSlow    = false;
        $alertExplain = false;

        if( ( $this->pdoHandler->slowQueries > 0 ) and
            ( $this->executionTime >= $this->pdoHandler->slowQueries ) ) {

            $alertSlow = true;
        }

        $doProfiling = ( $alertSlow or $this->forceProfiling );
        $doExplain   = ( $alertSlow or $this->forceExplain );

        $profilingResult = false;
        if( $doProfiling ) {
            $profilingResult = $this->pdoHandler->doProfiling();
        }
        $explainResult = false;
        if( $doExplain ) {
            $explainResult = $this->pdoHandler->doExplain( $this->queryString, $this->inputParameters );

            if( $explainResult['alert'] ){
                $explainResult['hash'] = crc32( $this->queryID .':'.
                                                $explainResult['alert'] );

                if( $explainResult['hash'] !== $this->skipExplainAlert )
                    $alertExplain = true ;
            }
        }

        if( $alertSlow or $alertExplain ) {
            $explainResult['list'] = PHP_EOL . $this->pdoHandler->formatExplain(
                $explainResult['list'] );

            if( $alertSlow and $alertExplain )
                $message = 'slow query with explain alert';
            elseif( $alertSlow )
                $message = 'slow query';
            else $message = 'explain alert';

            $context = array(
                'Execution Time' => $this->executionTime,
                'Original Query' => $this->queryString,
                'Original Input Params' => $this->inputParameters,
                );
            if( is_array( $profilingResult ) )
                foreach( $profilingResult as $key => $value )
                    $context[ 'Profiling '.$key ] = $value;
            if( is_array( $explainResult ) )
                foreach( $explainResult as $key => $value )
                    $context[ 'Explain '.$key ] = $value;

            kore::$error->trigger(kore_error::DEBUG, 'SQL-AUDIT', $message,
                    $context);
        }

        $this->pdoHandler->doAudit = $backupHandlerAudit;
        $this->forceProfiling = false;
        $this->forceExplain   = false;
    }

    public function __destruct()
    {
        if( $this->haveReturnResults and ! $this->endOfFetchReached ) {
            kore::$error->trigger( kore_error::DEBUG, 'SQL-AUDIT',
                "result was not entirely fetched",
                array('query' => $this->queryString, 'parameters' => $this->inputParameters) );
        } elseif( $this->executionNb === 0 ) {
            kore::$error->trigger( kore_error::DEBUG, 'SQL-AUDIT',
                "unused prepared statement",
                array('query' => $this->queryString) );
        }
    }
}


*/
