<?php

abstract class kore_parser {

    protected $config = array();

    public function __set($name, $value) {
        if (isset($this->_config[$name])) {
            $this->_config[$name] = $value;
        }
    }

    public function __get($name) {
        if (isset($this->_config[$name])) {
            return $this->_config[$name];
        }
    }

    public function setConfig(Array $config = array()) {
        foreach ($config as $k => $v) {
            if (isset($this->_config[$k])) {
                $this->_config[$k] = $v;
            }
        }
    }

    abstract public function parse($string, array $localConfig = array());

}