<?php
/**
 * This class allow easy usage of a service via a REST API, which use only JSON
 * to exchange data.
 *
 * @package API_REST
 */
class kore_api_restJson extends kore_api_rest
{
    /**
     * Init a kore_api_restJson object, adding dedicated headers.
     *
     * @see kore_api_rest::__construct()
     *
     * @param  $baseUrl
     */
    public function __construct($baseUrl)
    {
        parent::__construct($baseUrl);

        $this->setHeader('Accept', 'application/json');
        $this->_dataContentType = 'application/json';
    }

    /**
     * Fetch the result : we need to decode JSON.
     *
     * @param  kore_api_restResult $result
     * @return kore_api_restResult
     */
    protected function _fetchContents(kore_api_restResult $result)
    {
        /*
         * The parent may have to handle data decompression.
         */
        $result = parent::_fetchContents($result);

        /*
         * Do the JSON job.
         */
        $result->contents = json_decode($result->contents);

        return $result;
    }

    /**
     * Encode uploaded data in JSON.
     *
     * @param  mixed $data
     * @return string
     */
    protected function _encodeData($data)
    {
        /*
         * If no data was given, we don't try to convert it in JSON.
         */
        if ($data !== null)
            $data = json_encode($data);

        return parent::_encodeData($data);
    }

}
