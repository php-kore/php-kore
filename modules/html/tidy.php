<?php

class kore_html_tidy implements kore_template_filter {

    /**
     * Instance de Tidy
     * @var Tidy
     */
    private $_tidy;

    /**
     * Tampon d'erreurs
     * @var array
     */
    private $_errorBuffer = array();

    /**
     * Constructeur
     */
    public function __construct() {
        $this->_tidy = new Tidy();
    }

    /**
     * Procède à la validation du document
     *
     * @param kore_template_config $config
     * @param kore_template_context $context
     * @param string $contents
     */
    public function filter(kore_template_config $config,
    kore_template_context $context, $contents) {

        $tidyConfig = array(
            //'input-xml' => (FALSE !== (strpos($config->contentType, 'xml'))),
            'show-errors' => 6,
            'show-warnings' => TRUE,
        );
        $encoding = ($config->charset === 'UTF-8') ? 'utf8' : 'latin1';
        $this->_tidy->parseString($contents, $tidyConfig, $encoding);
        $this->_tidy->cleanRepair();
        if ($this->_tidy->errorBuffer) {
            /*
             * Le buffer d'erreurs de Tidy::diagnose() est plus complet que
             * celui de Tidy::cleanRepair(). À tel point que s'il n'y a pas
             * d'erreur, le buffer contient un message informant qu'il n'y a
             * pas d'erreur
             */
            $this->_tidy->diagnose();
            $e = trim($this->_tidy->errorBuffer);
            // On stocke le buffer sous forme de tableau
            $this->_errorBuffer['Tidy'] = explode(PHP_EOL, $e);

            // Erreur
            kore::$error->track("document non valide", null, $this->_errorBuffer);
            die($this->_formatContents($contents));
        }

        return $contents;
    }

    private function _formatContents($contents) {
        $contents = explode("\n", htmlspecialchars($contents));

        $output  = '<ol><li>';
        $output .= implode('</li><li>', $contents);
        $output .= '</li></ol>';
        return $output;
    }

}