<?php
/**
 * Execution de programme externe.
 *
 * @package exec
 */

/**
 * Execute un programme externe.
 *
 * Contrairement aux fonctions classiques (exec, passthru, system, etc.)
 * cette classe permet de controler l'environnement d'exécution du
 * programme, de gérer les flux (typiquement le flux d'entrée, le flux
 * de sortie ainsi que le flux d'erreur), et permet également l'execution
 * de programme en parallèle.
 *
 * @package exec
 */
class kore_exec
{
    /**
     * Numéro de "pipe" indiquant le flux d'entrée
     */
    const STDIN  = 0;

    /**
     * Numéro de "pipe" indiquant le flux de sortie.
     */
    const STDOUT = 1;

    /**
     * Numéro de "pipe" indiquant le flux d'erreur.
     */
    const STDERR = 2;

    /**
     * Intervalle d'actualisation utilisé par la méthode wait(), en
     * microsecondes.
     */
    const WAIT_USLEEP = 500;


    protected $_cmdLine;
    protected $_executionDir;
    protected $_envVars;
    protected $_descriptors;

    protected $_process = false;
    protected $_started = 0;
    protected $_exitCode = null;

    protected $_inputPipes  = false;
    protected $_outputPipes = false;

    public $pipes = array();
    public $contents = array();

    static public function escapeCmd( $string )
    {
        return escapeshellcmd($string);
    }

    static public function escapeArg( $string )
    {
        return escapeshellarg($string);
    }

    static public function makeCmdLine($cmdLine)
    {
        if (is_array($cmdLine)){
            $args = $cmdLine;
            $cmdLine = array_shift($args);
        } else {
            $cmdLine = (string) $cmdLine;
            $args = func_get_args();
            unset($args[0]);
        }

        foreach ($args as $arg)
            $cmdLine .= ' ' . escapeshellarg($arg);

        return $cmdLine;
    }

    public function __construct($cmdLine)
    {
        if (is_array($cmdLine)) {
            $cmdLine = self::makeCmdLine($cmdLine);
        } else {
            $args = func_get_args();
            $cmdLine = self::makeCmdLine($args);
        }

        $this->_cmdLine = $cmdLine;

        $this->setDescriptorFromPipe(self::STDIN, 'r');
        $this->setDescriptorFromPipe(self::STDOUT, 'w');
        $this->setDescriptorFromPipe(self::STDERR, 'w');
    }

    public function __destruct()
    {
        if ($this->_process !== false)
            $this->close();
    }

    public function getCmdLine()
    {
        return $this->_cmdLine;
    }

    public function changeExecutionDir($dir)
    {
        $this->_executionDir = realpath($dir);
    }

    public function clearEnvVars()
    {
        $this->_envVars = array();
    }

    public function copyEnvVars($vars)
    {
        if (!is_array($vars))
            $vars = func_get_args();
        foreach ($vars as $varname)
            $this->_envVars[$varname] = null;
    }

    public function setEnvVar($varname, $value)
    {
        $this->_envVars[$varname] = $value;
    }

    public function setEnvVars( array $vars)
    {
        foreach ($vars as $varname => $value)
            $this->_envVars[$varname] = $value;
    }

    public function removeEnvVars($vars)
    {
        if (!is_array($vars))
            $vars = func_get_args();
        if ($this->_envVars === null)
            $this->_envVars = array();
        foreach ($vars as $varname)
            if (array_key_exists($varname, $this->_envVars))
                unset($this->_envVars[$varname]);
    }

    public function getEnv()
    {
        if ($this->_envVars === null) return null;

        $env = array();
        foreach ($this->_envVars as $varname => $value)
            if ($value !== null)
                $env[$varname] = $value;
            elseif( isset($_ENV[$varname]) )
                $env[$varname] = $_ENV[$varname];
        return $env;
    }

    public function removeDescriptor( $descNumber )
    {
        if (array_key_exists($descNumber, $this->_descriptors))
            unset($this->_descriptors[$descNumber]);
    }

    public function copySystemDescriptors()
    {
        $this->_descriptors = array(
            self::STDIN => STDIN,
            self::STDOUT => STDOUT,
            self::STDERR => STDERR,
            );
    }

    public function setDescriptor( $descNumber, $descriptor )
    {
        if (is_resource($descriptor))
            $this->_descriptors[$descNumber] = $descriptor;
        else
            kore::$error->track(__FUNCTION__.": parameter \$descriptor must be a resource",
                    null, array(
                            'cmdline'     => $this->_cmdLine,
                            'descriptor'  => $descriptor));
    }

    public function setDescriptorFromPipe( $descNumber, $type )
    {
        $this->_descriptors[$descNumber] = array('pipe', $type);
    }

    public function setDescriptorFromFile( $descNumber, $fileName, $type )
    {
        $this->_descriptors[$descNumber] = array('file', $fileName,
                $type);
    }



    public function start()
    {
        if ($this->_process !== false)
            return false;

        ++$this->_started;
        $this->_exitCode = null;
        $this->pipes = array();

        $this->_process = proc_open($this->_cmdLine, $this->_descriptors,
                $this->pipes, $this->_executionDir, $this->getEnv());

        /*
         * Établi la liste des pipes à traiter.
         */
        $this->contents = array();
        $this->_inputPipes  = array();
        $this->_outputPipes = array();
        foreach ($this->_descriptors as $descNumber => $desc){
            /*
             * Seuls les pipes sont concernés.
             */
            if (!is_array($desc) or !$desc[0] === 'pipe')
                continue;

            $mode = ($desc[1]);

            /*
             * Pipes de sortie
             */
            if ($mode === 'w'){
                $this->_outputPipes[$descNumber] = $this->pipes[$descNumber];
                $this->contents[$descNumber] = false;

                /*
                 * Marque le flux comme non bloquant
                 */
                stream_set_blocking($this->pipes[$descNumber], 0);
            }

            /*
             * Pipes d'entrée
             */
            if ($mode === 'r'){
                $this->_inputPipes[$descNumber] = $this->pipes[$descNumber];
            }
        }

        if (!$this->_inputPipes)
            $this->_inputPipes = false;
        if (!$this->_outputPipes)
            $this->_outputPipes = false;


        return ($this->_process !== false);
    }

    public function close()
    {
        $this->closePipes();

        $result = proc_close($this->_process);
        $this->_process = false;
    }

    public function closePipes()
    {
        $this->closeInputPipes();

        if ($this->_outputPipes) foreach($this->_outputPipes as $pipe)
            fclose($pipe);
        $this->_outputPipes = false;

        $this->pipes = array();
    }

    public function getProcess()
    {
        if ($this->_process === false)
            return null;
        $process = (object) proc_get_status($this->_process);

        /*
         * Cette variable n'est valide qu'au premier appel, on doit donc la
         * stocker.
         */
        if ($process->exitcode !== -1)
            $this->_exitCode = $process->exitcode;

        return $process;
    }

    public function isRunning()
    {
        $this->flushOutputPipes();

        $process = $this->getProcess();
        if ($process === false)
            return false;
        return $process->running;
    }

    public function getReturnedCode()
    {
        if ($this->_started === 0)
            $this->start();
        $this->wait();
        return $this->_exitCode;
    }

    public function startAndWait()
    {
        $this->start();
        $this->wait();
    }

    /**
     * Ferme tous les pipes d'entrée utilisés par le processus.
     */
    public function closeInputPipes()
    {
        if (!$this->_inputPipes)
            return;

        foreach($this->_inputPipes as $descNumber => $pipe){
            fclose($pipe);
            unset($this->pipes[$descNumber]);
        }

        $this->_inputPipes = false;
    }

    /**
     * Lit les données potentiellement en attente sur les pipes.
     * À appeler régulièrement, sinon on peut se retrouver avec l'enfant bloqué
     * après avoir écrit 64Ko de données sur le pipe, en attente d'une prise en
     * compte par le parent.
     *
     * Les paramètres $tv_sec et $tv_usec peuvent être ajustés afin de préciser
     * un délai d'attente (@cf stream_select)
     *
     * @param  integer $tv_sec
     * @param  integer $tv_usec
     * @return integer
     */
    public function flushOutputPipes($tv_sec = 0, $tv_usec = 0)
    {
        $bytes = 0;
        do {
            /*
             * Construit la liste des flux à écouter.
             */
            if (!($read = $this->_outputPipes))
                break;
            $write = $except = array();

            $usable = stream_select($read, $write, $except, $tv_sec, $tv_usec);
            if ($usable === false or !$read)
                break;

            $readChars = 0;
            foreach ($read as $pipe){
                $buffer = fread($pipe, 8192);
                $len = strlen($buffer);

                $readChars += $len;

                if ($len > 0){
                    $bytes += $len;
                    $descNumber = array_search($pipe, $this->pipes, true);
                    if ($descNumber !== false)
                        $this->event_output($descNumber, $buffer);
                }
            }

            /*
             * Tant qu'il y a des caractères dans le buffer, on boucle.
             */
        } while ($readChars > 0);

        return $bytes;
    }

    /**
     * Méthode appelée lors de l'arrivée de données sur un pipe de sortie
     *
     * @param  integer $descNumber
     * @param  string  $data
     */
    protected function event_output($descNumber, $data)
    {
        $this->contents[$descNumber] .= $data;
    }

    public function wait()
    {
        /*
         * Si on avait des pipes d'entrée, les ferme.
         */
        $this->closeInputPipes();

        /*
         * Si on avait des pipes de sortie, on récupère leur sortie.
         */
        if ($read = $this->_outputPipes){
            $this->flushOutputPipes();

            foreach ($read as $descNumber => $pipe){
                stream_set_blocking($pipe, 1);
                $data = stream_get_contents($pipe);
                if ($data !== '' and $data !== false)
                    $this->event_output($descNumber, $data);
                fclose($pipe);
                unset($this->pipes[$descNumber]);
            }
            $this->_outputPipes = false;
        }

        $process = $this->getProcess();
        if ($process === null)
            return;

        if ($process->running) {
            if ($process->pid and function_exists('pcntl_waitpid')) {
                $status = null;
                pcntl_waitpid($process->pid, $status);

                if (pcntl_wifexited($status))
                    $this->_exitCode = pcntl_wexitstatus($status);

            } else {
                while (($process = $this->getProcess()) and
                        $process->running)

                    usleep(self::WAIT_USLEEP);
            }
        }
    }

    public function getOutput()
    {
        return @$this->contents[self::STDOUT];
    }

    public function getOutputErrors()
    {
        return @$this->contents[self::STDERR];
    }

}
