<?php
class kore_graph_engine
{
    var $taille = 300;
    var $legende = array();
    var $donnees = array();
    var $idGraph = 0;
    var $echelle = NULL;

    public function __construct()
    {
        $this->echelle = new stdClass();
        $this->echelle->ratio = 1;
        $this->echelle->borneMin = null;
        $this->echelle->borneMax = null;
        $this->echelle->min = null;
        $this->echelle->max = null;
        $this->echelle->intervale = array();
    }

    public function ajouteEtiquette( $cle, $etiquette )
    {
        if( ! isset( $this->legende[ $cle ] ) )
        {
            $this->legende[ $cle ] = new stdClass();
            $this->legende[ $cle ]->idGraph = ++$this->idGraph;
            $this->legende[ $cle ]->etiquette = $etiquette;
        }
        else $this->legende[ $cle ]->etiquette = $etiquette;
    }

    public function ajouteDonnee( $abscisse, $cle, $valeur )
    {
        if( ! isset( $this->donnees[ $abscisse ] ) )
            $this->donnees[ $abscisse ] = array();

        if( ! isset( $this->legende[ $cle ] ) )
            $this->ajouteEtiquette( $cle, '#'.$cle );

        $this->donnees[ $abscisse ][ $cle ] = $valeur;
    }

    protected function calculeBorne( $valeur )
    {
        $negative = false;
        if( $valeur == 0 )
            return 0;
        if( $valeur < 0 )
        {
            $negative = true;
            $valeur = $valeur * -1;
        }

        $echelle = pow( 10, floor( log10( $valeur ) ) );
        $facteur = ceil( $valeur / $echelle );
        $borne = $facteur * $echelle;

        if( $negative ) $borne = $borne * -1;

        return $borne;
    }

    protected function classeDonnees()
    {
        $defaultInterval = new stdClass();
        $defaultInterval->min = null;
        $defaultInterval->max = null;

        foreach( $this->legende as $cle => $etiquette )
        {
            $this->echelle->intervale[ $cle ] = clone $defaultInterval;
        }

        foreach( $this->donnees as $abscisse => $chiffres )
        {
            $classement = array();
            foreach( $this->legende as $cle => $etiquette )
            {
                if( isset( $chiffres[ $cle ] ) )
                {
                    $valeur = $chiffres[ $cle ];
                    $classement[ $cle ] = $valeur;

                    $interval = $this->echelle->intervale[ $cle ];

                    if( $interval->min === null or $interval->min > $valeur )
                        $interval->min = $valeur;

                    if( $interval->max === null or $interval->max < $valeur )
                        $interval->max = $valeur;
                }
            }

            $this->donnees[ $abscisse ] = $classement;
        }

        foreach( $this->echelle->intervale as $cle => $interval )
        {
            if( $interval->min === null )
            {
                unset( $this->echelle->intervale[ $cle ] );
            }
            else
            {
                if( $this->echelle->min === null or
                    $this->echelle->min > $interval->min )

                    $this->echelle->min = $interval->min;


                if( $this->echelle->max === null or
                    $this->echelle->max < $interval->max )

                    $this->echelle->max = $interval->max;
            }
        }

        $this->echelle->borneMin = $this->calculeBorne( $this->echelle->min );
        if( $this->echelle->borneMin > 0 ) $this->echelle->borneMin = 0;

        $this->echelle->borneMax = $this->calculeBorne( $this->echelle->max );
        if( $this->echelle->borneMin < 0 and $this->echelle->borneMax < 0 )
            $this->echelle->borneMax = 0;

        $interval = ( $this->echelle->borneMax - $this->echelle->borneMin );
        if( $interval == 0 ) $this->echelle->ratio = 1;
        else $this->echelle->ratio = ( $this->taille / $interval );
    }

    public function listeDonnees()
    {
        $this->classeDonnees();

        $graph = array();

        foreach( $this->donnees as $abscisse => $chiffres )
        {
            $graph[ $abscisse ] = array();

            foreach( $chiffres as $cle => $valeur )
            {
                $size = round( $valeur * $this->echelle->ratio );
                if( $size == 0 ) $size = 1;

                $data = new stdClass();
                $data->valeur  = $valeur;
                $data->idGraph = $this->legende[ $cle ]->idGraph;
                $data->size    = $size;

                $graph[ $abscisse ][ $cle ] = $data;
            }
        }

        return $graph;
    }

    public function listeLegende()
    {
        return $this->legende;
    }

}
