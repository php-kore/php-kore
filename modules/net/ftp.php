<?php
class kore_net_ftp extends kore_net_ftpAbstract
{
    /**
     * Chemin courant
     * @var string
     */
    protected $_currentPath = NULL;

    /**
     * Handler FTP
     * @var resource
     */
    protected $_handle = false;

    /**
     * Renvoit une instance de kore_net_ftp d'après une URL
     * @param  string $url
     * @return kore_net_ftp
     */
    public static function fromUrl( $url )
    {
        $ftp = parent::fromUrl($url);

        $url = parse_url($url);
        if (isset($url['path'])){
            $path = (string)substr($url['path'], 1);
            if ($path !== '') $ftp->chdir($path);
        }

        return $ftp;
    }

    /**
     * Constructeur
     * @param string $host
     * @param string $port
     * @param string $login
     * @param string $password
     */
    public function __construct($host = 'localhost', $port = 21,
                                $login = 'anonymous', $password = NULL)
    {
        parent::__construct($host, $port, $login, $password);

        if( $this->_connect() )
            $this->_connected = true;
    }

    /**
     * Définit les timeout
     * @param $timeout
     */
    public function setTimeout( $timeout )
    {
        $this->_timeout = (int) $timeout;

        if( $this->_handle !== false )
            ftp_set_option( $this->_handle, FTP_TIMEOUT_SEC, $this->_timeout );
    }

    /**
     * Établit la connexion
     */
    protected function _connect()
    {
        if (!$this->_connected) {
            kore::$error->clearLastError();

            $handle = @ ftp_connect( $this->_host, $this->_port, $this->_timeout );

            if( $handle === false ) {
                $this->_trackError( 'connect', kore::$error->getLastError(),
                        __FILE__, __LINE__ );
            } else {

                ftp_set_option( $handle, FTP_TIMEOUT_SEC, $this->_timeout );

                if( ! @ ftp_login( $handle, $this->_login, $this->_password ) ) {
                    $this->_trackError( 'login', kore::$error->getLastError(),
                            __FILE__, __LINE__ );
                    @ ftp_close( $handle );

                    $handle = false;

                } else {
                    $this->_handle = $handle;
                }

            }

            return ( $handle !== false );
        }
    }

    /**
     * Ferme la connexion
     */
    protected function _close()
    {
        $this->_currentPath = NULL;

        if( $this->_handle !== false ) {
            kore::$error->clearLastError();

            if( ! @ ftp_close( $this->_handle ) ) {
                $this->_trackError( 'close', kore::$error->getLastError(),
                        __FILE__, __LINE__ );
            } else {
                $this->_handle = false;
            }
        }
    }

    /**
     * Définit le mode passif/actif
     * @param boolean $enable
     */
    public function pasv( $enable )
    {
        $enable = (bool) $enable;

        if( $this->_pasv === $enable )
            return true;

        kore::$error->clearLastError();

        if ( ( $result = @ ftp_pasv( $this->_handle, $enable ) ) === false ) {
            $this->_trackError( 'pasv', kore::$error->getLastError(),
                    __FILE__, __LINE__ );
        } else {
            $this->_pasv = $enable;
        }

        return $result;
    }

    /**
     * Retourne le chemin courant sur le serveur distant
     */
    public function pwd()
    {
        if( $this->_currentPath !== NULL )
            return $this->_currentPath;

        kore::$error->clearLastError();

        if( ( $result = @ ftp_pwd( $this->_handle ) ) === false )
            $this->_trackError( 'pwd', kore::$error->getLastError(),
                    __FILE__, __LINE__ );
        else $this->_currentPath = $result;

        return $result;
    }

    /**
     * Change le répertoire courant sur le serveur distant
     * @param string $dirName
     */
    public function chdir( $dirName )
    {
        $this->_currentPath = NULL;

        kore::$error->clearLastError();

        if( ( $result = @ ftp_chdir( $this->_handle, $dirName ) ) === false )
            $this->_trackError( 'chdir', kore::$error->getLastError(),
                    __FILE__, __LINE__ );

        return $result;
    }

    /**
     * Remonte d'un  niveau dans la hiérarchie des fichiers
     */
    public function cdup()
    {
        $this->_currentPath = NULL;

        kore::$error->clearLastError();

        if( ( $result = @ ftp_cdup( $this->_handle ) ) === false )
            $this->_trackError( 'cdup', kore::$error->getLastError(),
                    __FILE__, __LINE__ );

        return $result;
    }

    /**
     * Crée un nouveau répertoire
     * @param string $dirName
     */
    public function mkdir( $dirName )
    {
        kore::$error->clearLastError();

        if( ( $result = @ ftp_mkdir( $this->_handle, $dirName ) ) === false )
            $this->_trackError( 'mkdir', kore::$error->getLastError(),
                    __FILE__, __LINE__ );

        return $result;
    }

    /**
     * Supprime un répertoire
     * @param $dirName
     */
    public function rmdir( $dirName )
    {
        kore::$error->clearLastError();

        if( ( $result = @ ftp_rmdir( $this->_handle, $dirName ) ) === false )
            $this->_trackError( 'rmdir', kore::$error->getLastError(),
                    __FILE__, __LINE__ );

        return $result;
    }

    /**
     * Renomme un fichier/répertoire
     * @param string $currentName
     * @param string $newName
     */
    public function rename( $currentName, $newName )
    {
        kore::$error->clearLastError();

        $result = @ ftp_rename( $this->_handle, $currentName, $newName );
        if( $result === false )
            $this->_trackError( 'rename', kore::$error->getLastError(),
                    __FILE__, __LINE__ );

        return $result;
    }

    /**
     * Supprime un fichier
     * @param string $fileName
     */
    public function delete( $fileName )
    {
        kore::$error->clearLastError();

        if( ( $result = @ ftp_rmdir( $this->_handle, $fileName ) ) === false )
            $this->_trackError( 'delete', kore::$error->getLastError(),
                    __FILE__, __LINE__ );

        return $result;
    }

    /**
     * Retourne la date de dernière modification d'un fichier (fileMTime)
     * @param string $fileName
     */
    public function time( $fileName )
    {
        kore::$error->clearLastError();

        if( ( $result = @ ftp_mdtm( $this->_handle, $fileName ) ) === false )
            $this->_trackError( 'pwd', kore::$error->getLastError(),
                    __FILE__, __LINE__ );

        return $result;
    }

    /**
     * Retourne la taille d'un fichier
     * @param stringf $fileName
     */
    public function size( $fileName )
    {
        kore::$error->clearLastError();

        if( ( $result = @ ftp_size( $this->_handle, $fileName ) ) === false )
            $this->_trackError( 'pwd', kore::$error->getLastError(),
                    __FILE__, __LINE__ );

        return $result;
    }

    /**
     * Modifie les permissions d'un fichier/répertoire
     * @param string $fileName
     * @param int $mode
     */
    public function chmod( $fileName, $mode )
    {
        kore::$error->clearLastError();

        $result = @ ftp_chmod( $this->_handle, $mode, $fileName );

        if( $result === false )
            $this->_trackError( 'pwd', kore::$error->getLastError(),
                    __FILE__, __LINE__ );

        return $result;
    }

    /**
     * Retourne le contenu d'un répertoire (par défaut, le répertoire courant)
     * @param string $dirName
     */
    public function rawlist( $dirName = NULL )
    {
//        if( $dirName === NULL ) $dirName = '.';

        if( strpos( $dirName, ' ' ) === false )
            $oldDir = NULL;
        else {
            $oldDir = $this->pwd();

            if( $oldDir === $dirName ) {
                $oldDir = NULL;
            } else {
                $this->chdir( $dirName );
            }

            $dirName = '.';
        }

        kore::$error->clearLastError();

        $result = @ ftp_rawlist( $this->_handle, $dirName );

        if( $result === false or $result === NULL )
            $this->_trackError( 'pwd', kore::$error->getLastError(),
                    __FILE__, __LINE__ );
        else
            $result = $this->_parseRawList( $result );

        if( $oldDir !== NULL ) $this->chdir( $oldDir );

        return $result ;
    }

    /**
     * Télécharge un fichier
     * @param string $remoteName
     * @param string $localName
     * @param string $mode
     * @param string $resumePos
     */
    public function get( $remoteName, $localName, $mode = FTP_BINARY,
            $resumePos = NULL )
    {
        kore::$error->clearLastError();

        $result = @ ftp_get( $this->_handle, $localName, $remoteName, $mode,
                $resumePos );

        if( $result === false )
            $this->_trackError( 'get', kore::$error->getLastError(),
                    __FILE__, __LINE__ );

        return $result;
    }

    /**
     * Exécute une commande SITE
     * @param string $command
     */
    public function site( $command )
    {
        kore::$error->clearLastError();

        if( ( $result = @ ftp_site( $this->_handle, $command ) ) === false )
            $this->_trackError( 'pwd', kore::$error->getLastError(),
                    __FILE__, __LINE__ );

        return $result;
    }

    /**
     * Exécute une commande FTP
     * @param string $command
     */
    public function exec( $command )
    {
        kore::$error->clearLastError();

        if( ( $result = @ ftp_exec( $this->_handle, $command ) ) === false )
            $this->_trackError( 'pwd', kore::$error->getLastError(),
                    __FILE__, __LINE__ );

        return $result;
    }

    /**
     * Maintient la connexion active
     */
    public function doIdle()
    {
        return ( $this->pwd() !== false );
    }

}
