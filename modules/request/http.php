<?php
/**
 * Gère le contexte d'exécution HTTP.
 *
 * @package request
 */

/**
 * Gère le contexte d'exécution HTTP.
 *
 * @package request
 */
class kore_request_http
{
    /**
     * Indique le protocole utilisé (http ou https).
     */
    protected static $_scheme;

    /**
     * Indique l'host complet.
     */
    protected static $_host;

    /**
     * Constructeur private pour empècher l'instanciation.
     */
    private function __construct()
    {
    }

    /**
     * Indique si la connexion est sécurisée.
     * Si $forceValue est renseignée, remplace la valeur retournée par
     * isSecured() et getScheme() ; particulièrement utile en cas de détection
     * erronée à cause de l'utilisation d'un reverse proxy par exemple.
     *
     * @param  boolean $forceValue
     * @return boolean
     */
    static function isSecured( $forceValue = NULL )
    {
        if ($forceValue !== NULL){
            if ($forceValue) {
                self::$_scheme = 'https';
                $_SERVER['SERVER_PORT'] = 443;
            } else {
                self::$_scheme = 'http';
                $_SERVER['SERVER_PORT'] = 80;
            }

            /**
             * Vide ce cache
             */
            self::$_host = NULL;
        }

        return ( self::getScheme() === 'https' );
    }

    /**
     * Retourne la page appelée (sans tenir compte du rewriting).
     *
     * @return string
     */
    static function getPage()
    {
        static $page = NULL;
        if( $page === NULL ) {
            $page = preg_replace(
                        array( '#(\\.php)/.*$#', '#/+(index\\.php)?#' ),
                        array( '$1', '/' ),
                        @ $_SERVER['PHP_SELF'] );
        }
        return $page ;
    }

    /**
     * Reconstruit l'URI de la page courante.
     *
     * @return string
     */
    static function getUri()
    {
        if( isset($_SERVER['REQUEST_URI']) )
            return $_SERVER['REQUEST_URI'];
        return self::getPage().'?'.http_build_query($_GET);
    }

    /**
     * Retourne l'URI de la page courante, débarassée de certains paramètres.
     *
     * @param  array  $exclude
     * @return string
     */
    static function filterUri( array $exclude )
    {
        $uri = self::getUri();
        $list = '';
        foreach( $exclude as $e ){
            if( $list !== '' ) $list .= '|';
            $list .= preg_quote($e);
        }
        return preg_replace("/&(?:$list)(?:=[^&#]+)?/", '', $uri);
    }

    /**
     * Reconstruit l'URL de la page courante.
     *
     * @return string
     */
    static function getUrl()
    {
        if( isset($_SERVER['REDIRECT_URL']) )
            return $_SERVER['REDIRECT_URL'];

        $url = self::getUri();
        if( ( $pos = strpos($url, '?') ) !== false )
            $url = substr($url, 0, $pos);

        return $url;
    }

    /**
     * Retourne le dossier dans lequel se trouve la page appelée.
     *
     * @return string
     */
    static function getDir()
    {
        static $dir = NULL;
        if( $dir === NULL ) {
            $dir = self::getPage();
            if( substr( $dir, -1 ) !== '/' ) {
                $dir = dirname( $dir ) ;
                if( substr( $dir, -1 ) !== '/' )
                    $dir .= '/';
            }
        }
        return $dir ;
    }

    /**
     * Retourne le protocole d'accès utilisé (http ou https).
     *
     * @return string
     */
    static function getScheme()
    {
        if( self::$_scheme === NULL ) {

            if(  ( isset( $_SERVER['HTTPS'] ) and $_SERVER['HTTPS'] === 'on' )
              or ( isset( $_SERVER['SERVER_PORT'] )  and
                   $_SERVER['SERVER_PORT'] == 443 ) )

                self::$_scheme = 'https';

            else self::$_scheme = 'http';
        }

        return self::$_scheme;
    }

    /**
     * Retourne l'host HTTP courant, si défini.
     *
     * @return string
     */
    static function getHTTPHost()
    {
        static $host = null;

        if ($host === null){
            if (isset($_SERVER['HTTP_HOST'])) {
                $host = $_SERVER['HTTP_HOST'];

                if (($pos = strpos($host, ':')) !== false)
                    $host = substr($host, 0, $pos);

            } elseif (isset($_SERVER['SERVER_NAME'])) {
                $host = $_SERVER['SERVER_NAME'];
            } else {
                $host = 'undefined';
            }

        }

        return $host;
    }

    /**
     * Retourne l'host complet du serveur : protocol + nom du serveur + port
     * (si necessaire).
     *
     * @return string
     */
    static function getHost()
    {
        if ( self::$_host === NULL ) {
            $scheme = self::getScheme();
            self::$_host = $scheme . '://' . static::getHTTPHost();

            if (isset( $_SERVER['SERVER_PORT'] ) ) {
                if( ( $scheme === 'http' and $_SERVER['SERVER_PORT'] != 80 )
                  or ( $scheme === 'https' and $_SERVER['SERVER_PORT'] != 443) )
                self::$_host .= ':' . $_SERVER['SERVER_PORT'];
            }
        }
        return self::$_host;
    }

    static function getUnsecuredHost()
    {
        return 'http://' . static::getHTTPHost();
    }

    static function getSecuredHost()
    {
        return 'https://' . static::getHTTPHost();
    }

    /**
     * Complète une URL par rapport au script courant.
     *
     * @param  string  $url
     * @param  boolean $addHost
     * @return string
     */
    static function completeURL( $url, $needHost = false )
    {
        $addHost = ( strpos($url, '://') === false );
        $addDir = ( $addHost and substr($url, 0, 1) !== '/' );

        if( $addDir )
            $url = self::getDir() . $url;
        if( $addHost and $needHost )
            $url = self::getHost() . $url;

        return $url;
    }

    /**
     * Build a token specific to the given $name. This method is deprecated, and
     * you should use the class kore_request_token instead.
     *
     * @deprecated
     * @param  string $name
     * @return string
     */
    static public function buildToken($name = null)
    {
        if ($name === null) {
            $name = self::getPage();
            kore::$error->track(__METHOD__ . " is deprecated, please use new kore_request_token(kore_request_http::getPage())");
        } else {
            kore::$error->track(__METHOD__ . " is deprecated, please use new kore_request_token('$name')");
        }

        $token = new kore_request_token($name);
        return (string) $token;
    }

}
