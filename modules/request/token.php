<?php
/**
 * The kore_request_token allow to build a temporary but repeatable token, to
 * reduce "cross-site request forgery".
 *
 * Please note that to really reduce CSRF you should use a salt specific to the
 * user (login, email, or session id for example).
 *
 * It can also be tuned thanks to this kore::$conf parameters :
 * - kore::$conf->token_default_saltSources
 * - kore::$conf->token_default_timeSlice
 * - kore::$conf->token_default_nbSlice
 * - kore::$conf->token_additionnalSalt
 */
class kore_request_token
{
   const USE_HOSTNAME = 1,        // Use server hostname
         USE_WEBDIR = 2,          // Use website "document root"
         USE_SERVER_NAME = 4,     // Use website "server name"
         USE_SESSIONS = 8,        // Use session id
         USE_IP = 16,             // Use client IP
         USE_USER_AGENT = 32;     // Use browser name

    /**
     * Sort of «namespace»
     *
     * @var string
     */
    protected $_baseValue;

    /**
     * Some random data to integrate in the salt.
     *
     * @var string
     */
    protected $_additionnalSalt;

    /**
     * List of data sources to populate the salt (bit mask).
     *
     * @var integer
     */
    protected $_saltSources;

    /**
     * Time (in seconds) after what the salt change
     *
     * @var integer
     */
    protected $_timeSlice;

    /**
     * Number of time slice which are accepted.
     *
     * @var integer
     */
    protected $_nbSlice;

    /**
     * Internal cache to speed up the _getSalt() process, if called many times.
     *
     * @var string
     */
    private $_getSaltCache;


    /**
     * Initiate an instance of kore_request_token
     *
     * @param string  $baseValue
     */
    public function __construct($baseValue)
    {
        $this->_baseValue = $baseValue;

        /*
         * Init the object from the main configuration (kore::$conf).
         */
        if ($this->_saltSources === null)
            $this->_saltSources = (int) kore::$conf->get('token_default_saltSources', 22);
        if ($this->_timeSlice === null)
            $this->_timeSlice = (int) kore::$conf->get('token_default_timeSlice', 7200);
        if ($this->_nbSlice === null)
            $this->_nbSlice = (int) kore::$conf->get('token_default_nbSlice', 12);

        /*
         * Include any additionnal salt from the main configuration.
         */
        if ($this->_additionnalSalt === null)
            $this->_additionnalSalt = kore::$conf->token_additionnalSalt;
    }

    /**
     * Return the current value of the token.
     *
     * @return string
     */
    public function __toString()
    {
        return $this->_buildToken();
    }

    /**
     * Return the current saltSources (bit mask)
     *
     * @return integer
     */
    public function getSaltSources()
    {
        return $this->_saltSources;
    }

    /**
     * Change salt sources (bit mask)
     *
     * @param  integer $saltSources
     */
    public function setSaltSources($saltSources)
    {
        $this->_saltSources = (int) $saltSources;
        $this->_clearSaltCache();
    }

    /**
     * Add a random string in the salt.
     *
     * @param string $string
     */
    public function addSalt($string)
    {
        $this->_additionnalSalt .= $string;
        $this->_clearSaltCache();
    }

    /**
     * Set a random string as salt.
     *
     * @param string $string
     */
    public function setSalt($string)
    {
        $this->_additionnalSalt = $string;
        $this->_clearSaltCache();
    }

    /**
     * Change time slices accepted for this token.
     *
     * @param integer $time
     * @param integer $nb
     */
    public function setTimeSlice($time, $nb)
    {
        $this->_timeSlice = (int) $time;
        $this->_nbSlice = (int) $nb;
    }

    /**
     * Clear the cache of the _getSalt() method. This method *MUST* be called
     * each time that the _additionnalSalt or _saltSources are modified.
     */
    protected function _clearSaltCache()
    {
        $this->_getSaltCache = null;
    }

    /**
     * Build the value of the token, for a given timestamp, and based on the
     * current setup.
     *
     * @param  integer $timestamp
     * @return string
     */
    protected function _buildToken($timestamp = null)
    {
        /*
         * This salt is temporary and will expire after some time.
         */
        if ($timestamp === null)
            $timestamp = kore::requestTime();

        $slice = floor((int)$timestamp / $this->_timeSlice);

        /*
         * Return the built token.
         */
        return $this->_encodeToken($slice.$this->_getSalt().$this->_baseValue);
    }

    /**
     * Build a salt that will be used to make our token specific to the current
     * website / application.
     *
     * @return string
     */
    protected function _getSalt()
    {
        if ($this->_getSaltCache !== null)
            return $this->_getSaltCache;

        /*
         * Include a user defined salt (from member's login, for example)
         */
        $salt = $this->_additionnalSalt;

        $flags = $this->_saltSources;

        if ($flags & self::USE_HOSTNAME)
            $salt .= ':'. kore::getHostname();
        if ($flags & self::USE_WEBDIR)
            $salt .= ':'. kore::getWebDir();
        if ($flags & self::USE_SERVER_NAME)
            $salt .= ':'. kore_request_http::getHTTPHost();
        if ($flags & self::USE_SESSIONS)
            $salt .= ':'. kore::$session->getId();

        if (($flags & self::USE_IP) and isset($_SERVER['REMOTE_ADDR'])){
            /*
             * If it seems to be an IPv4, we use only the first 24 bits.
             * For an IPv6, we keep the full IP.
             */
            $ip = $_SERVER['REMOTE_ADDR'];
            if (preg_match('#^([0-9]+\.){3}[0-9]+$#', $ip))
                $salt .= ip2long($ip)>>8;
            else
                $salt .= $ip;
        }

        if ($flags & self::USE_USER_AGENT)
            $salt .= ':' . @$_SERVER['HTTP_USER_AGENT'];

        return $this->_getSaltCache = sha1($salt, true);
    }

    /**
     * Hash and encode data to build the final token.
     *
     * @param  string  $rawData
     * @return string
     */
    protected function _encodeToken($rawData)
    {
        /*
         * We use only chars in range A-Za-z0-9.-
         */
        return strtr(rtrim(base64_encode(sha1($rawData, true)), '='), '+/', '-.');
    }

    /**
     * Validate that the givenToken is compatible with the current token setup.
     *
     * @param  string $givenToken
     * @return boolean
     */
    public function validate( $givenToken )
    {
        $givenToken = (string) $givenToken;

        if ($givenToken === '')
            return false;

        $maxOffset = ($this->_nbSlice * $this->_timeSlice);

        $now = (int)kore::requestTime();
        for ($offset = 0; $offset < $maxOffset; $offset += $this->_timeSlice){
            if ($this->_buildToken($now - $offset) === $givenToken)
                return true;
        }

        return false;
    }
}