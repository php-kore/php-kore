<?php
/**
 * Module LessCSS qui permet de formater une CSS à partir d'une CSS
 * au format LessCSS.
 * @author plug-alexandrer
 *
 */
class kore_lessCSS
{
    protected $_source;

    protected $_newToken = null;

    protected $_variable = array();

    public $minify = true;

    /**
     * Accepte l'adresse d'un fichier contenant du CSS à parser
     */
    public static function fromFile($pathFile)
    {
        return self::fromString( file_get_contents($pathFile) );
    }

    /**
     * Accepte une chaine de caractère.
     */
    public static function fromString($string)
    {
        $obj = new self;

        $obj->_source = $string;

        return $obj;
    }

    protected function __construct()
    {
        $this->_newToken = new stdClass();
        $this->_newToken->type = null;
        $this->_newToken->data = '';
        $this->_newToken->parent = null;
        $this->_newToken->children = array();
    }

    /**
     * Vérifie si le paramètre passé est oui ou non une lettre, un chiffre
     * ou un '_'.
     *
     * @param string $word
     * @return boolean
     */
    public function is_word($word)
    {
        return preg_match('#^[a-zA-Z0-9_]$#', $word);
    }

    /**
     * Vérifie si le paramètre passé est oui ou non un espace.
     *
     * @param string $chard
     * @return boolean
     */
    public function is_space($char)
    {
        return preg_match('#^\\s$#', $char);
    }

    /**
     * Méthode Magique __toString()
     */
    public function __toString()
    {
        return $this->parse();
    }

    /**
     * Parse le contenu passé en paramètre.
     *
     * @return string
     */
    public function parse()
    {
        $parsed = $this->getParsed($this->_source);

        $this->cleanupToken($parsed);

        $newTree = clone $this->_newToken;
        $newTree->type = 'root';

        $this->convert($parsed, $newTree);

        $this->cleanupToken($newTree);

        return rtrim($this->createFinalContents($newTree));
    }

    /**
     * Détruit les parents d'un token ainsi que les enfants dans le cas où
     * ceux-ci sont vides de manière recursive.
     *
     * @param stdClass $token
     * @return stdClass
     */
    private function cleanupToken($token)
    {
        unset($token->parent);

        $token->data = trim($token->data);
        $token->data = preg_replace('#\\s+#', ' ', $token->data);
        $token->data = preg_replace('#\\s*,\\s*#', ',', $token->data);

        if( empty($token->children) )
           unset($token->children);
        else {
            foreach( $token->children as $token )
                $this->cleanupToken($token);
        }

        return $token;
    }

    private function convert($sourceTree, $destTree)
    {
        if( !isset($sourceTree->children) )
            return;

        foreach( $sourceTree->children as $token ) {
            if( $token->type === 'tag' ){
               $destTree->children[] = $token;
               $this->verify($token, $destTree);
            } else {
               $destTree->children[] = $token;
            }
        }
    }

    private function createFinalContents($tokens, $tabulation = 0, $isLastProp = false)
    {
        $data = '';
        if( !isset($tokens->children) )
            return $data;

        foreach( $tokens->children as $idx => $token ) {

            switch($token->type) {
                case 'tag':
                    if ( isset($token->children) or !$this->minify ) {

                        if ( !$this->minify and $tabulation )
                            $data .= str_repeat("\t", $tabulation);

                        $data .= $token->data;

                        if ($this->minify)
                            $data .= '{';
                        else
                            $data .= ' {'.PHP_EOL;

                        if ( isset($token->children) )
                            $data .= $this->createFinalContents($token, $tabulation + 1);

                        $data .= '}'.PHP_EOL;
                        if ( !$this->minify )
                            $data .= PHP_EOL;
                    }
                break;
                case 'prop':
                    if ( isset($token->children) ) {

                        if ( !$this->minify and $tabulation )
                            $data .= str_repeat("\t", $tabulation);

                        $isLast = true;
                        $maxId = count($tokens->children) -1;
                        for( $idxTmp = $idx +1; $idxTmp <= $maxId; $idxTmp++ ){
                            if( $tokens->children[$idxTmp]->type === 'prop' ){
                                $isLast = false;
                                break;
                            }
                        }

                        $data .= $token->data.':';
                        $data .= $this->createFinalContents($token, $tabulation + 1, $isLast);
                    }
                break;
                case 'value':
                    $len = strlen($token->data);
                    $offset = 0;
                    while ( $offset < $len and
                         ( ( $pos = strpos($token->data, '$', $offset) ) !== false ) ){
                        $varLen = strspn($token->data, 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789_', $pos+1);

                        if( $varLen === 0 )
                            $data .= substr($token->data, $offset, $pos - $offset + 1);
                        else {
                            $data .= substr($token->data, $offset, $pos - $offset);

                            $varName = substr($token->data, $pos+1, $varLen);
                            if ( isset($this->_variable[$varName]) )
                                $data .= $this->_variable[$varName];
                            else
                                $data .= 'undefined('.$varName.')';

                        }

                        $offset = $pos + 1 + $varLen;
                    }

                    $data .= substr($token->data, $offset);

                    $isLastValue = ( $idx == (count($tokens->children) - 1) );

                    if ( !$isLastValue )
                        $data .= ' ';
                    else{

                        $data = preg_replace('/#([0-9a-fA-F])\1([0-9a-fA-F])\2([0-9a-fA-F])\3/i',
                            '#$1$2$3', $data);

                        if( !$this->minify or !$isLastProp )
                            $data .= ';';
                    }

                    if ( !$this->minify and $isLastValue)
                        $data .= PHP_EOL;
                break;
                case 'comments':
                    if ( !$this->minify )
                        $data .= PHP_EOL.$token->data.PHP_EOL.PHP_EOL;
                break;
            }
        }
        return $data;
    }

    private function verify($token, $destTree)
    {
        if( !isset($token->children) )
            return;

        foreach( $token->children as $key => $tk ){
            if( $tk->type !== 'tag' )
                continue;

            unset($token->children[$key]);
            $destTree->children[] = $tk;

            $parentsNames = explode(',', $token->data);
            $childrenNames = explode(',', $tk->data);

            $nameTag = array();

            foreach($parentsNames as $pnTag )
                foreach($childrenNames as $cnTag)
                    $nameTag[] = $pnTag .' '. $cnTag;

            $tk->data = implode(',', $nameTag);

            $this->verify($tk, $destTree);
        }
    }

    private function getParsed($contents)
    {
        $offset = -1;

        $newTree = NULL;

        $tokens = clone $this->_newToken;
        $tokens->type = 'root';

        $currentToken = $tokens;

        $offsetMax = strlen($contents);

        while(++$offset < $offsetMax) {
            $c = $contents{$offset};

            if ( $currentToken->type === 'comments' ) {
                $token->data .= $c;

                if ( ($c === '/') and (substr($currentToken->data, -2) === '*/') )
                    $currentToken = $currentToken->parent;
            }
            elseif ( $currentToken->type === 'root' or
                     $currentToken->type === 'tag' ) {
                if ( $this->is_space($c) )
                    continue;

                if ( preg_match('/^[#.*\\-@:\\/]$/', $c) or $this->is_word($c) ) {
                    $token = clone $this->_newToken;
                    $token->type = 'unknown';
                    $token->parent = $currentToken;
                    $token->data .= $c;

                    $currentToken->children[] = $token;
                    $currentToken = $token;
                }
                elseif ( $c === '$' ) {
                    $token = clone $this->_newToken;
                    $token->type = 'var';
                    $token->parent = $currentToken;

                    //$currentToken->children[] = $token;
                    $currentToken = $token;
                }
                elseif ( $c === '}' and $currentToken->parent !== null ) {
                    $currentToken = $currentToken->parent;
                }
                else
                    var_dump($c);
            }
            elseif ( $currentToken->type === 'unknown' ) {
                if ( $c === '{')
                   $currentToken->type = 'tag';
                elseif ( $c === ';' or $c === '}' ) {

                    if ( substr($currentToken->data, 0,1) === '@')
                        $token->data .= $c;
                    else {
                        $temp = explode(':', $currentToken->data, 2);

                        $currentToken->type = 'prop';
                        $currentToken->data = $temp[0];

                        $temps = explode(' ', $temp[1]);

                        foreach($temps as $value) {
                            if ( $value !== '') {
                                $token = clone $this->_newToken;
                                $token->type = 'value';
                                $token->data = $value;
                                $currentToken->children[] = $token;
                            }
                        }

                        if ( $c === ';' )
                            $currentToken = $currentToken->parent;
                        else
                            $currentToken = $currentToken->parent->parent;
                    }
                }
                elseif ( $c === '*' and $currentToken->data === '/' ) {
                    $token->data .= $c;
                    $currentToken->type = 'comments';
                }
                else
                   $token->data .= $c;
            }
            elseif ( $currentToken->type === 'var' ) {
                if ( $c === ';' ) {
                    $temp = explode(':', $currentToken->data, 2);
                    $this->_variable[$temp[0]] = trim($temp[1]);

                    $currentToken = $currentToken->parent;
                }
                else
                    $token->data .= $c;
            }
            else
                var_dump($c);
        }

        return $tokens;
    }
}