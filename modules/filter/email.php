<?php

class kore_filter_email
{
    protected $emailAddr = NULL;

    protected $formatIsValid = false;
    protected $domainIsValid = NULL;
    protected $domainWasModified = NULL;

    protected $useCache = false;
    protected $alwaysCheckDomain = true;

    protected $firstPart = NULL;
    protected $host = NULL;
    protected $ext = NULL;
    protected $domain = NULL;

    protected $cacheTimeIfNotExists = 7200;
    protected $cacheTimeIfExists = 2592000;

    public function __construct( $emailAddr )
    {
        $this->emailAddr = trim( $emailAddr );

    if( preg_match( '#^([\\w-]+(?:\\.[\\w-]+)*)@((?:[\\w-]+\\.)*\\w[\\w-]{0,66})\\.([a-z]{2,6}(?:\\.[a-z]{2})?)$#i',
              $emailAddr, $match ) )
        {
            $this->formatIsValid = true;

            $this->firstPart = $match[1];
            $this->host = strtolower( $match[2] );
            $this->ext  = strtolower( $match[3] );
            $this->domain = $this->host .'.'. $this->ext;

            $this->emailAddr = $this->firstPart.'@'.$this->domain;
        }
    }

    public function isValid()
    {
        if( $this->alwaysCheckDomain ) {
            if( $this->formatIsValid ) {
                $this->checkDomain();
                return $this->domainIsValid;
            }
        }

        return $this->formatIsValid;
    }

    public function getEmailAddr()
    {
        if( !$this->isValid() )
            return false;

        return $this->emailAddr;
    }

    protected function checkDomain()
    {
        if( $this->domainIsValid !== NULL )
            return;
        if( !$this->formatIsValid ) {
            $this->domainIsValid = false;
            return;
        }

        if( $this->useCache ) {
            $cache = $this->getDomainCache();
            $infos = $cache->get();
        } else {
            $infos = false;
        }

        if( $infos === false ) {

            $infos = new stdClass();
            $infos->result = 'error';
            $infos->domain = $this->domain;

            /**
             * On commence par tester l'existence du domaine, s'il existe c'est
             * plutôt bon signe. Toutefois on vérifie quand même qu'il soit
             * déclaré comme recevant des emails.
             */
            if( @ checkdnsrr( $this->domain, 'ANY' ) === true ) {
                if( checkdnsrr(  $this->domain, 'MX' )
                  or checkdnsrr( $this->domain, 'A' )
                  or checkdnsrr( $this->domain, 'SRV' ) )
                  $infos->result = 'ok';
            }

            /**
             * A priori le domaine n'existe pas ; on tente de corriger l'adresse
             * saisie.
             */
            if( $infos->result !== 'ok' ) {

              $frequentDomains = $this->getFrequentDomains();

              $plusPetit  = NULL;
              $plusProche = NULL;
              foreach( $frequentDomains as $d ) {
                $lev = levenshtein( $this->domain, $d );
                if( ( $lev < $plusPetit ) or ( $plusPetit === NULL ) ) {
                  $plusPetit  = $lev;
                  $plusProche = $d;
                }
              }
              if( $plusPetit !== NULL ) {
                $len = strlen( $plusProche );
                if( ( $len > 8 and $plusPetit <= 2 ) or
                    $plusPetit <= 1 )
                    {
                      $infos->result = 'modified';
                      $infos->domain = $plusProche;
                    }
              }

              /**
               * Si le domaine n'existe pas, malgré la correction ne conserve
               * le cache que peu de temps.
               */
              $cacheOptions = array( 'lifeTime' =>
                                     $this->cacheTimeIfNotExists );

            } else {

                /**
                 * Si le domaine existe, conserve le cache plus longtemps.
                 */
                $cacheOptions = array( 'lifeTime' =>
                                       $this->cacheTimeIfExists );
            }

            if( $this->useCache )
                $cache->set( $infos, $cacheOptions );
        }

        $this->domainIsValid = ( $infos->result !== 'error' );
        if( $this->domainWasModified = ( $infos->result === 'modified' ) ) {
            $this->domain = $infos->domain;
            $this->emailAddr = $this->firstPart.'@'.$infos->domain;

            if( ($pos = strrpos($this->domain, '.') ) !== false ){
                $this->host = substr($this->domain,0,$pos);
                $this->ext = substr($this->domain,$pos+1);
            }
        }
    }

    protected function getDomainCache()
    {
        $idCache = 'email/domainCheck/' . kore_cache_disk::splitId(
                    rawurlencode( $this->domain ), 1 );

        return new kore_cache_disk( $idCache );
    }


    public function isDomainValid()
    {
        $this->checkDomain();
        return $this->domainIsValid;
    }

    public function wasDomainModified()
    {
        $this->checkDomain();
        return $this->domainWasModified;
    }

  protected function getFrequentDomains()
  {
        return array();
  }
}