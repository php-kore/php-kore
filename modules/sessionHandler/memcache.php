<?php

class kore_sessionHandler_pdoMemcache
{
	protected $_memcache;
	protected $_dbProfil;
	protected $_cacheTimeout;

	protected $_lastBackendWrite;

	protected $_sqlTransactionnal = false;

	function __construct( Memcache $memcache, $dbProfil )
	{
		$this->_memcache = $memcache;
		$this->_dbProfil = $dbProfil;

		$this->_cacheTimeout = kore::$conf->get( 'session_cacheTimeout', 60 );

        kore::$conf->bindWithIni( 'session_timeout', 'session.gc_maxlifetime' );
		kore::$conf->bindWithIni(
        	'session_cookieDomain',     'session.cookie_domain' );
	}

	protected function getDomain()
	{
		static $domain = NULL;
		if( $domain === NULL ){
			$domain = kore::$conf->session_cookieDomain;
			if(empty($domain))
				$domain = kore_request_http::getHTTPHost();
		}
		return $domain ;
	}

	protected function getSqlRead()
	{
		return "select data
				from session
			    where id = :id";
	}
	protected function getSqlUpdate()
	{
		return "update session
				set data = :data, lastUpdate = now()
			    where id = :id";
	}
	protected function getSqlInsert()
	{
		return "insert into session ( id, data, lastUpdate )
				values ( :id, :data, now() )
				on duplicate key update data = values(data),
				                        lastUpdate = values(lastUpdate)";
	}
	protected function getSqlDestroy()
	{
		return "delete from session
				where id = :id";
	}
	protected function getSqlGarbage()
	{
		return "select id
				from session
				where lastUpdate + interval :maxLifeTime seconds < now()";
	}

	public function open( $savePath, $sessionName )
	{
		return true;
	}

	public function close()
	{
		return true;
	}

	public function read( $id )
	{
		$bench = kore::$debug->benchInit( 'session', 'read' );

		$key = $id.':'.$this->getDomain();

        if( ( $data = $this->_memcache->get( $key ) ) !== false ){
        	$bench->setFinalStatus( 'from memcache' );

			$this->_lastBackendWrite = $data['time'];
			$data = $data['sess'];

		} else {
			$db = kore::$db->instance( $this->_dbProfil );

			$query = $this->getSqlRead();
			$params = array( 'id' => $id );

			if( ( $res = $db->query( $query, $params ) ) !== false ) {
				while( ( $row = $res->fetchObject() ) !== false ){
					$data = $row->data;
				}
				unset( $res );
			}

			if( $data === false ){
				$bench->setFinalStatus( 'not found' );
			}else{
				$bench->setFinalStatus( 'from db' );
				$this->_memcache->set( $key, array('data'=>$data,'time'=>NULL),
						NULL, kore::$conf->session_timeout );
			}

		}

		return $data;
	}

	public function write( $id, $data )
	{
		$bench = kore::$debug->benchInit( 'session', 'write' );
		$key = $id.':'.$this->getDomain();

		if( $this->_lastBackendWrite + $this->_cacheTimeout <
			kore::requestTime() ){

			$db = kore::$db->instance( $this->_dbProfil );

			$query = $this->getSqlUpdate();
			$params = array( 'id' => $id, 'data' => $data );

			$nbUpdated = $db->exec( $query, array( $data, $id ) );
			if( $nbUpdated === false ) {
				$bench->setFinalStatus( 'error' );
			} else {
				$bench->setFinalStatus( 'in db' );

				if( $nbUpdated === 0 ){
					$query = $this->getSqlInsert();
					$db->exec( $query, array( $data, $id ) );
				}

				$this->_lastBackendWrite = kore::requestTime();
			}

		} else {
			$bench->setFinalStatus( 'in memcache' );
		}

		$data = array('data'=>$data, 'time'=>$this->_lastBackendWrite);

		return $this->_memcache->set( $data );;
	}

	public function destroy( $id )
	{
		$bench = kore::$debug->benchInit( 'session', 'destroy' );
		$key = $id.':'.$this->getDomain();

		$db = kore::$db->instance( $this->_dbProfil );

		$query = $this->getSqlDestroy();
		$params = array( 'id' => $id );

		$db->exec( $query, $params );

		$this->_memcache->delete( $key );

		return true;
	}

	public function gc( $maxLifeTime )
	{
		$bench = kore::$debug->benchInit( 'session', 'garbage' );

		$db = kore::$db->instance( $this->_dbProfil );

		$query = $this->getSqlGarbage();
		$params = array( 'maxLifeTime' => $maxLifeTime );

		$listToDestroy = array();
		if( ( $res = $db->query( $query, $params ) ) !== false ){
			while( ( $row = $res->fetchObject() ) !== false ){
				$listToDestroy[]= $row->id;
			}
			unset( $res );
		}

		$stmtDestroy = $db->prepare( $this->getSqlDestroy() );
		$domain = $this->getDomain();
		foreach( $listToDestroy as $id ){
			$stmtDestroy->execute( array( 'id' => $id ) );
			$this->_memcache->delete( $id.':'.$domain );
		}

	}

}
