<?php
/**
 * Classe minimale d'aide au débugage.
 *
 * @package debug
 */
class kore_debug extends kore_debugLight
{
    const MAX_BENCH_BY_CATEGORY = 2048;
    const MAX_MESSAGE = 20;

    protected $_message = array();

    protected $_bench      = array();
    protected $_benchKeeped = array();

    static function replaceKoreHandler()
    {
        $old = kore::$debug;
        $new = new self();

        kore::$debug = $new;

        $new->enableDebugging();
        $new->importCheckPoint( $old->exportCheckPoint() );

        kore::$loader->load( 'kore_bench' );
        $new->benchClass = 'kore_bench';
    }

    public function setEnvironment( $env )
    {
        if (!in_array($env, array('prod', 'test', 'dev'))) {
            kore::$error->track("unknown environment [$env]");

        } elseif ($this->_environment !== $env) {
            $this->_environment = $env;
        }
    }

    public function enableDebugging()
    {
        $this->_debugging = true;
    }

    public function message( $msg )
    {
        if( count( $this->_message ) < self::MAX_MESSAGE )
            $this->_message[] = $msg;
    }

    public function getMessages()
    {
        $out = $this->_message;
//        $out[] = 'Memory Usage : ' . round( memory_get_usage()/1024/1024, 2 )
//          . ' Mo (peak '.round( memory_get_peak_usage()/1024/1024, 2 ).' Mo)';

        return $out;
    }

    public function benchCheckPointStop( $category )
    {
        if ( isset( $this->_checkpoint[ $category ] ) ) {
            $event = new stdClass();
            $event->time = kore::time();
            $this->_checkpoint[ $category ][ '' ] = $event;
        }
    }

    public function benchGetCurrentCheckPoint( $category )
    {
        $return = NULL;
        if ( isset( $this->_checkpoint[ $category ] ) ) {
            end($this->_checkpoint[ $category ]);
            $return = key($this->_checkpoint[ $category ]);
        }

        return $return;
    }


    public function keepBench( kore_bench $bench )
    {
        array_push($this->_benchKeeped, $bench);
    }

    public function addBenchResult( $result )
    {
        $elapsed = bcsub(kore::time(), $result->start, 6);

        /*
         * Build the event key.
         */
        $key = $result->key;
        if ($result->status)
            $key .= ' ('.$result->status.')';

        /*
         * Group results by elapsed time.
         */
        $group = ceil(log($elapsed, 2));

        if (isset($this->_bench[$result->category][$key][$group])) {
            /*
             * Update an existing entry.
             */

            $stats = & $this->_bench[$result->category][$key][$group];

            $stats['count']++;
            $stats['elapsed'] = bcadd($stats['elapsed'], $elapsed, 6);

        } else {
            /*
             * Create a new entry.
             */

            $stats = array('count' => 1, 'elapsed' => $elapsed);
            if (!isset($this->_bench[$result->category]))
                $this->_bench[$result->category] = array();

            if (!isset($this->_bench[$result->category][$key]))
                $this->_bench[$result->category][$key] = array();

            $this->_bench[$result->category][$key][$group] = $stats;
        }
    }

    /**
     * (old) method kept only for compatibility.
     */
    public function benchGet()
    {
        $stats = new stdClass();
        $stats->checkpoint = array(
                __METHOD__.'()' => array(
                        'this method is deprecated, please use benchGetCheckpoints() instead' => 0,
                ));
        $stats->bench      = array(
                __METHOD__.'()' => array(
                        'this method is deprecated, please use benchGetResults() instead' => 0,
                ));

        return $stats;
    }

    /**
     * Retourne la liste de tous les checkpoints rencontrés.
     *
     * @return array
     */
    public function benchGetCheckpoints()
    {
        $this->benchCheckPoint('main', 'bench (checkpoints)');

        $result = array();

        if ( $this->_checkpoint !== NULL ) {
            $currentTime = kore::time();

            foreach ( $this->_checkpoint as $category => $list ) {

                $result[ $category ] = array();

                $firstEvent    = NULL;
                $previousEvent = NULL;
                $previousName = NULL;
                foreach ( $list as $name => $event ) {
                    if ( $firstEvent === NULL )
                        $firstEvent = $event;
                    if ( $previousEvent !== NULL ) {
                        $id = $previousName;
                        if ( isset( $previousEvent->memory ) )
                            $id .= ' ::: ' .
                                round($event->memory /1024/1024, 2) .' / '.
                                round($event->memoryPeak /1024/1024, 2) . 'Mo';
                        $result[ $category ][ $id ] =
                            bcsub($event->time, $previousEvent->time, 6);
                    }
                    $previousEvent = $event;
                    $previousName = $name;
                }

                if ( $previousName !== '' ) {
                    if ( $previousEvent !== NULL ) {
                        $id = $previousName;
                        if ( isset( $previousEvent->memory ) )
                            $id .= ' ::: ' .
                                round($event->memory /1024/1024, 2) .' / '.
                                round($event->memoryPeak /1024/1024, 2) . 'Mo';
                        $result[ $category ][ $id ] =
                            bcsub($currentTime, $previousEvent->time, 6);
                    }

                    if ( $firstEvent !== NULL )
                        $result[ $category ][ 'total' ] =
                            bcsub($currentTime, $firstEvent->time, 6);
                }
            }
        }

        if ( $this->canUseXdebug() === true ) {
            if ( !isset( $result['xdebug'] ) )
                $result['xdebug'] = array();
            $result['xdebug']['elapsed'] = xdebug_time_index();
        }

        return $result;
    }

    protected function _formatTime($time)
    {
        return round(bcmul($time, 1000, 6), 3).'ms';
    }

    protected function _formatTimeGroups($groups, $nbCalls = null)
    {
        ksort($groups);

        if ($nbCalls === null){
            $nbCalls = 0;
            foreach ($groups as $stats)
                $nbCalls += $stats['count'];
        }

        $times = array();

        foreach ($groups as $idGroup => $stats){
            $key = '< '.$this->_formatTime(bcpow(2, $idGroup, 9));
            $times[$key] = round($stats['count']*100/$nbCalls, 1);
        }

        return $times;
    }

    public function benchGetResults()
    {
        $this->benchCheckPoint('main', 'bench (results)');

        /*
         * Release all benchs still kept in memory.
         */
        foreach ($this->_benchKeeped as $bench)
            $bench->setFinalStatus('forced');
        $this->_benchKeeped = array();

        $result = array();

        foreach ($this->_bench as $category => $eventList) {
            $cat = new stdClass;
            $cat->name = $category;
            $cat->nbCalls   = 0;
            $cat->totalTime = 0;
            $cat->avgTime   = 0;
            $cat->times     = null;
            $cat->events    = array();

            $result[] = $cat;

            $catGroups = array();

            foreach ( $eventList as $eventKey => $groups ) {
                $event = new stdClass;
                $event->name = $eventKey;
                $event->nbCalls   = 0;
                $event->totalTime = 0;
                $event->avgTime   = 0;
                $event->times     = null;

                /*
                 * Start by counting nbItems
                 */
                foreach ($groups as $idGroup => $stats){
                    $event->nbCalls   += $stats['count'];
                    $event->totalTime = bcadd($event->totalTime, $stats['elapsed'], 6);

                    /*
                     * Count time repartition of the category
                     */
                    if (!isset($catGroups[$idGroup])) {
                        $catGroups[$idGroup] = $stats;
                    } else {
                        $catGroups[$idGroup]['count'] += $stats['count'];
                        $catGroups[$idGroup]['elapsed'] = bcadd($catGroups[$idGroup]['elapsed'], $stats['elapsed'], 6);
                    }
                }

                $event->times = $this->_formatTimeGroups($groups, $event->nbCalls);

                $cat->events[] = $event;
                $cat->nbCalls   += $event->nbCalls;
                $cat->totalTime = bcadd($cat->totalTime, $event->totalTime, 6);

                $event->avgTime = $this->_formatTime(bcdiv($event->totalTime, $event->nbCalls, 6));
                $event->totalTime = $this->_formatTime($event->totalTime);
            }

            $cat->avgTime = $this->_formatTime(bcdiv($cat->totalTime, $cat->nbCalls, 6));
            $cat->totalTime = $this->_formatTime($cat->totalTime);

            $cat->times = $this->_formatTimeGroups($catGroups, $cat->nbCalls);
        }

        return $result;
    }

}
