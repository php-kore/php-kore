<?php

class kore_email_imap
{
    protected $_stream;
    protected $_server;

    public $purgeDeletedMessages = false;

    public function __construct($mailbox, $username, $password, $options = NIL, $retries = 0)
    {
        $this->_server = $mailbox;
        $this->_stream = imap_open($mailbox, $username, $password, $options,
                $retries);

        if( ! $this->_stream )
            throw new Exception("can't connect to the mailbox", 1);
    }

    public function __destruct()
    {
        $flag = 0;
        if( $this->purgeDeletedMessages ) $flag |= CL_EXPUNGE;

        imap_close($this->_stream, $flag);

        if (($alerts = $this->getAlerts()) !== false)
            kore::$error->track("IMAP: alerts after closing connection");
        if (($errors = $this->getErrors()) !== false)
            kore::$error->track("IMAP: errors after closing connection");
    }

    public function infos()
    {
        return imap_check($this->_stream);
    }

    public function getErrors()
    {
        return imap_errors();
    }

    public function getAlerts()
    {
        return imap_alerts();
    }

    public function flushCache($flag)
    {
        return imap_gc($this->_stream, $flag);
    }

    public function listmailbox($pattern = '*')
    {
        $list = imap_list($this->_stream, $this->_server, $pattern);
//        if( is_array($list) ) foreach( $list as $idx => $mailbox )
//            $list[$idx] = utf8_encode(imap_utf7_decode($mailbox));
        return $list;
    }

    public function getmailboxes($pattern = '*')
    {
        $list = imap_getmailboxes($this->_stream, $this->_server, $pattern);
//        if( is_array($list) ) foreach( $list as $idx => $infos )
//            $infos->name = utf8_encode(imap_utf7_decode($infos->name));
        return $list;
    }

    public function listMessageAll()
    {
        $from = 1;
        $to = $this->infos()->Nmsgs;

        $list = imap_fetch_overview($this->_stream, $from.':'.$to);
        return $list;
    }

    public function listMessageRange($from, $to, $options = FT_UID)
    {
        $list = imap_fetch_overview($this->_stream, $from.':'.$to, $options);
        return $list;
    }

    public function getMessageUid( $messageId )
    {
        return imap_uid($this->_stream, $messageId);
    }

    public function getMessageId( $messageUid )
    {
        return imap_msgno($this->_stream, $messageUid);
    }

}