<?php

/**
 *
 * @see kore_errorHandler
 * @package error
 */
class kore_error
{
    /**
     * Some severity constants, copied on RFC3164.
     */
    const SEVERITY_EMERGENCY = 0,  // Emergency: system is unusable
          SEVERITY_ALERT = 1,      // Alert: action must be taken immediately
          SEVERITY_CRITICAL = 2,   // Critical: critical conditions
          SEVERITY_ERROR = 3,      // Error: error conditions
          SEVERITY_WARNING = 4,    // Warning: warning conditions
          SEVERITY_NOTICE = 5,     // Notice: normal but significant condition
          SEVERITY_INFO = 6,       // Informational: informational messages
          SEVERITY_DEBUG = 7;      // Debug: debug-level messages

    /**
     * For compatibility, deprecated constants
     */
    const ALERT = 1,
          WARN = 4,
          NOTICE = 5,
          INFO = 6,
          DEBUG = 7;

    /**
     * Some constants to define the origin of the error.
     */
    const ORIGIN_PHP = 1,
          ORIGIN_UNCAUGHT_EXCEPTION = 2,
          ORIGIN_USER = 3,
          ORIGIN_USER_EXCEPTION = 4;

    protected $_origin;
    protected $_message;
    protected $_code;
    protected $_context;
    protected $_file;
    protected $_line;
    protected $_severity;
    protected $_backTrace;


    /**
     * Convert a severity integer to a string.
     *
     * @param  integer $severity
     * @return string
     */
    static protected function _severityToString($severity)
    {
        static $equiv = array(
                self::SEVERITY_EMERGENCY   => 'emergency',
                self::SEVERITY_ALERT       => 'alert',
                self::SEVERITY_CRITICAL    => 'critical',
                self::SEVERITY_ERROR       => 'error',
                self::SEVERITY_WARNING     => 'warning',
                self::SEVERITY_NOTICE      => 'notice',
                self::SEVERITY_INFO        => 'info',
                self::SEVERITY_DEBUG       => 'debug',
                );

        if (!isset($equiv[$severity]))
            return 'UNKNOWN';

        return $equiv[$severity];
    }


    /**
     * Instanciate a kore_error by copying an Exception (or an ErrorException).
     *
     * @param Exception $e
     * @return kore_error
     */
    static public function fromException($e)
    {
        $context = array('previousException' => $e);

        $error = new static(get_class($e).': '.$e->getMessage(), null,
                $context, $e->getFile(), $e->getLine(), $e->getCode());
        $error->_backTrace = $e->getTrace();

        if ($e instanceOf ErrorException)
            $error->_severity = $e->getSeverity();
        else
            $error->_severity = self::SEVERITY_ERROR;

        $error->_origin = self::ORIGIN_USER_EXCEPTION;

        return $error;
    }

    /**
     * Create a new error message.
     *
     * @param string  $message
     * @param integer $severity
     * @param mixed   $context
     * @param string  $file
     * @param integer $line
     * @param integer $code
     */
    public function __construct($message, $severity = null, $context = null, $file = null, $line = null, $code = null)
    {
        if ($severity === null)
            $severity = self::SEVERITY_WARNING;

        $this->_message = $message;
        $this->_severity = $severity;
        $this->_file = $file;
        $this->_line = $line;
        $this->_context = $context;
        $this->_code = $code;

        $this->_origin = self::ORIGIN_USER;

//        $this->_backTrace = debug_backtrace();
    }

    /**
     * Convert the error to a string.
     *
     * @return string
     */
    public function __toString()
    {
        $message = '';
        if ($this->_severity !== null)
            $message .= ucfirst(static::_severityToString($this->_severity))
                    .': ';

        $message .= $this->_message;

        if ($this->_code)
            $message .= ' #'.$this->_code;

        if ($this->_file !== null)
            $message .= ' in ' . $this->_file;
        if ($this->_line !== null)
            $message .= ' on line ' . $this->_line;

        return $message;
    }

    /**
     * Return an HTML version of the error message.
     *
     * @return string
     */
    public function toHtml()
    {
        return htmlspecialchars((string) $this);
    }

    /**
     * Return an instance of ErrorException, based on this kore_error.
     *
     * @return ErrorException
     */
    public function toException()
    {
        if (($severity = $this->_severity) === null)
            $severity = self::SEVERITY_ERROR;
        elseif ($severity > self::SEVERITY_ERROR)
            $severity = self::SEVERITY_ERROR;

        return new ErrorException($this->_message, (int)$this->_code,
                $severity, (string) $this->_file, $this->_line);
    }

    /**
     * Return the backtrace associated to the error, if there is one.
     *
     * @return array
     */
    public function getBacktrace()
    {
        return $this->_backTrace;
    }

    /**
     * Return the current error message.
     *
     * @return string
     */
    public function getMessage()
    {
        return (string) $this->_message;
    }

    /**
     * Change the current error message.
     *
     * @param string $message
     */
    public function setMessage($message)
    {
        $this->_message = (string) $message;
    }

    /**
     * Return the error code.
     *
     * @return integer
     */
    public function getCode()
    {
        return $this->_code;
    }

    /**
     * Return the severity level.
     *
     * @return integer
     */
    public function getSeverity()
    {
        return $this->_severity;
    }

    /**
     * Change the origin of the error.
     *
     * @param integer $origin
     */
    public function setOrigin($origin)
    {
        $this->_origin = $origin;
    }

    /**
     * Get the origin of the error.
     *
     * @return integer
     */
    public function getOrigin()
    {
        return $this->_origin;
    }

    /**
     * Return the context.
     *
     * @return mixed
     */
    public function getContext()
    {
        return $this->_context;
    }

    /**
     * Indicate if the kore_errorHandler should try to use the PHP's default
     * error reporting system (default is true).
     *
     * @return boolean
     */
    public function usePHPDefaultReporting()
    {
        return kore::$conf->get('error_usePHPDefaultReporting', true);
    }

    /**
     * If the PHP's default error reporting can't be used, then try to report
     * the error.
     */
    public function report()
    {
        /*
         * Affiche l'erreur, si l'option est active.
         */
        if (ini_get('display_errors')){
            if (ini_get('html_errors'))
                echo $this->toHtml() . PHP_EOL;
            else
                echo $this . PHP_EOL;
        }

        /*
         * Trace l'erreur, si l'option est active.
         */
        if (ini_get('log_errors')){
            $message = ini_get('error_prepend_string') .$this.
                    ini_get('error_append_string');

            error_log($message, 0);
        }
    }

    /**
     * Send the error to the current errorHandler.
     *
     * @param integer $phpErrorLevel
     */
    public function track($phpErrorLevel = E_USER_NOTICE)
    {
        kore::$error->trackError($this, $$phpErrorLevel);
    }
}
