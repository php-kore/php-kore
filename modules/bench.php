<?php
class kore_bench extends kore_benchLite
{
    public $start;
    public $category;
    public $key;
    public $status;

    public function __construct( $category, $key )
    {
/*        if (!is_scalar($category) or !is_scalar($key)){
            kore::$error->track(__METHOD__ . " n'accepte que des scalaires");
            if (!is_scalar($category))
                $category = 'not scalar';
            if (!is_scalar($key))
                $key = 'not scalar';
        }*/

        $this->start = kore::time();
        $this->category = (string) $category;
        $this->key = (string) $key;
    }

    public function setFinalStatus($status)
    {
        $this->status = $status;
    }

    public function keep()
    {
        kore::$debug->keepBench($this);
    }

    function __destruct()
    {
        kore::$debug->addBenchResult($this);
    }
}
