<?php

/**
 * Class which contains a list of callbacks to use with kore_errorHandler, to
 * modify error handling.
 */

class kore_error_callbacks
{
    /**
     * Handler which convert all PHP errors to ErrorException.
     *
     * It also ignore the "shut-up" operator in case of fatal error
     * (note : only fatal errors which are send to the error_handler can be
     * catched).
     *
     * Very usefull in development environment, or when you need to garanty
     * data integrity.
     *
     * @param  kore_error $error
     * @return boolean
     */
    public static function convertPHPErrorsToExceptions(kore_error $error)
    {
        /*
         * Skip errors not triggers by PHP.
         */
        if ($error->getOrigin() !== kore_error::ORIGIN_PHP)
            return;

        /*
         * Force displaying of fatal errors.
         */
        if ($error->getSeverity() !== null and
            $error->getSeverity() <= kore_error::SEVERITY_ERROR){

            $error->report();

            /*
             * Mark the reporting as done.
             */
            return true;
        }

        /*
         * If the "shut-up" operator was used, skip the error.
         */
        if (!error_reporting())
            return;

        /*
         * Throw an exception.
         */
        throw $error->toException();
    }

}