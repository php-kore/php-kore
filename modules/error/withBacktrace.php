<?php
class kore_error_withBacktrace extends kore_error
{

    /**
     * Filtre le contenu d'un backtrace afin qu'il soit un peu plus compact.
     *
     * Les chemins connus de Kore sont remplacés par des alias, plus courts.
     * Les occurences de la classe kore_error_withBacktrace sont effacées.
     *
     * @param  array $backtrace
     * @return array
     */
    public static function filterBacktrace($backtrace)
    {
        $replaceFilename = array(
            rtrim(kore::getKoreDir(), '/') => '$KOREDIR',
            );

        if (($tmp = kore::getWebDir()) !== null)
            $replaceFilename[realpath($tmp)] = '$WEBDIR';
        if (($tmp = kore::$conf->include_globalPath ) !== NULL)
            $replaceFilename[realpath($tmp)] = '$INCLUDEDIR';
        if (($tmp = kore::$conf->storagePath) !== NULL)
            $replaceFilename[realpath($tmp)] = '$STORAGEDIR';

        if (isset($replaceFilename['/']))
            unset($replaceFilename['/']);

        $started = false;

        $output = array();
        foreach ($backtrace as $idx => $line){
            if (!$started and isset($line['class'])
                and $line['class'] === __CLASS__)
                continue;

            $started = true;
            if (isset($line['file']))
                $line['file'] = strtr($line['file'], $replaceFilename);
            $output[] = $line;
        }

        return $output;
    }

    /**
     * Converti un backtrace en chaine, pour en permettre l'affichage.
     *
     * @param  array $backtrace
     * @return string
     */
    static public function formatBacktrace($backtrace = null)
    {
        if ($backtrace === null)
            $backtrace = debug_backtrace();

        $backtrace = self::filterBacktrace($backtrace);

        $output = '';
        foreach ($backtrace as $idx => $line){
            if (!isset($line['function']))
                $func = '';
            else
                $func = $line['function'];

            if (!empty($line['type'])) $func = $line['type'] . $func;
            if (!empty($line['class'])) $func = $line['class'] . $func;

            $args = '';
            if (!empty($line['args'])) {
                foreach ($line['args'] as $arg){
                    if ($args !== '') $args .= ', ';

                    $type = gettype($arg);

                    if ($type === 'object')
                        $args .= get_class($arg);
                    elseif ($type === 'boolean')
                        $args .= ($arg ? 'true' : 'false');
                    elseif ($arg === null)
                        $args .= 'null';
                    elseif (is_scalar($arg))
                        $args .= "($type)$arg";
                    elseif (is_array($arg))
                        $args .= "({$type}{".count($arg)."})";
                    else
                        $args .= "($type)";
                }
            }

            $output .= "#$idx ";

            if (!isset($line['file']))
                $output .= "[internal]\t";
            else
                $output .= $line['file'].':'.$line['line'];

            $output .= "\t$func($args)".PHP_EOL;
        }

        return $output;
    }


    /**
     * Affichage un backtrace
     *
     * @param  array $backtrace
     */
    static public function displayBacktrace($backtrace = null)
    {
        echo str_repeat('*', 40) . PHP_EOL;
        echo self::formatBacktrace($backtrace);
        echo str_repeat('*', 40) . PHP_EOL;
    }



    /**
     * Convert the error to a string.
     *
     * @return string
     */
    public function __toString()
    {
        $message = parent::__toString();

        $b = self::formatBacktrace($this->_backTrace);
        if ($b)
            $message .= PHP_EOL."Stack trace:".PHP_EOL.$b.PHP_EOL;

        return $message;
    }


    /**
     * Return an HTML version of the error message.
     *
     * @return string
     */
    public function toHtml()
    {
        return '<pre>'.parent::toHtml().'</pre>';
    }


    /**
     * Indicate if the kore_errorHandler should try to use the PHP's default
     * error reporting system (default is true).
     *
     * @return boolean
     */
    public function usePHPDefaultReporting()
    {
        /*
         * If xDebug is loaded and the SAPI is not CLI, then don't try to do the
         * xDebug's job.
         */
        if (extension_loaded('xdebug') and kore::$conf->sapiName !== 'cli')
            return parent::usePHPDefaultReporting();

        return false;
    }

}