<?php

class kore_template_compiler
{
    /**
     * Nom de la classe à utiliser par défaut pour la configuration du template.
     *
     * @var string
     */
    protected $_defaultConfigClass;

    /**
     * Nom de la classe à utiliser par défaut pour le contexte du template.
     *
     * @var string
     */
    protected $_defaultContextClass;

    /**
     * Configuration utilisée pour le traitement du template.
     *
     * @var kore_template_config
     */
    protected $_config;

    /**
     * Context dans lequel traiter le template.
     *
     * @var kore_template_context
     */
    protected $_context;

    /**
     * Indique s'il faut se limiter à la précompilation du template uniquement,
     * afin de l'intégrer dans la compilation d'un autre cache.
     *
     * @var boolean
     */
    protected $_preCompilation = false;

    /**
     * Contenu du template.
     *
     * @var string
     */
    protected $_contents;

    /**
     * Indique si le template est déjà compilé ou non.
     *
     * @var boolean
     */
    protected $_compiled = true;


    protected $_sourceName;
    protected $_position = array();


    protected $_tagStack = array();



    /**
     * Crée une instance du compilateur.
     *
     * @param  $config              kore_template_config
     * @param  $context             kore_template_context
     * @param  $onlyPreCompilation  boolean
     * @return kore_template_compiler
     */
    public function __construct( $config = NULL, $context = NULL, $onlyPreCompilation = false )
    {
        if( $config === NULL ) {
            if( ( $config = $this->_defaultConfigClass ) === NULL )
                $config = kore::$conf->get('template_configClass',
                        'kore_template_config');
        }
        if( is_string($config) ) $config = new $config;
        /**
         * Clone la configuration afin de s'assurer que la compilation ne la
         * modifiera pas.
         */
        else $this->_config = clone $config;

        if( $context === NULL ) {
            if( ( $context = $this->_defaultContextClass ) === NULL )
                $context = kore::$conf->get('template_contextClass',
                        'kore_template_context');
        }
        if( is_string($context) ) $context = new $context;
        /**
         * Clone le context afin de s'assurer que la compilation ne le modifiera
         * pas.
         */
        else $this->_context = clone $context;

        $this->_preCompilation = (bool) $onlyPreCompilation;

        /**
         * TODO : validation des options.
         */
        if( !$this->_config->htmlParser ){
            $this->_config->encodeEntitiesIfNotUTF8 = false;
            $this->_config->encodeEntitiesIfUTF8 = false;
        }

    }

    protected function throwError( $message, $errorLine )
    {
        throw new kore_template_compiler_exception($message, $errorLine,
            $this->_sourceName);
    }

    public function setContents( $contents )
    {
        $this->_contents = $contents;
        $this->_compiled = false;

        $this->_position = array(0 => ':memory:');

        $this->formatContents();
    }

    public function loadContentsFromFile( $fileName )
    {
        $this->_contents = file_get_contents($fileName);
        $this->_compiled = false;

        if( $this->_contents === false )
            $this->throwError('template "' . $fileName . '" not found', 0);

        $this->_sourceName = $fileName;
        $this->_position = array(0 => $fileName);

        $this->formatContents();
    }

    /**
     * Formate le contenu précédement chargé afin de simplifier la compilation.
     */
    protected function formatContents()
    {
        /**
         * Pour faciliter les traitements, converti systématiquement la source
         * au format UNIX, et remplace toutes les tabulations par 4 espaces.
         */
		$search = array("\r\n", "\r", "\t");
		$replace = array("\n", "\n", '    ');

        /**
         * Pour éviter les problèmes de compatibilité, on remplace les tags PHP
         * par une version "encodée".
         */
        if( $this->_config->shortOpenTag ) {
            $search[]= '<'.'?';
            $replace[]= '<{hidden:}?';
        } else {
            $search[]= '<'.'?php';
            $replace[]= '<{hidden:}?php';
        }
        $search[]= '?>';
        $replace[]= '?{hidden:}>';

        $this->_contents = str_replace($search, $replace, $this->_contents);

        /**
         * Supprime les espaces superflus en fin de ligne.
         */
        $this->_contents = preg_replace('# +\\n#', "\n", $this->_contents);
    }

    /**
     * Retourne le contenu compilé.
     *
     * @return string
     */
    public function getResult()
    {
        if( ! $this->_compiled ) $this->compile();
        return $this->_contents;
    }

    /**
     * Compile le template.
     */
    protected function compile()
    {
        /**
         * Recherche d'abord les attributs "kore:" dans les tags HTML/XHTML/XML
         */
        if( $this->_config->htmlParser )
            $this->step_koreNameSpace();

        /**
         * Traite les tags "statiques".
         */
        $this->step_staticInternalTags();

        if( ! $this->_preCompilation ) {
            /**
             * Traite ensuite les tags "dynamiques".
             */
            $this->step_dynamicInternalTags();

            /**
             * Nettoye le code du template.
             */
            if( $this->_config->minifyTemplateCode )
                $this->minifyResult();

/*
            header( 'Content-type: text/plain' );
            echo "===============================================\n";
            echo $this->_contents;
            echo "\n";
            for($i=0;$i<50;$i++)
                echo "===============================================\n";
            $code = '?'.'>' . $this->_contents;
            eval($code);
            exit; */
        }

        $this->_compiled = true;
    }

    /**
     * Première passe : recherche les tags HTML/XML utilisant des attributs
     * préfixés par "kore:". Ces attributs sont ainsi remplacés par leur
     * équivalent utilisant la syntaxe "normale".
     */
    protected function step_koreNameSpace()
    {
        $bench = kore::$debug->benchInit('template',
                'koreNameSpace ' . basename($this->_sourceName));

        $compiler = new kore_template_compiler_koreNameSpace(
                $this->_config, $this->_context);
        $compiler->setContents($this->_contents, $this->_sourceName);

        $this->_contents = $compiler->getResult();
    }

    /**
     * Seconde passe : il s'agit ici de traiter uniquement les tags ayant la
     * propriété "statique". Ceux ci ne sont traités qu'à la compilation, c'est
     * par exemple le cas des tags {load:}.
     */
    protected function step_staticInternalTags()
    {
        $bench = kore::$debug->benchInit('template',
                'staticInternalTags ' . basename($this->_sourceName));

        $compiler = new kore_template_compiler_internalTags(
                $this->_config, $this->_context);
        $compiler->compileStatic(true);
        $compiler->setContents($this->_contents, $this->_sourceName);

        $this->_contents = $compiler->getResult();
    }

    /**
     * Troisième passe : on traite les tags restants, ceux qui seront exécutés
     * à chaque utilisation du template.
     */
    protected function step_dynamicInternalTags()
    {
        $bench = kore::$debug->benchInit('template',
                'dynamicInternalTags ' . basename($this->_sourceName));

        $compiler = new kore_template_compiler_internalTags(
                $this->_config, $this->_context);
        $compiler->compileStatic(false);
        $compiler->setContents($this->_contents, $this->_sourceName);

        $this->_contents = $compiler->getResult();
    }

    /**
     * Quatrième passe : finition. Avant d'enregistrer le résultat de la
     * compilation sur le disque on tente de minimiser la taille du code PHP et
     * XHTML.
     * Divers avantages, bien que parfois minimes :
     * - fichier plus petit, c'est à dire moins d'utilisation du disque et du
     *   cache disque
     * - code PHP plus succinct, plus rapide à parser
     * - code HTML plus compact, plus rapide à transférer
     *
     * Le principal inconvénient étant que cela augmente légèrement la durée du
     * processus de compilation.
     */
    protected function minifyResult()
    {
        $bench = kore::$debug->benchInit('template', 'minifyResult');

        $phpMinifier = new kore_minify_php($this->_contents);

        if( $this->_config->htmlParser and
            $this->_config->minifyTemplateNonPhpCode ){

            $xhtmlMinifier = new kore_minify_xhtml();
            $xhtmlMinifier->useFullXHTML($this->_context->_conf->isXML);
            $xhtmlMinifier->setHaveABody(strpos($this->_contents, '<body')!==false);
            $phpMinifier->setDataMinifier($xhtmlMinifier);
        }

        $this->_contents = $phpMinifier->getMinified();
    }

}
