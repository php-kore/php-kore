<?php

class kore_template_compiler_internalTags extends kore_template_compiler_step
{
    const TYPE_NO_BLOCK       = 0;
    const TYPE_START_BLOCK    = 1;
    const TYPE_CONTINUE_BLOCK = 2;
    const TYPE_END_BLOCK      = 3;

    protected $_compileStatic = false;
    protected $_tagStack = array();

    protected $_internalCommands = array(
        'hidden', 'load', 'continue', 'break',
        'if', 'else', 'elseif', 'endif',
        'for', 'endfor',
        'foreach', 'endforeach',
        'while', 'endwhile',
        'end',
        );

    protected $_allowedFunctions = array();

    public function initialize()
    {
        $this->_internalCommands = array_flip($this->_internalCommands);
        $this->_allowedFunctions = array_flip($this->_config->allowedFunctions);
    }

    public function compileStatic( $static )
    {
        $this->_compileStatic = (bool) $static;
    }

    public function getResult()
    {
        if( $this->_compileStatic === true )
            $contextReference = '$c';
        else
            $contextReference = '$c';

        $encodeEntities = $this->_config->encodeEntities();

        $entitiesOptions = 'ENT_COMPAT, \''.addslashes($this->_config->charset).'\'';

        $newContents = '';

        $offset = 0;
        $nbModifiedTags = 0;

        $tokenizer = new kore_template_compiler_internalTokenizer($this->_contents, $this->_config);

        while(($newOffset = strpos($this->_contents, '{', $offset) )!==false) {

            $newContents .= substr($this->_contents, $offset, $newOffset -
                    $offset);

            $offset = $newOffset;

            try {
                $r = $tokenizer->parse($offset);
            } catch ( Exception $e ) {
                $this->throwException('syntax error : ' . $e->getMessage(),
						$offset);
            }

            /**
             * S'il ne s'agit pas d'un tag interne, l'ignore.
             *
             * TODO : ne plus prendre en compte ces cas ?
             */
            if( $r->ignoreStatment ) {
                $newContents .= '{';
                $offset++;
                continue;
            }

            $commandName = NULL;

            /**
             * Détermine quelle commande correspond à ce tag.
             */
            foreach( $r->modifiers as $modifierName => $tmp ) {

                if( isset( $this->_internalCommands[$modifierName] ) ) {

                    if( $commandName === NULL ) {

                        $commandName = $modifierName ;
                        unset( $r->modifiers[ $modifierName ] );

                    } else {

                        $this->throwException(
                            "only one command per statment is allowed (" .
                            "'$commandName' or '$modifierName')", $offset );

                    }
                }
            }

            if( $commandName === NULL )
                $commandName = 'echo';

            /**
             * Détermine les modifieurs autorisés.
             */
            $modifiers = array();

            switch( $commandName ) {
                case 'echo':
                    $allowedModifiers = array('static', 'url', 'js', 'pre');
                    break;

                case 'load':
                    /**
                     * Le load est systématiquement géré de manière statique.
                     */
                    $modifiers['static'] = true;
                    $allowedModifiers = array();
                    break;

                default:
                    $allowedModifiers = array('static');
            }
            foreach( $allowedModifiers as $modifier )
                $modifiers[ $modifier ] = NULL ;

            foreach( $r->modifiers as $modifierName => $tmp ) {

                if( ! in_array($modifierName, $allowedModifiers) ) {

                    $this->throwException(
                        "modifier \"$modifierName\" not in allowed list : ".
                        implode( ', ', $allowedModifiers ), $offset );

                }
                else $modifiers[ $modifierName ] = true ;
            }

            if( !isset( $modifiers['static'] ) )
                $modifiers['static'] = false ;
            if( isset( $modifiers['url'] ) and $modifiers['url'] )
                $modifiers['pre'] = true;

            if( $this->_compileStatic !== $modifiers['static'] ) {
                /**
                 * Il est nécessaire de continuer quand même le traitement afin
                 * de préserver la pile de tags.
                 */
            }

            $code = $this->convertsTokens($contextReference, $r->tokens,
                    $offset );

            $preCode = NULL;
            $postCode = NULL;

            $tagOpen = '<?php ';
            $tagClose = ' ?'.'>';
            if( $r->linefeed ) $tagClose .= "\n";

            $completeCode = true;
            $tagType = self::TYPE_NO_BLOCK;
            $tagTypeDepends = array();

            switch( $commandName ) {

                case 'foreach':
                    $parts = explode( ',', $code );
                    if( count( $parts ) < 2 or count( $parts ) > 3 ) {
                        $this->throwException(
                            'syntax error in "foreach" statment' , $offset );
                    }

                    $code = 'foreach( ' . $parts[0] .' as ' . $parts[1];
                    if( isset($parts[2]) )
                        $code .= ' => ' . $parts[2];
                    $code .= ' ) {' ;

                    $tagType = self::TYPE_START_BLOCK;

                    break;

                case 'for':
                    $parts = explode(';', $code);
                    if( count( $parts ) !== 3 ) {
                        $this->throwException( 'syntax error in "for" statment',
                            $offset );
                    }

                    $code = 'for( ' . $parts[0] .' ; ' . $parts[1] . ' ; '
                            . $parts[2] . ' ) {' ;

                    $tagType = self::TYPE_START_BLOCK;

                    break;

                case 'while':
                    $code = "while($code) {";

                    $tagType = self::TYPE_START_BLOCK;

                    break;

                case 'if':
                    $code = "if($code) {";

                    $tagType = self::TYPE_START_BLOCK;
                    break;

                case 'elseif':
                    $code = "} elseif($code) {";

                    $tagType = self::TYPE_CONTINUE_BLOCK;
                    $tagTypeDepends = array('if');
                    break;

                case 'else':
                    $code = '} else {' ;

                    $tagType = self::TYPE_CONTINUE_BLOCK;
                    $tagTypeDepends = array('if');
                    break;

                case 'endforeach':
                    $code = '}';
                    $tagType = self::TYPE_END_BLOCK;
                    $tagTypeDepends = array('foreach');
                    break;
                case 'endfor':
                    $code = '}';
                    $tagType = self::TYPE_END_BLOCK;
                    $tagTypeDepends = array('for');
                    break;
                case 'endwhile':
                    $code = '}';
                    $tagType = self::TYPE_END_BLOCK;
                    $tagTypeDepends = array('while');
                    break;
                case 'endif':
                    $code = '}';
                    $tagType = self::TYPE_END_BLOCK;
                    $tagTypeDepends = array('if');
                    break;
                case 'end':
                    $code = '}';
                    $tagType = self::TYPE_END_BLOCK;
                    $tagTypeDepends = array('foreach', 'for', 'while', 'if');
                    break;

                case 'hidden':
                    break;
                case 'continue':
                    $code = 'continue';
                    break;
                case 'break':
                    $code = 'break ' . $code;
                    break;

                case 'echo':
                    $encodeHtml = $this->_config->htmlParser;

                    foreach( $modifiers as $mod => $enabled ) {
                        if(!$enabled) continue;

                        if( $mod === 'url' ) {
                            $code = "rawurlencode($code)";
                        } elseif( $mod === 'js' ) {
                            $code = $contextReference ."->escapeJS($code)";
                        } elseif( $mod === 'pre' ) {
                            $encodeHtml = false;
                        }
                    }

                    if( $encodeHtml ) {
                        if( $encodeEntities )
                            $code = "htmlentities($code,$entitiesOptions)";
                        else $code = "htmlspecialchars($code,$entitiesOptions)";
                    }

                    $code = 'echo ' . $code;

                    break;

                case 'load':
                    if( $this->_compileStatic !== true ) {
                        $this->throwException( "la commande load ne peut être ".
                                "utilisée durant la compilation dynamique",
                                $offset );
                    }

                    $php = '$code = ' . $code . '; return true;';

/*                    $executer = new kore_template_compiler_executeStatic();
                    $executer->setContext( $this->_context );
                    $newContents = $executer->execute( $newContents );
*/

                    /**
                     * TODO : voir s'il est possible d'utiliser
                     * kore_template_compiler_executeStatic à la place d'eval().
                     */
                    $c = $this->_context;
                    $return = eval($php);

                    if( $return !== true ){
                        $this->throwException('syntax error in "load" statment',
                            $offset );
                    }

                    $completeCode = false ;

                    $code = $this->includeTemplate( $code );
                    break;

                default:
                    break;
            }

            if( $completeCode ) {

                if( $tagType === self::TYPE_NO_BLOCK ) {
                    /**
                     * Pour le moment rien de particulier à faire ici.
                     */

                } elseif( $tagType === self::TYPE_START_BLOCK ) {

                    $tagInfos = new stdClass();
                    $tagInfos->isStatic = $modifiers['static'];
                    $tagInfos->commandName = $commandName;
                    $tagInfos->offset = $offset;

                    array_push( $this->_tagStack, $tagInfos );

                } else {
                    /**
                     * Il s'agit d'un bloc à "continuer" ; en vérifie la
                     * compatibilité.
                     */
                    if( $tagType === self::TYPE_END_BLOCK )
                        $tagInfos = array_pop( $this->_tagStack );
                    else $tagInfos = end( $this->_tagStack );

                    if( is_object( $tagInfos ) ) {

                        if(!in_array($tagInfos->commandName, $tagTypeDepends)){
                            $this->throwException(
                                "le tag $commandName nécessite un tag ouvrant ".
                                "de type ".implode('/', $tagTypeDepends),
                                $offset );
                        }

                        $modifiers['static'] = $tagInfos->isStatic;

                    } elseif( $this->_compileStatic ) {
                        /**
                         * S'il s'agit d'une précompilation ignore ce cas
                         * d'erreur mais exclus le tag de la précompilation.
                         */
                        $modifiers['static'] = false;
                    } else {
                        $this->throwException(
                            "le tag $commandName nécessite un tag ouvrant",
                            $offset );
                    }
                }

                $code = $tagOpen . $preCode . $code . $postCode . $tagClose ;
            }

            if( $this->_compileStatic !== $modifiers[ 'static' ] ) {

                $newContents .= substr( $this->_contents, $offset,
                        $r->length );

            } else {

                $nbModifiedTags ++;
                $newContents .= $code;

            }

            $offset += $r->length ;
        }

        foreach( $this->_tagStack as $tagInfos ) {

            if( $this->_compileStatic ) {
                if( $tagInfos->isStatic ) {
                    $this->throwException(
                        'parse error, static command "'.$tagInfos->commandName.
                        '" not closed at the end of is template',
                        $tagInfos->offset );
                }
            } else {
                $this->throwException(
                    'parse error, command "' .$tagInfos->commandName.
                    '" was not closed',
                    $tagInfos->offset );
            }

        }

        $newContents .= substr($this->_contents, $offset);

        if( $nbModifiedTags > 0 and $this->_compileStatic ) {
            $executer = new kore_template_compiler_executeStatic();
            $executer->setContext($this->_context);
            $newContents = $executer->execute($newContents, 0,
                    $this->_sourceName);
        }

        return $newContents;
    }

    protected function convertsTokens( $templateContext, array $tokenList, $offset )
    {
        /**
         * Note : des espaces sont volontairement ajoutés un peu partout par
         * précaution : si un cas n'est pas pris en charge correctement par le
         * moteur de template, cela devrait ainsi déclencher un PARSE ERROR coté
         * PHP plutôt que de provoquer une éventuelle faille de sécurité.
         */
        $code = '';
        $templateContext = ' ' . $templateContext . '->';

        $lastToken = new kore_template_compiler_internalToken( '', '', '' );

        foreach( $tokenList as $token ) {

            $keyword = $token->contents;

            switch( $token->type ) {

                case kore_template_compiler_internalTokenizer::T_NUMERIC:
                    $code .= ' '.$keyword.' ';
                    break;

                case kore_template_compiler_internalTokenizer::T_VARIABLE:
                    if( $token->subtype === 'constant' )
                       $code .= ' '.$keyword.' ';
                    else {
                        $code .= $templateContext.$keyword;
                        if( $token->subtype === '' )
                            $code .= ' ';
                    }
                    break;

                case kore_template_compiler_internalTokenizer::T_PROPERTY:
                    $code .= $keyword;
                    if( $token->subtype === '' )
                        $code .= ' ';
                    break;

                case kore_template_compiler_internalTokenizer::T_METHOD:
                    $code .= $keyword;
                    break;

                case kore_template_compiler_internalTokenizer::T_FUNCTION:
                    if( isset( $this->_allowedFunctions[$keyword] ) ) {
                        $code .= ' '.$keyword;
                    } else {
                        $code .= $templateContext.$keyword;
                    }
                    break;

                case kore_template_compiler_internalTokenizer::T_SEMICOLON:
                    $code .= $keyword;
                    break;
                case kore_template_compiler_internalTokenizer::T_STRING:
                    $code .= ' '.$keyword.' ';
                    break;
                case kore_template_compiler_internalTokenizer::T_SYMBOL:
                    if( $keyword === '.' )
                        $code .= '->';
                    elseif( $token->subtype !== 'fakeOperator' )
                        $code .= $keyword ;
                    else
                        $code .= $this->_config->fakeOperators[$keyword];
                    break;
                default:
                    $this->throwException(
                        'internal error : "' . $token->type .
                        '" tokens are not converted', $offset );
            }

            $lastToken = $token;
        }

        return $code;
    }

    protected function includeTemplate( $templateName )
    {
        $template = new kore_template($templateName, $this->_config, $this->_context);
        $templateFile = $template->getSourcePath();

        $compiler = new kore_template_compiler($this->_config, $this->_context, true);
        $compiler->loadContentsFromFile($templateFile);

        return $compiler->getResult();
    }

}
