<?php
class kore_template_compiler_internalTokenizer
{
    const T_WHITE = 0;
    const T_STRING = 1;
    const T_WORD = 2;
    const T_SEMICOLON = 3;
    const T_SYMBOL = 4;

    const T_FUNCTION = 10;
    const T_METHOD = 11;
    const T_VARIABLE = 12;
    const T_OBJECT = 13;
    const T_PROPERTY = 14;
    const T_NUMERIC = 15;

    protected $buffer;
    public $length;

    protected $confFakePHPOperators;
    protected $confPHPOperators = array('and', 'or', 'xor');
    protected $confPHPConstants = array('TRUE', 'FALSE', 'NULL',
            'true', 'false', 'null', 'PHP_EOL');
    protected $confSymbolsWichForcesNewToken = array('(', ')', '[', ']', ';');

    public function __construct( $buffer, kore_template_config $config )
    {
        $this->buffer = $buffer;
        $this->config = $config;

        $this->confFakePHPOperators = $config->fakeOperators;
        $this->confPHPOperators = array_flip($this->confPHPOperators);
        $this->confPHPConstants = array_flip($this->confPHPConstants);
        $this->confSymbolsWichForcesNewToken = array_flip($this->confSymbolsWichForcesNewToken);
    }

    public function parseXXX( $startOffset = 0 )
    {
        $this->length = 1;
        $endOfBuffer = strlen($this->buffer);

        $r = new stdClass();
        $r->modifiers = array();
        $r->ignoreStatment = false;
        $r->tokens = array();
        $r->length = 1;
        $r->linefeed = false;

        $tokenIdx = 0;

        $i = $startOffset;
        $endWasFound = false;

        $currentTokenType = null;
        $currentToken = null;
        $bracketsStack = array();

        $currentOffset = $startOffset + 1;
        $chunkLen = 0;

        $mask = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ_0123456789';
        $wordLength = 0;
        do {
//            echo "[$currentOffset]\n";
            $wordLength = strspn($this->buffer, $mask, $currentOffset);
            if( $wordLength > 0 ){
                $char = substr($this->buffer, $currentOffset, $wordLength);
                $newTokenType = self::T_WORD;
                $currentOffset += $wordLength;
            } else {
                $newTokenType = $currentTokenType;
                $char = $this->buffer{$currentOffset};
                $currentOffset++;

                if( $char === ':' ) {
                    $newTokenType = self::T_SEMICOLON;

                } elseif( $char === '\'' or $char === '"' ) {
//                    echo "substr [" . substr($this->buffer, $currentOffset, 50)."]\n";

                    $currentTokenType = null;
                    $newTokenType = self::T_STRING;
                    $string = $char;

                    $i = $currentOffset-1;
                    $escape = false;
                    while( ++$i < $endOfBuffer ) {
                        $c = $this->buffer{$i};
                        $char .= $c;

                        if( $c === $string and !$escape ) {
                            $string = false ;
                            break;
                        } elseif( $c === '\\' and ! $escape )
                            $escape = true ;
                        else $escape = false;
                    }

                    if( $string )
                        throw new Exception('end of string not found (started by ['.$string.'] )');
                    elseif( $char === '"' and
                            preg_match('#\\$[a-zA-Z0-9_]#S', $currentToken->contents) )
                        throw new Exception('variable found in a string enclosed by double quotes');

                    $currentOffset += strlen($char) - 1;

                } elseif( $char === '}' ) {
                    $endWasFound = ($currentOffset-1) - $startOffset ;
                    break;

                } elseif( $char === "\n" ) {
                    /**
                     * Est-il vraiment nécessaire d'ignorer ça ? Prévoir un
                     * mécanisme pour ignorer le JS serait peut-être préférable.
                     */
                    $r->ignoreStatment = true;
                    break;

                } elseif( $char === ' ' or $char === "\n" ) {
                    $currentTokenType = self::T_WHITE;
                    continue;

                } elseif( $char === '{' or $char === '$' or $char === '`' ) {
                    throw new Exception( 'character "'.$char.'" is not allowed in templates statments' );
                } else {// Symbols
                    $newTokenType = self::T_SYMBOL;

                    if ( ( $currentTokenType === self::T_SYMBOL ) and
                         ( isset($this->confSymbolsWichForcesNewToken[$char])
                        or isset($this->confSymbolsWichForcesNewToken[$currentToken->contents])) )

                        $currentTokenType = null;

                    if( $char === '(' or $char === '[' ) {
                        array_push($bracketsStack, $char);

                    } elseif( $char === ']' ) {
                        $endStack = end($bracketsStack);

                        if( $endStack === '[' )
                            array_pop($bracketsStack);
                        elseif( $endStack )
                            throw new Exception('"'.$char.'" found while expecting ")"');
                        else
                            throw new Exception('unexpected "'.$char.'" found');
                    } elseif( $char === ')' ) {
                        $endStack = end($bracketsStack);

                        if( $endStack === '(' )
                            array_pop($bracketsStack);
                        elseif( $endStack )
                            throw new Exception('"'.$char.'" found while expecting "]"');
                        else
                            throw new Exception('unexpected "'.$char.'" found');
                    }
                }
            }

            if( $currentTokenType === $newTokenType )
                $currentToken->contents .= $char;
            else {
                $currentTokenType = $newTokenType;
                $currentToken = new kore_template_compiler_internalToken(
                        $currentTokenType, $char);
                $r->tokens[++$tokenIdx] = $currentToken;
            }

        } while( $currentOffset < $endOfBuffer );

        if( $r->ignoreStatment ){
            print_r($r);
            exit;
            return $r;
        }

        if( ! $endWasFound )
            throw new Exception('end of statment not found');
        elseif( count($bracketsStack) > 0 ) {
//            var_dump(substr($this->buffer, $startOffset, $endWasFound));
            $char = array_pop($bracketsStack);
            if( $char === '(' )
                $needed = ')';
            else $needed = ']';

            throw new Exception( '"'.$needed.'" required in ' );
        }

        $this->checkTokens($r);

        $r->length = $endWasFound + 1;
        $this->length = $r->length;

        if( substr($this->buffer, $r->length, 1) === "\n" )
            $r->linefeed = true;

        return $r;
    }

    public function parse( $startOffset = 0 )
    {
        $endOfBuffer = strlen($this->buffer);
        $this->length = 1;

        $r = new stdClass();
        $r->modifiers = array();
        $r->ignoreStatment = false;
        $r->tokens = array();
        $r->length = 1;
        $r->linefeed = false;

        $tokenIdx = 0;

        $currentOffset = $startOffset;
        $endWasFound = false;

        $currentTokenType = null;
        $currentToken = null;
        $bracketsStack = array();

        while( ++ $currentOffset < $endOfBuffer ) {

            $char = $this->buffer{$currentOffset};

            if( ( $char >= 'a' and $char <= 'z' ) or
                      ( $char >= 'A' and $char <= 'Z' ) or
                      ( $char >= '0' and $char <= '9' ) or
                      ( $char === '_' ) ) {

                if( $currentTokenType === self::T_WORD )
                    $currentToken->contents .= $char ;
                else {
                    $currentTokenType = self::T_WORD;
                    $currentToken = new kore_template_compiler_internalToken(
                            $currentTokenType, $char);
                    $r->tokens[++$tokenIdx] = $currentToken;
                }

            } elseif( $char === ':' ) {
                if( $currentTokenType === self::T_SEMICOLON )
                    $currentToken->contents .= $char ;
                else {
                    $currentTokenType = self::T_SEMICOLON;

                    $currentToken = new kore_template_compiler_internalToken(
                            $currentTokenType, $char);
                    $r->tokens[++$tokenIdx] = $currentToken;
                }

            } elseif( $char === '\'' or $char === '"' ) {
                $currentTokenType = self::T_STRING;

                $currentToken = new kore_template_compiler_internalToken(
                        $currentTokenType, $char);
                $r->tokens[++$tokenIdx] = $currentToken;

                $string = $char ;

                if( $char === '"' )
                    $currentToken->subtype = 'parsed';

                $i = $currentOffset;
                $escape = false;
                while( ++$i < $endOfBuffer ) {
                    $c = $this->buffer{$i};
                    $currentToken->contents .= $c;

                    if( $c === $string and !$escape ) {
                        $string = false ;
                        break;
                    } elseif( $c === '\\' and ! $escape )
                        $escape = true ;
                    else $escape = false;
                }

                if( $string )
                    throw new Exception('end of string not found (started by ['.$string.'] )');
                elseif( $char === '"' and
                        preg_match('#\\$[a-zA-Z0-9_]#S', $currentToken->contents) )
                    throw new Exception('variable found in a string enclosed by double quotes');

                $currentOffset += strlen($currentToken->contents) - 1;

            } elseif( $char === '}' ) {
                $endWasFound = $currentOffset - $startOffset ;
                break;

            /**
             * Est-il vraiment nécessaire d'ignorer ça ? Prévoir un mécanisme
             * pour ignorer le JS serait peut-être préférable.
             */
            } elseif( $char === "\n" ) {
                $r->ignoreStatment = true;
                break;

            } elseif( $char === ' ' or $char === "\n" ) {
                $currentTokenType = self::T_WHITE;

            } elseif( $char === '{' or $char === '$' or $char === '`' ) {
                throw new Exception( 'character "'.$char.'" is not allowed in templates statments' );

            } else {// Symbols

                if( ( $currentTokenType !== self::T_SYMBOL )
                    or isset( $this->confSymbolsWichForcesNewToken[ $char ] )
                    or isset( $this->confSymbolsWichForcesNewToken[ $r->tokens[ $tokenIdx ]->contents ] ) ) {

                    $currentTokenType = self::T_SYMBOL;
                    $currentToken = new kore_template_compiler_internalToken(
                            $currentTokenType, $char);
                    $r->tokens[++$tokenIdx] = $currentToken;

                } else $currentToken->contents .= $char ;

                if( $char === '(' or $char === '[' ) {
                    array_push($bracketsStack, $char);

                } elseif( $char === ']' ) {
                    $endStack = end($bracketsStack);

                    if( $endStack === '[' )
                        array_pop($bracketsStack);
                    elseif( $endStack )
                        throw new Exception('"'.$char.'" found while expecting ")"');
                    else
                        throw new Exception('unexpected "'.$char.'" found');
                } elseif( $char === ')' ) {
                    $endStack = end($bracketsStack);

                    if( $endStack === '(' )
                        array_pop($bracketsStack);
                    elseif( $endStack )
                        throw new Exception('"'.$char.'" found while expecting "]"');
                    else
                        throw new Exception('unexpected "'.$char.'" found');
                }
            }
        }

        if( $r->ignoreStatment )
            return $r;

        if( ! $endWasFound )
            throw new Exception('end of statment not found');
        elseif( count($bracketsStack) > 0 ) {
            $char = array_pop($bracketsStack);
            if( $char === '(' )
                $needed = ')';
            else $needed = ']';

            throw new Exception( '"'.$needed.'" required in ' );
        }

        $this->checkTokens($r);

        $r->length = $endWasFound + 1;
        $this->length = $r->length;

        if( substr($this->buffer, $startOffset+$r->length, 1) === "\n" )
            $r->linefeed = true;

        return $r;
    }

    public function checkTokens($r)
    {
//        print_r($r);

        // check tokens
        $searchModifiers = true;
        $tokenIdx = 0 ;
        $previousToken = NULL;
        $currentToken = NULL;

        while( isset($r->tokens[++$tokenIdx]) ) {
            if( $currentToken !== NULL )
                $previousToken = $currentToken;

            $currentToken = $r->tokens[$tokenIdx];

            if( isset($r->tokens[$tokenIdx + 1 ]) )
                $nextToken = $r->tokens[$tokenIdx + 1];
            else $nextToken = NULL;

            if( $searchModifiers and $currentToken->type === self::T_WORD
                                 and $nextToken !== NULL
                                 and $nextToken->contents === ':' ) {

                $r->modifiers[$currentToken->contents] = true;

                // remove current and next token
                $currentToken = NULL ;
                unset($r->tokens[$tokenIdx]);
                unset($r->tokens[$tokenIdx + 1]);
                $tokenIdx++;

                continue;

            } else $searchModifiers = false;

            if( $currentToken->type === self::T_WORD ) {

                $currentToken->type = self::T_VARIABLE;
                $currentToken->subtype = '';

                if( is_numeric($currentToken->contents) ) {
                    $currentToken->type = self::T_NUMERIC;

                } elseif( isset($this->confPHPOperators[$currentToken->contents]) ) {
                    $currentToken->type = self::T_SYMBOL;
                    $currentToken->subtype = 'operator';

                } elseif( isset($this->confFakePHPOperators[$currentToken->contents]) ) {
                    $currentToken->type = self::T_SYMBOL;
                    $currentToken->subtype = 'fakeOperator';

                } elseif( isset($this->confPHPConstants[$currentToken->contents]) ) {
                    $currentToken->type = self::T_VARIABLE;
                    $currentToken->subtype = 'constant';

                } else {
                    $isFromObject = false ;

                    if( $previousToken !== NULL ) {
                        if( $previousToken->contents === '::' or $previousToken->contents === '.' ) {
                            $isFromObject = true;
                            $currentToken->type = self::T_PROPERTY;
                        }
                    }

                    if( $nextToken !== NULL ) {
                        $firstChar = substr($nextToken->contents, 0, 1);
                        if( $firstChar === '(' ) {
                            if( $isFromObject )
                                $currentToken->type = self::T_METHOD;
                            else $currentToken->type = self::T_FUNCTION;

                        } elseif( $firstChar === '[' ) {
                            $currentToken->subtype = 'array';
                        }
                    }
                }

            } elseif( $currentToken->contents === '.' ) {

                if( $previousToken === NULL )
                    throw new Exception( 'parse error on "."' );

                if( $previousToken->type === self::T_SYMBOL ) {
                    // skip error

                } elseif ( $previousToken->type === self::T_VARIABLE or $previousToken->type === self::T_PROPERTY )
                    $previousToken->subtype = 'object';
                else
                    throw new Exception( 'parse error on "."' );

            } elseif( $currentToken->contents === '->' ) {
                throw new Exception( 'please use "." to access object properties and methods instead of "->"' );

            } elseif( $currentToken->type === self::T_SEMICOLON ) {
                if( $currentToken->contents === '::' and $previousToken !== NULL and
                    ( $previousToken->type === self::T_VARIABLE or $previousToken->type === self::T_PROPERTY ) )
                    $r->previousToken->subtype = 'class';
                else {
                    throw new Exception( 'parse error on "'.$currentToken->contents.'"' );
                }
            }

            if( $currentToken->type !== self::T_SYMBOL and $previousToken !== NULL
                and ( $previousToken->type !== self::T_SYMBOL and $previousToken->type !== self::T_SEMICOLON ) ) {
                throw new Exception( 'parse error near '.$currentToken->type.' "' . $currentToken->contents .'" ' );
            }
        }
    }

    public function __toString()
    {
        return substr($this->buffer, 0, $this->length);
    }

}

class kore_template_compiler_internalToken
{
    public $type;
    public $subtype;
    public $contents;

    function __construct( $type, $contents )
    {
        $this->type = $type ;
        $this->contents = $contents ;
    }
}

