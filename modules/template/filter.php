<?php

interface kore_template_filter {

    public function filter(kore_template_config $config,
        kore_template_context $context, $contents);

}