<?php

class kore_user_groupList extends kore_db_mapping_list
{
    /**
     * Classe commune à tous les objets de la collection.
     */
    const CLASS_ITEM = 'kore_user_group';


    static function fromUser(kore_user $user)
    {
        $list = new static;

        $stmt = $list->_stmtGetFromUser();
        $list->_fetchStatement($stmt, array('idUser' => $user->getKey()));

        return $list;
    }

    /**
     * Retourne un PDOStatement permettant de récupérer tous les groupes d'un
     * utilisateur.
     * Un seul paramètre sera passé : idUser
     *
     * @return PDOStatement
     */
    protected function _stmtGetFromUser()
    {
        $db = $this->_getReadDb();
        $class = static::CLASS_ITEM;

        $myTable = $class::getFullTableName($db);

        list($joinUser, $keyUser) = $class::helperJoinRelUser($db);

        return $db->prepare(
           "select ".$class::MAIN_COLS."
            from $myTable
            $joinUser
            where $keyUser = :idUser
            and now() >= startDate
            and (endDate is null or now() < endDate)");
    }

}
