<?php

class kore_user_userList extends kore_db_mapping_list
{
    /**
     * Classe commune à tous les objets de la collection.
     */
    const CLASS_ITEM = 'kore_user';


    static public function fromGroup(kore_user_group $group)
    {
        $list = new static;

        $stmt = $list->_stmtGetFromGroup();
        $list->_fetchStatement($stmt, array('idGroup' => $group->getKey()));

        return $list;
    }

    static public function fromPrivilege(kore_user_privilege $priv)
    {
        $list = new static;

        $stmt = $list->_stmtGetFromPriv();
        $list->_fetchStatement($stmt, array(
                'idPrivUser'  => $priv->getKey(),
                'idPrivGroup' => $priv->getKey()));

        return $list;
    }

    /**
     * Retourne un PDOStatement permettant de récupérer tous les utilisateurs
     * d'un groupe.
     * Un seul paramètre sera passé : idGroup
     *
     * @return PDOStatement
     */
    protected function _stmtGetFromGroup()
    {
        $db = $this->_getReadDb();
        $class = static::CLASS_ITEM;


         $myTable = $class::getFullTableName($db);
         list($joinGroup, $keyGroup) = $class::helperJoinRelGroup($db);

         return $db->prepare(
            "select ".$class::MAIN_COLS."
             from $myTable
             $joinGroup
             where $keyGroup = :idGroup
             order by ".$class::PRIMARY_KEY);
    }

    /**
     * Retourne un PDOStatement permettant de récupérer tous les utilisateurs
     * bénéficiant d'un privilège défini.
     * Deux paramètres seront passés : idPrivUser & idPrivGroup
     *
     * @return PDOStatement
     */
    protected function _stmtGetFromPriv()
    {
        $db = $this->_getReadDb();
        $class = static::CLASS_ITEM;

        $myTable = $class::getFullTableName($db);

        list($joinPriv, $keyPriv) = $class::helperJoinRelPrivilege($db);

        list($joinGroup, $keyGroup) = $class::helperJoinRelGroup($db);
        $classGroup = $class::CLASS_GROUP;
        list($joinPrivGroup, $keyPrivGroup) = $classGroup::helperJoinRelPrivilege($db,
            $class::TABLE_REL_GROUP, $classGroup::KEY_REL_PRIVILEGE);

        $query =
           "select ".$class::MAIN_COLS."
            from $myTable
            $joinPriv
            where $keyPriv = :idPrivUser
            union
            select ".$class::MAIN_COLS."
            from $myTable
            $joinGroup
            $joinPrivGroup
            where $keyPrivGroup = :idPrivGroup
            order by " . $class::PRIMARY_KEY;

        return $db->prepare($query);
    }

}
