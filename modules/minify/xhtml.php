<?php
class kore_minify_xhtml
{
    const typeArg = 'a',
          typeTag = 't',
          typeData = 'd';

    protected $_previousChunkType = self::typeData;
    protected $_previousChunk;

    protected $_delimiters = '<';

    protected $_inTag = false;
    protected $_inArg = false;

    protected $_tagStack = array();
    protected $_currentTagName;

    protected $_haveBody = true;
    protected $_inBody = false;
    protected $_forceReturnsOnTags = array('!doctype', 'body', 'table', 'ul');









    protected $_contents = NULL;
    protected $_minified = true;

    protected $_useFullXHTML = false;
    protected $_charset = 'utf-8';
    protected $_searchedCharset = 'UTF-8, ISO-8859-1';

    // http://en.wikipedia.org/wiki/HTML_element
    protected $_inLineTags = array(
            'em', 'strong', 'q', 'cite', 'dfn', 'abbr', 'acronym',
            'code', 'samp', 'kbd', 'var',
            'sub', 'sup', 'del', 'ins',
            'a', 'img', 'br', 'map', 'area', 'object', 'param',
            'embed', 'noembed',
            'span',
            'b', 'i', 'big', 'small', 'tt',

            /**
             * Quelques tags "deprecated" mais pouvant encore être utilisés dans
             * un email.
             */
            'u', 'font',
            );



    protected $_currentTag = NULL;
    protected $_firstDataAfterTag = true;
    protected $_tagWasPrecedBySpace = false;

    protected $_nbChars = 0;

    protected $_charsetSearch;












/*
    protected $minifiedContents = NULL;
    protected $charset = 'iso-8859-1';

    protected $done = false;

    protected $stackTags = array();
    protected $stackLevel = 0;


    protected $inTag = false;
    protected $endTag = NULL;
    protected $inProperty = false;
    protected $endProperty = false;
*/

    public static function minify( $contents )
    {
        $minifier = new kore_minify_xhtml($contents);
        return $minifier->getMinified();
    }

    public function __construct( $contents = NULL )
    {
        if( $contents !== NULL )
            $this->setContents($contents);

        /**
         * Pour des raisons de performance, inverse les tableaux.
         */
        $this->_inLineTags = array_flip($this->_inLineTags);
        $this->_forceReturnsOnTags = array_flip($this->_forceReturnsOnTags);
    }

    public function setContents( $contents )
    {
        /**
         * Converti au format UNIX ; et remplace toutes les tabulations par 4
         * espaces afin de faciliter les traitements.
         */
        $contents = str_replace(array("\r\n", "\r", "\t"),
                array("\n", "\n", '    ' ), $contents);

        $this->_contents = $contents;
        $this->_minified = false;
    }

    public function useFullXHTML( $enable = true )
    {
        $this->_useFullXHTML = $enable;
    }

    public function setCharset( $charset )
    {
        $this->_charset = strtolower($charset);
    }

    public function getMinified()
    {
        if( ! $this->_minified )
            $this->minification();

        return $this->_contents;
    }

    protected function minification()
    {
        $newContents = '';
        $this->_tagStack = array();

        $this->_minified = true;
        $this->_contents = $newContents;
    }

    public function setHaveABody($have)
    {
        $this->_haveBody = (bool) $have;
        $this->_inBody = !$this->_haveBody;
    }

    public function add( $htmlPart )
    {
        /**
         * Converti au format UNIX ; et remplace toutes les tabulations par 4
         * espaces afin de faciliter les traitements.
         */
        $htmlPart = str_replace(array("\r\n", "\r", "\t"),
                array("\n", "\n", '    ' ), $htmlPart);

//        var_dump($this->_inTag, $this->_inArg, $htmlPart, $this->_delimiters);

        $newHtml = '';

        $offset = 0;
        $end = strlen($htmlPart);
        do {
            $len = strcspn($htmlPart, $this->_delimiters, $offset);

            $isLastChars = ( $offset+$len >= $end );

            if( $this->_inTag === true ) $len++;

//            echo "POS $offset + $len < $end\n";

            if( $len === 0 ) $chunk = '';
            else $chunk = substr($htmlPart, $offset, $len);

            $offset += $len;

            $sep = substr($htmlPart, $offset-1, 1);

            if( $isLastChars ){
                if( $this->_inTag === false )
                    $type = self::typeData;
                elseif( $this->_inArg === false )
                    $type = self::typeTag;
                else $type = self::typeArg;

            } elseif( $this->_inTag === false ){
                $type = self::typeData;

                $this->_inTag = true;
                $this->_delimiters = '"\'>';

            } elseif( $this->_inArg === false ){
                $type = self::typeTag;

                if( $sep === '>' ){
                    $this->_inTag = false;
                    $this->_delimiters = '<';
                } else {
                    $this->_inArg = true;
                    $this->_delimiters = $sep;
                }

            } else {
                $type = self::typeArg;
                $this->_inArg = false;
                $this->_delimiters = '"\'>';
            }

            if( $chunk !== '' )
                $newHtml .= $this->_addChunk($type, $chunk);
            else {
                $this->_previousChunkType = null;
                $this->_previousChunk = null;
            }

        } while( $offset < $end );

/*        echo "================================\n";
        echo "================================\n";
        echo "================================\n";*/

        return $newHtml;


/*        echo "================================\n";
        echo "================================\n";
        echo "================================\n";
        echo "================================\n";
        echo $htmlPart;
        echo "\n================================\n";
*/
        $newHtml = '';

        $this->_firstDataAfterTag = false;

        if( $this->_inArg ) $limiter = '"';
        elseif( $this->_inTag ) $limiter = '>';
        else $limiter = '<';

        while( ($pos = strpos($htmlPart, $limiter) ) !== false ) {
            if( $limiter !== '<' ) $pos++;

            if( $pos === 0 ) $before = '';
            else $before = substr($htmlPart, 0, $pos);

            $htmlPart = substr($htmlPart, $pos);

            if( $this->_inArg ) {
                if( $before !== '' )
                    $newHtml .= $this->addArg( $before );

                $this->_inArg = false;
                $limiter = '>';

            } elseif( $this->_inTag ) {
                if( $before !== '' )
                    $newHtml .= $this->addTag( $before );

                $this->_inTag = false;
                $limiter = '<';
            } else {
                if( $before !== '' )
                    $newHtml .= $this->addData( $before );

                $this->_inTag = true;
                $this->_tagWasPrecedBySpace = (bool)
                    preg_match( '#\\s$#', $before );
                $limiter = '>';
            }

            $pos = strrpos( $newHtml, "\n" );
            if( $pos === false )
                $this->_nbChars += strlen( $newHtml );
            else $this->_nbChars = strlen( $newHtml ) - $pos - 1;
        }

        if( $htmlPart !== '' ) {
            if( $this->_inArg )
                $newHtml .= $this->addArg($htmlPart);
            elseif( $this->_inTag )
                $newHtml .= $this->addTag($htmlPart);
            else
                $newHtml .= $this->addData($htmlPart);

            $pos = strrpos($newHtml, "\n");
            if( $pos === false )
                $this->_nbChars += strlen($newHtml);
            else $this->_nbChars = strlen($newHtml) - $pos - 1;
        }

        $len = strlen($newHtml);
        for($i=0;$i<$len;$i++)
            if( ord($newHtml{$i})>127 ){
                $newHtml = $this->changeCharset($newHtml);
                break;
            }
/*
        echo $newHtml;
        echo "\n================================\n";
*/
        return $newHtml;
    }

    protected function _addChunk($type, $code)
    {
//        echo "$type\t(".strlen($code).') '.str_replace("\n", "\\n", $code);

        if( $type === self::typeData )
            $newCode = $this->_addData($code);
        elseif( $type === self::typeTag )
            $newCode = $this->_addTag($code);
        elseif( $type === self::typeArg )
            $newCode = $this->_addArg($code);
        else $newCode = $code;

//        echo "\t(".strlen($newCode).') '.str_replace("\n", "\\n", $newCode)."\n\n";

        $this->_previousChunkType = $type;
        $this->_previousChunk = $code;

        return $newCode;
    }

    protected function _addData( $code )
    {
        if( ! $this->_inBody )
            return trim(preg_replace(
                    array('#\\s*\\n\\s*#', '# +#'),
                    array("\n", ' '), $code));

        $currentTag = false;
        $inLine = false;
        foreach( $this->_tagStack as $tagName ) {
            /**
             * Ne fait aucune modification dans les balises <pre>
             */
            if( $tagName === 'pre' )
                return $code;
            if( isset($this->_inLineTags[$tagName]) )
                $inLine = true;
        }

        /**
         * Remplace les espaces consécutifs par un unique espace.
         */
        $code = preg_replace(
                array('#\\s*\\n\\s*#', '# +#'),
                array("\n", ' '), $code);

        /**
         * Désactivé car génère des bugs
         */
//        if( $inLine === false )
//            $code = trim($code);

        return $code;
    }

    protected function _addTag( $code )
    {
        /**
         * Supprime les espaces superflus.
         */
        $search = array('#\\s+#', '#\\s+>#');
        $replace = array(' ', '>');

        if( $this->_useFullXHTML ) {
            $search[] = '#\\s+/>#';
            $replace[] = '/>';
        } else {
            $search[] = '#(\\S)/>#';
            $replace[] = '$1 />';
        }

        $code = preg_replace($search, $replace, $code);

        if( substr($code, 0, 1) === '<' ){
            if( ! preg_match('#^<(/)?([?!]?[\\w:\\-]+)#s', $code, $match) )
                $this->_currentTagName = null;
            else {
                $close = ( $match[1] !== '' );
                $tagName = strtolower($match[2]);

                $this->_currentTagName = $tagName;

                if( ! $close ) {
                    array_push($this->_tagStack, $tagName);
                    if( $tagName === 'body' )
                        $this->_inBody = true;

                } else {
                    $lastTag = array_pop($this->_tagStack);
                    if( $lastTag !== $tagName ){
                        /**
                         * Erreur HTML probablement, dans le doute on remet le tag
                         * dans la liste.
                         */
                        array_push($this->_tagStack, $lastTag);
                    } else {
                        if( $tagName === 'body' )
                            $this->_inBody = false;
                    }
                }
            }
        }

        if( $this->_currentTagName !== null ){
            if( substr($code, -2) === '/>' ){
                /**
                 * S'il s'agit d'un tag autofermant, le retire de la pile.
                 */
                array_pop($this->_tagStack);
            }

            if( substr($code, -1) === '>' ){
                if( isset($this->_forceReturnsOnTags[$this->_currentTagName]) )
                    $code .= "\n";
            }
        }

        return $code;
    }

    protected function _addArg( $code )
    {
        /**
         * Remplace les espaces consécutifs par un unique espace.
         */
        $code = preg_replace('#\\s+#', ' ', $code);



        return $code;
    }

    protected function oldAddTag( $html )
    {
//        echo 'T';
        /**
         * Supprime les espaces superflus.
         */
        $search = array(
            '#\\s+#',
            '#\\s+>#',
            );
        $replace = array(
            ' ',
            '>',
            );
        if( $this->_useFullXHTML ) {
            $search[] = '#\\s+/>#';
            $replace[] = '/>';
        }

        $html = preg_replace($search, $replace, $html);

        /**
         * Si _inTag est un booléen, c'est qu'il s'agit d'un nouveau tag.
         */
        if( $this->_inTag === true ) {
            $this->_currentTag = NULL;
            $this->_inTag = $html;

            if($this->_inTag === '<')
                $this->_inTag .= '?';

        } elseif( $this->_currentTag === NULL ){
            $this->_inTag .= $html;
        }

        if( $this->_currentTag === NULL ) {
            if( preg_match( '#^<(/)?([?!]?[\\w:\\-]+)#s', $this->_inTag,
                    $match ) ) {

                $this->_currentTag = $match[2];

                if( isset( $this->_forceReturnsOnTags[ $this->_currentTag ] )
                    and substr($html,0,1) === '<' ) {

                    $this->_nbChars = strlen($html);
                    $html = "\n" . $html;
                }

                if( $match[1] === '/' ) {
                    array_pop( $this->_tagStack );

                } else {

                    if( isset( $this->_inLineTags[ $this->_currentTag ] ) ){
                        if( $previousTag = end( $this->_tagStack ) )
                            $previousTag->containsData = true;
                    }

                    $tagInfos = new stdClass();
                    $tagInfos->name = $this->_currentTag;
                    $tagInfos->containsData = isset(
                        $this->_inLineTags[ $this->_currentTag ] );

                    array_push( $this->_tagStack, $tagInfos );
                }
            }
        }

        if( substr($html, -1) === '>' ) {

            if( substr($this->_currentTag, 0, 1) === '?' ) {
                $closed = true;
                $endLength = 2;
            } elseif( substr( $html, -2 ) === '/>' ) {
                $closed = true;
                $endLength = 2;
            } elseif( $this->_currentTag === '!DOCTYPE' ) {
                $closed = true;
                $endLength = 1;
            } else {
                $closed = false;
                $endLength = 1;
            }

            if( $closed ) {
                array_pop($this->_tagStack);

                if( isset( $this->_forceReturnsOnTags[ $this->_currentTag ] ) ){
                    $html .= "\n";
                    $this->_nbChars = 0;
                }
            }

            $this->_currentTag = NULL;

            $this->_firstDataAfterTag = true;

            if( $this->_nbChars + strlen( $html ) > 256 ) {
                if(! preg_replace( '# ([a-zA-Z:]+=["\'])#', "\n\$1", $html ) )
                    if(! isset($this->_inTag[$this->_currentTag]) )
                        $html .= "\n";
                    else $html = substr( $html, 0, -1 * $endLength ) ."\n".
                            substr( $html, -1 * $endLength );
            }
        }

        return $html;
    }

    protected function oldAddData( $html )
    {
//        echo 'D';

        $inBody = false;
        $inTitle = false;

        $containsData = false;
        $currentTag = false;
        foreach( $this->_tagStack as $currentTag ) {
            /**
             * Ne fait aucune modification dans les balises <pre>
             */
            if( $currentTag->name === 'pre' ) {
                return $html;
            }

            if( $currentTag->name === 'body' ) {
                $inBody = true;
            } elseif( $currentTag->name === 'title' ) {
                $inTitle = true;
            }

            if( $currentTag->containsData )
                $containsData = true;
        }

//        var_dump($inBody, $inTitle, $containsData, $currentTag);

        $html = preg_replace(array('#\\s*(\\n)\\s*#', '#( )+#'), '$1', $html);

//        var_dump($html);

        /**
         * Si on est pas encore dans la balise body, les espaces importent peu.
         */
        if( !$inTitle and !$inBody ) {
            return ltrim($html);
        }

//        echo 'tmp';
        $tHtml = ltrim($html);

        if( $tHtml !== '' and $currentTag ){
            $currentTag->containsData = true;
        } elseif( $this->_firstDataAfterTag ) {
            if( $this->_tagWasPrecedBySpace or ! $containsData )
                $html = "$tHtml";
        }

//        var_dump($html);

        if( $html !== '' )
            $this->_firstDataAfterTag = false;

        return $html;
    }

    protected function changeCharset( $html )
    {
        $encoding = mb_detect_encoding( $html, $this->_searchedCharset );
        if( strtolower($encoding) !== $this->_charset )
            $html = mb_convert_encoding( $html, $this->_charset,
                    $encoding );

        if( $this->_charset === 'iso-8859-1' ){

            if( $this->_charsetSearch === NULL ) {

                $this->_charsetSearch = get_html_translation_table(
                        HTML_ENTITIES, ENT_NOQUOTES );

                $keeped = array( '&', '<', '>', '"' );
                foreach( $keeped as $char )
                    if( isset( $this->_charsetSearch[$char] ) )
                        unset( $this->_charsetSearch[$char] );

                // Ajout des caractères spéciaux "Windows" (cp1252)
                $this->_charsetSearch[ chr( 0x80 ) ] = '&euro;';
                $this->_charsetSearch[ chr( 0x82 ) ] = '&sbquo;';
                $this->_charsetSearch[ chr( 0x83 ) ] = '&fnof;';
                $this->_charsetSearch[ chr( 0x84 ) ] = '&bdquo;';
                $this->_charsetSearch[ chr( 0x85 ) ] = '&hellip;';
                $this->_charsetSearch[ chr( 0x86 ) ] = '&dagger;';
                $this->_charsetSearch[ chr( 0x87 ) ] = '&Dagger;';
                $this->_charsetSearch[ chr( 0x88 ) ] = '&circ;';
                $this->_charsetSearch[ chr( 0x89 ) ] = '&permil;';
                $this->_charsetSearch[ chr( 0x8A ) ] = '&Scaron;';
                $this->_charsetSearch[ chr( 0x8B ) ] = '&lsaquo;';
                $this->_charsetSearch[ chr( 0x8C ) ] = '&OElig;';
                $this->_charsetSearch[ chr( 0x91 ) ] = '&lsquo;';
                $this->_charsetSearch[ chr( 0x92 ) ] = '&rsquo;';
                $this->_charsetSearch[ chr( 0x93 ) ] = '&ldquo;';
                $this->_charsetSearch[ chr( 0x94 ) ] = '&rdquo;';
                $this->_charsetSearch[ chr( 0x95 ) ] = '&bull;';
                $this->_charsetSearch[ chr( 0x96 ) ] = '&ndash;';
                $this->_charsetSearch[ chr( 0x97 ) ] = '&mdash;';
                $this->_charsetSearch[ chr( 0x98 ) ] = '&tilde;';
                $this->_charsetSearch[ chr( 0x99 ) ] = '&trade;';
                $this->_charsetSearch[ chr( 0x9A ) ] = '&scaron;';
                $this->_charsetSearch[ chr( 0x9B ) ] = '&rsaquo;';
                $this->_charsetSearch[ chr( 0x9C ) ] = '&oelig;';
                $this->_charsetSearch[ chr( 0x9F ) ] = '&Yuml;';

            }

            $html = strtr( $html, $this->_charsetSearch );

        } elseif( $this->_charset === 'utf-8' ) {

            $html = htmlentities( $html, ENT_NOQUOTES, $this->_charset );
            $chars = array(
                '&amp;' => '&',
                '&lt;' => '<',
                '&gt;' => '>',
                );

            $html = strtr( $html, $chars );
        }

        return $html;
    }

/*
        $htmlPartEnd = strlen( $htmlPart );
        echo "\n##################################################################\n";
        echo '(',strlen( $htmlPart ),")\n";
        echo $htmlPart, " #\n";

        $output = NULL;

        $offset = 0;

        if( $this->inProperty ) {
            $pos = strpos( $htmlPart, $this->endProperty );
            if( $pos === false )
                return $htmlPart;

            $len = strlen( $this->endProperty );
            $offset = $pos + $len;
            $output .= $this->parseProperty( substr( $htmlPart, 0, $offset ) );
            $this->inProperty = false;

        } elseif( $this->inTag ) {
            $pos = strpos( $htmlPart, $this->endTag );
            if( $pos === false )
                return $htmlPart;

            $len = strlen( $this->endTag );
            $offset = $pos + $len;
            $output .= $this->parseTag( substr( $htmlPart, 0, $offset ) );
            $this->inTag = false;
        }

        return $output;
*/

/*
        $bufferEnd = strlen( $this->bufferCurrent );
        $offset = 0 ;
        while( ( $newOffset = strpos( $this->bufferCurrent, '<', $offset ) ) !== false )
        {
            $this->addData( substr( $this->bufferCurrent, $offset, $newOffset - $offset ) );
            $offset = $newOffset;

            $ignoreTag = false;
            $deleteTag = false;

            /**
             * Bloc CDATA. N'en modifie pas le contenu (JS ?)
             * /
            if( substr( $this->bufferCurrent, $offset + 1, 8 ) === '![CDATA[' )
            {
                $ignoreTag = true;
                $end_tag = ']]>';
                $start_tag_length = 9;
                $end_tag_length = strlen( $end_tag );
                $tag_name = 'cdata';

                // Recherche immédiatement la fermeture de balise, et ignore le contenu
                $end = strpos( $this->bufferCurrent, $end_tag, $offset + 1 );
            }
            /**
             * Commentaire HTML. Le supprime.
             * /
            elseif( substr( $this->bufferCurrent, $offset + 1, 3 ) === '!--' )
            {
                $deleteTag = true ;
                $end_tag = '-->';
                $start_tag_length = 4;
                $end_tag_length = strlen( $end_tag );
                $tag_name = 'comments';

                // Recherche immédiatement la fermeture de balise, et ignore le contenu
                $end = strpos( $this->bufferCurrent, $end_tag, $offset + 1 );
            }
            else
            {
                $end_tag = '>';
                $start_tag_length = 1;
                $end_tag_length = strlen( $end_tag );
                $tag_name = 'unknown';

                if( ! preg_match( '#^[/?]?([\\w:]+)#s', substr( $this->bufferCurrent, $offset + 1, 32 ), $match ) )
                {
                    // this is not an html tag... so it should be an html error, but
                    // ignore it in case it is a PHP statment.
                    $this->addData( '<' );
                    $offset++;
                    continue;
                }

                // Search for tags end
                $tag_name = strtolower( $match[1] );

                $string = false;
                $i = $offset + $start_tag_length - 1;
                $end = false;

                while( ++ $i < $buffer_end )
                {
                    $char = $this->bufferCurrent{$i};

                    if( $string !== false )
                    {
                        if( $char === $string )
                        {
                            $string = false;
                        }
                    }
                    elseif( $char === '{' )
                    {
                        $string = '}';
                    }
                    elseif( $char === '"' or $char === '\'' )
                    {
                        $string = $char ;
                    }
                    elseif( substr( $this->bufferCurrent, $i, $end_tag_length ) === $end_tag )
                    {
                        $end = $i;
                        break;
                    }
                }
            }

            if( $end === false )
            {
                throw new kore_html_templateCompilerHtmlException(
                    'end of tag "' . $tag_name.'" not found', $offset );
            }

            // extrait le contenu de la balise
            $tag = substr( $this->bufferCurrent, $offset + $start_tag_length, $end - $offset - $start_tag_length );
            $trim_tag = trim( $tag );

            if( $deleteTag )
            {
                // skip it
            }
            elseif( $ignoreTag )
            {
                $this->completeBufferTemp( '<![CDATA['.$tag.']]>' );
            }
            else
            {
                $tag_name = strtolower( $tag_name );

                $open = true; $close = true;
                if( $tag{0} === '/' )
                    $open = false ;
                elseif( $tag{0} === '?' )
                    ;
                elseif( $tag{strlen( $tag ) - 1} !== '/' )
                    $close = false;

                if( $open )  $this->openHtmlTag( $tag, $tag_name, $offset );
                if( $close ) $this->closeHtmlTag( $tag, $tag_name, $open === false );
            }

            // place le pointeur après la balise, pour continuer l'analyse
            $offset = $end + $end_tag_length;
        }

        foreach( $this->stackTags as $tagName => $list )
        {
            foreach( $list as $infos )
            {
                throw new kore_html_templateCompilerHtmlException(
                    'tag "'.$tagName.'" is not closed', $infos['offset'] );
            }
        }

        $this->completeBufferTemp( substr( $this->bufferCurrent, $offset ) );
        $this->commitBufferTemp();

        return $this->bufferCurrent;
    }
*/

/*
    protected function openHtmlTag( $tag, $tagName, $offset )
    {
        $info = false;
        $params = trim( substr( $tag, strlen( $tagName ) + 1 ) );

        $pre_code = '';
        $post_code = '';
        $new_code = '<' . $tag . '>';

        $addLineFeed = ( ! isset( $this->dontAddLineFeedAfterCloseTag[ $tagName ] ) );

        $info = array(
            'post_code'    => $post_code,
            'offset'       => $offset,
            'add_lineFeed' => $addLineFeed,
            );

        if( ! isset( $this->stackTags[ $tagName ] ) )
            $this->stackTags[ $tagName ] = array( $info );
        else array_push( $this->stackTags[ $tagName ], $info );

        $return = $pre_code . $new_code;

        $this->completeBufferTemp( $return );

        $this->stackLevel++;
    }

    protected function closeHtmlTag( $tag, $tagName, $returnTag = true )
    {
        if( $returnTag )
            $return = '</'.$tagName.'>';
        else $return = '';

        if( isset( $this->stackTags[ $tagName ] ) )
        {
            if( $info = array_pop( $this->stackTags[ $tagName ] ) )
            {
                if( $info['add_lineFeed'] )
                    $return .= "\n";

                $return .= $info['post_code'];
            }
            if( empty( $this->stackTags[ $tagName ] ) )
                unset( $this->stackTags[ $tagName ] );
        }

        $this->stackLevel--;

        if( $return !== '' )
            $this->completeBufferTemp( $return );
    }

    public function getBuffer()
    {
        return $this->bufferCurrent ;
    }
    protected function completeBufferTemp( $data )
    {
        if( $this->stackLevel > 0 )
        {
            $indent = str_repeat( '  ', $this->stackLevel );

//            $data = str_replace( "\n", "\n" . $indent, $data );
        }

        $this->bufferTemp .= $data;
    }
    protected function commitBufferTemp()
    {
        $this->bufferCurrent = $this->bufferTemp ;
        $this->bufferTemp = '';
    }

*/

}

class kore_minify_xhtmlException extends Exception
{
    protected $bufferOffset = -1;

    public function __construct( $message, $offset )
    {
        $this->bufferOffset = $offset ;
        parent::__construct( $message, 0 );
    }

    public function getbufferOffset()
    {
        return $this->bufferOffset ;
    }
}