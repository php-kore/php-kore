<?php

class kore_minify_php
{
    const SINGLE_CHAR = 'a';

    protected $contents = '';
    protected $minifiedContents = NULL;
    protected $done = false;

    protected $dataMinifier = NULL;

    public $symbolTokens = array(
            T_AND_EQUAL, T_ARRAY_CAST,
            T_BOOLEAN_AND, T_BOOLEAN_OR,
            T_BOOL_CAST, T_CLOSE_TAG,
            T_CONCAT_EQUAL, T_DEC,
            T_DIV_EQUAL,
            T_DOLLAR_OPEN_CURLY_BRACES, T_DOUBLE_ARROW,
            T_DOUBLE_CAST, T_DOUBLE_COLON, T_INC,
            T_INT_CAST, T_IS_EQUAL, T_IS_GREATER_OR_EQUAL,
            T_IS_IDENTICAL, T_IS_NOT_EQUAL,
            T_IS_NOT_IDENTICAL, T_IS_SMALLER_OR_EQUAL,
            T_MINUS_EQUAL, T_MOD_EQUAL, T_MUL_EQUAL,
            T_OBJECT_CAST, T_OBJECT_OPERATOR,
            T_OPEN_TAG, T_OPEN_TAG_WITH_ECHO,
            T_OR_EQUAL, T_PAAMAYIM_NEKUDOTAYIM, T_PLUS_EQUAL,
            T_SL, T_SL_EQUAL, T_SR, T_SR_EQUAL, T_START_HEREDOC,
            T_STRING_CAST, T_UNSET_CAST, T_XOR_EQUAL,
            );

    protected $openTag = NULL;
    protected $openTagWithEcho = NULL;

    public static function minify( $contents )
    {
        $minifier = new kore_minify_php( $contents );
        return $minifier->getMinified();
    }

    public function __construct( $contents )
    {
        /**
         * Converti le contenu au format UNIX directement.
         */
        $this->contents = str_replace(array("\r\n", "\r"), "\n", $contents);

        $this->symbolTokens = array_flip( $this->symbolTokens );

        $this->useShortTags(false);
    }

    public function useShortTags( $use = true )
    {
        if( $use ) {
            $this->openTag = '<? ';
            $this->openTagWithEcho = '<?=';
        } else {
            $this->openTag = '<?php ';
            $this->openTagWithEcho = '<?php echo ';
        }
    }

    public function getMinified()
    {
        $this->minification();

        return $this->minifiedContents;
    }

    public function setDataMinifier( $minifier )
    {
        $this->dataMinifier = $minifier;
    }

    protected function minification()
    {
        if( $this->done === true )
            return;
        $this->done = true;

        $bench = kore::$debug->benchInit('minifyPHP', 'minification');

        /**
         * Supprime quelques tags PHP superflus.
         */
        $this->contents = str_replace(
            array(
                '<?php echo ',
                '} ?'.'>'.$this->openTag,
                '{ ?'.'>'.$this->openTag,
                '} ?'.'>'."\n".$this->openTag,
                '{ ?'.'>'."\n".$this->openTag,
                '?'.'>'.$this->openTag,
                '?'.'>'."\n".$this->openTag,
                ),
            array(
                $this->openTagWithEcho,
                '}',
                '{',
                '}'."\n",
                '{'."\n",
                ';',
                ';'."\n",
                ),
            $this->contents);

        /**
         * Remplace le contenu courant par la version "tokenizée".
         * N'utilise pas de seconde variable afin de limiter la consommation
         * mémoire.
         */
        $this->contents = token_get_all($this->contents);

        $previousTokenType = NULL;

        foreach( $this->contents as $idx => $token ) {

            if( is_array($token) ) {
                list($tokenType, $tokenString) = $token;
            } else {
                $tokenType = self::SINGLE_CHAR;
                $tokenString = $token;
            }

            switch( $tokenType ) {
                case T_COMMENT:
                    // Ignore les commentaires
                    break;
                case T_OPEN_TAG:
                    $this->minifiedContents .= $this->openTag;
                    break;
                case T_OPEN_TAG_WITH_ECHO:
                    $this->minifiedContents .= $this->openTagWithEcho;
                    break;
                case T_WHITESPACE:
                    if( ! ( $this->isSymbol($previousTokenType) or
                        ( isset($this->contents[$idx+1]) and
                          $this->isSymbol($this->contents[$idx+1]) ) ) )

                        $this->minifiedContents .= ' ';
                    break;
                case T_INLINE_HTML:
                    if( $this->dataMinifier !== NULL )
                        $tokenString = $this->dataMinifier->add( $tokenString );
                    // no break
                case self::SINGLE_CHAR:
                    // no break
                default:
                    $this->minifiedContents .= $tokenString;
            }

            $previousTokenType = $tokenType;
        }

    }

    protected function isSymbol( $token )
    {
        if( is_string($token) )
            return true;

        // si $token est un entier, alors on a le token type
        if( is_int($token) )
            return isset($this->symbolTokens[$token]);

        if( is_array($token) )
            return isset($this->symbolTokens[$token[0]]);

        return true;
    }
}
