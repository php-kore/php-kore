<?php
/**
 * Réduit la taille d'un fichier CSS {@link kore_minify_stylesheet}.
 *
 * @package minify
 */


/**
 * Réduit la taille d'un fichier CSS, sans en perdre le fonctionnement.
 *
 * Attention toutefois : tous les commentaires seront supprimés, y compris
 * les "hacks" IE 5. Préférez les commentaires conditionnels pour ce genre
 * de choses.
 *
 * La compression est uniquement syntaxique, et pas poussée au maximum :
 * par exmeple les sauts de lignes entre les différents tags sont conservés
 * afin de garder un minimum de lisibilité.
 *
 * @package minify
 */
class kore_minify_stylesheet
{
    /**
     * Réduit la taille des données.
     *
     * @param string $data  Contenu à réduire
     * @return string       Contenu réduit
     */
    public static function minify( $data )
    {
        // Conversion UNIX
        $data = str_replace( array( "\r\n", "\r" ), "\n", $data );

        // Suppression des espaces en trop
        $data = preg_replace(
                    array(
                        '#/\\*.*\\*/#sU',
                        '#\\s*\\{\\s*#',
                        '#\\s*;\\s*#',
                        '#\\s*;?\\s*\\}#',
                        '#\\s*,\\s*#',
                        '#:\\s+#',
                        '#([\\s:]+)0\\.#',
                        '#([\\s:]+)(0)\\s*(px|em|%|in|cm|mm|pc|pt|ex)#',

                        '#[ \\t]+$#m',
                        '#^[ \\t]+#m',
                        '#[ \\t]+#',
                        '#\\n+#',
                        ),
                    array(
                        '',
                        '{',
                        ';',
                        '}',
                        ',',
                        ':',
                        '$1.',
                        '$1$2',

                        '',
                        '',
                        ' ',
                        "\n",
                        ),
                    $data );

        // Réduction des codes couleurs
        $data = preg_replace_callback(
                    '@#([a-fA-F0-9]{6})([ ;}])@',
                    create_function(
                        '$match',
                        '$c=strtolower($match[1]);
                         if($c{0}===$c{1} and
                            $c{2}===$c{3} and
                            $c{4}===$c{5}){
                            $c = $c{0}.$c{2}.$c{4};
                         }
                         return \'#\'.$c.$match[2];'
                        ),
                    $data );

        return trim( $data );
    }

}