<?php
/**
 * Réduit la taille d'un fichier JS {@link kore_minify_javascript}.
 *
 * @package minify
 */


/**
 * Réduit la taille d'un fichier JS, sans en perdre le fonctionnement.
 *
 * La compression est uniquement syntaxique, et pas poussée au maximum :
 * les noms des variables et autres objets internes sont conservés tels
 * quels. De plus des sauts de ligne sont volontairement conservés afin
 * d'éviter d'avoir 10'000 caractères sur une seule ligne.
 *
 * Attention le parseur est plutôt rudimentaire, et bien qu'aucune limitation
 * n'ait été trouvée pour le moment il se peut qu'il y en ait.
 *
 * @package minify
 */
class kore_minify_javascript
{
    /**
     * Nombre de caractères maximal par ligne.
     */
    const LF_TRIGGER = 3072;
    const MAX_LEN = 4095;

    const INTERNAL_OPTIONAL_SPACE = '';
    const INTERNAL_OPTIONAL_RETURN = "\r";
    const INTERNAL_MANDATORY_SPACE = ' ';
    const INTERNAL_MANDATORY_RETURN = "\n";

    protected $done = false;
    protected $contents = '';
    protected $minifiedContents = NULL;

    protected $emptyToken   = NULL;

    protected $currentToken = NULL;
    protected $previousToken = NULL;

    protected $charsOnLine = 0;

    protected $firstToken   = NULL;
    protected $tree = array();
    protected $currentTokenTree = NULL;
    protected $isPrimary = true;
    protected $stackPrimary = array();

    protected $keywords = array( 'return', 'if', 'function', 'while', 'new' );

    public static function minify( $contents )
    {
        $minifier = new kore_minify_javascript( $contents );
        return $minifier->getMinified();
    }

    public function __construct( $contents )
    {
        /**
         * Converti le contenu au format UNIX directement.
         */
        $this->contents = str_replace( array( "\r\n", "\r" ), "\n", trim( $contents ) );

        $this->keywords = array_flip( $this->keywords );

        $this->firstToken = new stdClass();
        $this->firstToken->type = 'root';
        $this->firstToken->tree = array();

        $this->emptyToken = new stdClass();
        $this->emptyToken->type = NULL;

        $this->previousToken = $this->emptyToken;

        $this->currentToken = $this->firstToken;
        $this->currentTokenTree = $this->firstToken;
    }

    public function getMinified()
    {
        $this->minification();

        return $this->minifiedContents;
    }

    /**
     * Réduit la taille des données.
     */
    public function minification()
    {
        if( $this->done === true )
            return;
        $this->done = true;

        /**
         * Pour améliorer les performances, fait un premier découpage
         * en regroupant les caractères alphanumériques.
         *
         * Par contre la consommation mémoire est fortement augmentée.
         */
        $bench = kore::$debug->benchInit( 'split' );
        $this->contents = preg_split( '#([^a-zA-Z0-9$_])#', $this->contents,
            -1, PREG_SPLIT_DELIM_CAPTURE | PREG_SPLIT_NO_EMPTY );
        unset( $bench );

        $maxIdx = count( $this->contents ) - 1;
        foreach( $this->contents as $i => $char )
        {
            if( $this->currentToken->type === 'comment' )
            {
                if ( ( $char === "\n" and
                       $this->currentToken->limit === 'inline' )
                    or
                     ( $char === '/' and
                       $this->currentToken->lastChar === '*' ) ) {

                    $this->nextToken();

                } else
                    $this->currentToken->lastChar = $char;

                continue;
            }
            elseif( $this->currentToken->type === 'string' )
            {
                $this->currentToken->string .= $char;

                if( ( $char === $this->currentToken->limit )
                    and ( ! $this->currentToken->escaped ) )
                {
                    unset( $this->currentToken->limit );
                    unset( $this->currentToken->escaped );

                    $this->nextToken();
                }
                elseif( $char === '\\' )
                {
                    if( $this->currentToken->escaped )
                        $this->currentToken->escaped = false;
                    else $this->currentToken->escaped = true;
                }
                else $this->currentToken->escaped = false;
            }
            elseif( strlen( $char ) > 1 )
            {
                $this->addCharToToken( 'word', $char );
            }
            elseif( $char === '\'' or $char === '"' )
            {
                $this->addToken( 'string', $char );
                $this->currentToken->limit   = $char;
                $this->currentToken->escaped = false;
            }
            elseif( $char === '/' )
            {
                if( $i < $maxIdx )
                {
                    $nextChar = $this->contents[ $i+1 ];
                    if( $nextChar === '/' ) {
                        $this->addCommentToken( 'inline' );
                        continue;
                    } elseif( $nextChar === '*' ) {
                        $this->addCommentToken( 'block' );
                        continue;
                    }
                }

                /**
                 * Est ce une division ou une expression régulière ?
                 */
                $this->nextToken();

                if( ! $this->isPrimary ) {
                    $this->addCharToToken( 'maths', '/' );
                } else {
                    $this->addToken( 'string', $char );
                    $this->currentToken->limit   = $char;
                    $this->currentToken->escaped = false;
                }
            }
            elseif( $char === '{' ) {
                $this->increaseLevel( '}' );
                $this->addToken( 'openBloc', $char );

            } elseif( $char === '}' ) {
                $this->addToken( 'closeBloc', $char );
                $this->decreaseLevel( $char );

            } elseif( $char === '(' ) {
                $this->increaseLevel( ')' );
                $this->addToken( 'openBloc', $char );

            } elseif( $char === ')' ) {
                $this->addToken( 'closeBloc', $char );
                $this->decreaseLevel( $char );

            } elseif( $char === '[' ) {
                $this->increaseLevel( ']' );
                $this->addToken( 'openBloc', $char );

            } elseif( $char === ']' ) {
                $this->addToken( 'closeBloc', $char );
                $this->decreaseLevel( $char );

            } elseif( $char === ';' ) {
                $this->addToken( 'separator', $char );

            } elseif( $char === '-' or $char === '+' or $char === '*'
                    or $char === '/' or $char === '%' or $char === '^' ) {
                $this->addCharToToken( 'maths', $char );

            } elseif( self::isAlphaNum( $char ) ) {

                $this->addCharToToken( 'word', $char );

            } elseif( self::isSpace( $char ) ) {

                $this->addCharToToken( 'space', $char );

            } else {
                $this->addCharToToken( 'otherSymbol', $char );
            }

            unset( $this->contents[ $i ] );
        }

        /**
         * Bloc non compressé ? Alors il s'agit d'une erreur.
         */
        if( count( $this->tree ) > 0 ) {
            throw new kore_minify_javascriptException( "end not found",
                kore_minify_javascriptException::PARSE_ERROR );
        }

        $this->compressBranch( $this->firstToken );
        $this->minifiedContents .= $this->firstToken->string;
        $this->minifiedContents = trim(str_replace(
            self::INTERNAL_OPTIONAL_RETURN, '', $this->minifiedContents));
    }

    function nextToken( )
    {
        if( $this->currentToken->type === 'comment' )
            $this->currentToken = $this->previousToken;

        elseif( $this->currentToken !== $this->emptyToken ) {

            $this->previousToken = $this->currentToken;
            $this->currentToken = $this->emptyToken;

            switch( $this->previousToken->type ) {
                case 'separator':
                case 'maths':
                case 'otherSymbol':
                    $this->isPrimary = true;
                    break;
                case 'word':
                    $this->isPrimary = isset( $this->keywords[
                        strtolower($this->previousToken->string) ] );
                    break;
            }
        }
    }

    function addCommentToken( $limit )
    {
        $this->nextToken();

        $newToken = new stdClass();
        $newToken->type = 'comment';
        $newToken->limit = $limit;
        $newToken->lastChar = $limit;
        $this->currentToken = $newToken;
    }

    function addToken( $type, $string = NULL )
    {
        $this->nextToken();

/*
        if( $this->previousToken->type === 'closeBloc'
            and $this->previousToken->string === '}'
            and $type !== 'separator' and $type !== 'otherSymbol' )
        {
            $this->addToken( 'separator', ';' );
        }
*/

        $newToken = new stdClass();
        $newToken->type = $type;
        if ( $string !== NULL )
            $newToken->string = $string;
        $this->currentToken = $newToken;

        array_push($this->currentTokenTree->tree, $newToken);

        if( $type === 'separator' )
        {
            if( count( $this->tree ) === 0 ) {
                $this->compressBranch( $this->currentTokenTree );
                $this->minifiedContents .= $this->currentTokenTree->string;

                $this->currentTokenTree->tree = array();
                $this->currentTokenTree->string = NULL;
            }
        }
    }
    protected function addCharToToken( $type, $char )
    {
        if( $this->currentToken->type !== $type )
            $this->addToken( $type );

        $this->charsOnLine += strlen( $char );

        if( !isset( $this->currentToken->string ) )
            $this->currentToken->string = $char;
        else $this->currentToken->string .= $char;
    }

    protected function increaseLevel( $closeChar )
    {
        array_push( $this->tree, $this->currentTokenTree );

        $this->addToken( 'node' );
        $this->currentToken->closeChar = $closeChar;
        $this->currentToken->tree = array();
        $this->currentTokenTree = $this->currentToken;

        $this->isPrimary = true;
    }
    protected function decreaseLevel( $closeChar )
    {
        if( $this->currentTokenTree->closeChar !== $closeChar )
        {
            throw new kore_minify_javascriptException(
                "unexpected char '$closeChar' found, '".
                $this->currentTokenTree->closeChar."' needed",
                kore_minify_javascriptException::PARSE_ERROR );
        }

        $this->compressBranch( $this->currentTokenTree );

        $this->currentTokenTree = array_pop( $this->tree );
        if( $this->currentTokenTree === NULL )
        {
            throw new kore_minify_javascriptException( "invalid bloc structure",
                kore_minify_javascriptException::PARSE_ERROR );
        }

        $this->isPrimary = false;
    }

    protected function compressBranch( $node )
    {
//        $bench = kore::$debug->benchInit( 'compressBranch' );

        $newString = '';

        /**
         * Première passe, supprime les éléments inutiles,
         * mais ne touche pas aux espaces.
         */
        $previousToken = $this->emptyToken;
        $previousRealToken = $this->emptyToken;
        $listTokens = array();
        foreach ( $node->tree as $idx => $token ) {

            /**
             * Supprime les ";" avant les accolades fermantes
             */
            if( $token->type === 'closeBloc' and $token->string === '}'
                and $previousRealToken->string === ';' ) {

//                if( $previousToken->string === 'space' )
//                    array_pop( $listTokens );

                while( array_pop( $listTokens )->type === 'space' );

            }

            $previousToken = $token;
            if( $token->type !== 'space' )
                $previousRealToken = $token;
            array_push( $listTokens, $token );
        }

        /**
         * Seconde passe, concaténation.
         */
        $previousToken = $this->emptyToken;
        $maxIdx = count( $listTokens ) - 1;
        foreach ( $listTokens as $idx => $token ) {
            if( $idx === $maxIdx )
                $nextToken = $this->emptyToken;
            else $nextToken = $listTokens[ $idx+1 ];

            if( $token->type === 'space' ) {
                $haveReturn = ( strpos($token->string,"\n") !== false );

                if ( $previousToken->type === NULL or
                     $nextToken->type === NULL ) {

                    $token->string = self::INTERNAL_OPTIONAL_SPACE;
                } else {
                    $pChar = substr( $previousToken->string, -1 );
                    $nChar = substr( $nextToken->string, 0, 1 );

                    $previous = ( self::isAlphaNum( $pChar )
                                or ( $previousToken->type === 'string' ) );
                    $next = ( self::isAlphaNum( $nChar )
                            or ( $nextToken->type === 'string' ) );

                    /**
                     * Deux alphanumériques.
                     */
                    if( $previous and $next ) {

                        $token->string = self::INTERNAL_MANDATORY_SPACE;

                    } elseif ( $previousToken->type === 'maths' and
                        $nextToken->type === 'maths' and
                        ( $pChar === '+' or $pChar === '-' ) and
                        ( $nChar === '+' or $nChar === '-' ) ) {

                        $token->string = self::INTERNAL_MANDATORY_SPACE;

                    } else $token->string = self::INTERNAL_OPTIONAL_SPACE;
                }

                if( $haveReturn )
                {
                    if( $token->string === self::INTERNAL_MANDATORY_SPACE )
                         $token->string = self::INTERNAL_MANDATORY_RETURN;
                    else $token->string = self::INTERNAL_OPTIONAL_RETURN;
                }
            }

            $newString .= $token->string;
            $previousToken = $token;
        }
        $node->type = 'compact';
        unset( $node->tree );
        $node->string = $newString;
    }

    protected static function isSpace( $char )
    {
        return ( strspn( $char, " \t\n" ) > 0 );
    }

    protected static function isAlphaNum( $char )
    {
        return preg_match( '#[[:alnum:]$_]#', $char );
    }
}

class kore_minify_javascriptException extends Exception
{
    const PARSE_ERROR = 1;
}
