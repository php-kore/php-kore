<?php
/**
 * Class to manage a specific route, from kore_route_router
 *
 * @package route
 */
class kore_route_route
{
    public $host;
    public $url;
    public $uri;
    public $type;

    public $name;
    public $rule;
    public $callback;
    public $params = array();

    private $_reverse;

    /**
     * Setup the object, via kore_route_router::$_directRules
     *
     * @param  string $host
     * @param  string $url
     * @param  string $uri
     * @param  string $rule
     * @param  mixed  $callback
     * @return kore_route_route
     */
    static public function fromDirect($host, $url, $uri, $rule, $callback)
    {
        $obj = new static;
        $obj->type = 'direct';
        $obj->host = $host;
        $obj->url = $url;
        $obj->uri = $uri;

        $obj->rule = $rule;
        $obj->callback = $callback;

        $obj->_reverse = $obj->url;

        return $obj;
    }

    /**
     * Setup the object, via kore_route_router::$_pregRules
     *
     * @param  string $host
     * @param  string $url
     * @param  string $uri
     * @param  string $ruleName
     * @param  string $rule
     * @param  mixed  $callback
     * @param  array  $params
     * @param  string $params
     * @return kore_route_route
     */
    static public function fromPreg($host, $url, $uri, $ruleName, $rule, $callback, $params, $reverse='')
    {
        $obj = new static;
        $obj->type = 'preg';
        $obj->host = $host;
        $obj->url  = $url;
        $obj->uri = $uri;

        $obj->name = $ruleName;
        $obj->rule = $rule;
        $obj->callback = $callback;

        /*
         * keep only named subpattern
         */
        foreach ($params as $name => $value) {
            if (is_string($name))
                $obj->params[$name] = $value;
        }

        $obj->_reverse = $reverse;

        return $obj;
    }

    /**
     * Setup the object, via kore_request_http
     *
     * @param  array  $params
     * @return kore_route_route
     */
    static public function fromRequest($params = null)
    {
        $obj = new static;
        $obj->type = 'direct';
        $obj->host = kore_request_http::getHTTPHost();
        $obj->url  = kore_request_http::getUrl();
        $obj->uri  = kore_request_http::getUri();

        $obj->_reverse = $obj->url;

        if ($params === null)
            $params = $_GET;
        $obj->params = $params;

        return $obj;
    }

    /**
     * Called by kore_route_router::getRoute(), to setup a route after direct
     * instanciation (probably for reverse routing).
     */
    public function init()
    {
    }

    /**
     * Apply the routing to the defined callback.
     *
     * @return mixed
     */
    public function route()
    {
        $callback = $this->callback;
        if (is_string($callback)) {
            /*
             * Manage dynamic callback names.
             */

            if (isset($this->params['ctrl']))
                $callback = str_replace('{CTRL}', $this->params['ctrl'],
                        $callback);
            elseif (isset($this->params['file']))
                $callback = str_replace('{FILE}', $this->params['file'],
                        $callback);
            /*
             * By security, we always add this suffix
             */
            $callback .= 'Action';
        }

        kore::$debug->benchCheckPoint('main', 'controller');
        return call_user_func($callback, $this);
    }

    /**
     * Set route parameter
     *
     * @param string $param
     * @param mixed $value
     * @throws Exception
     */
    public function set($param,$value)
    {
        if (is_object($value)) {

            if(method_exists($value, 'toRoute'))
                $this->params[$param] = (string) $value->toRoute();
            else $this->params[$param] = (string) $value;

        } else {
            $this->params[$param] = $value;
        }

        return $this;
    }

    /**
     * Remove route parameter :
     *     it's just a shortcut with null value
     *
     * @param string $param
     * @throws Exception
     */
    public function remove($param)
    {
        $this->set($param,NULL);
        return $this;
    }

    /**
     * Generate url from route
     *
     * @return string
     */
    public function reverse()
    {
        /*
         * Initilizing url with rule
         */
        $url = $this->_reverse;

        /*
         * parsing parameters
         */
        $params = array();
        foreach($this->params as $name => $value) {
            if ($value!==NULL and $value!=="") {
                $newUrl = preg_replace("`\\{".$name."\\}\\??`", rawurlencode($value), $url);
                /*
                 * if param is not inner url params, it's probably a querystring parameter
                 */
                if ($newUrl===$url)
                    $params[$name]=$value;

                $url = $newUrl;
            }
        }

        /*
         * Finish cleaning if there is optional parameters
         */
        $url = preg_replace('`(/|\\.|)\\{\w*\\}\\?`','',$url);

        /*
         * Check if it remains parameters which have not been replaced
         */
        if (preg_match('`\\{.*?\\}`', $url, $arg) > 0)
            throw new kore_route_exception("route parameters were not all parsed",
                kore_route_exception::MISSING_PARAMETER);

        /*
         * Append other parameters to url
         * by default http_build_query use RFC 1738 (like urlencode)
         */
        $otherParameters = http_build_query($params);

        if ($otherParameters!=='')
            $url.='?'.$otherParameters;

        return $url;
    }

    /**
     * Handle pagination to the current route, via kore_pagination.
     *
     * @param  integer $maxItemsPerPage
     * @return kore_pagination
     */
    public function addPagination($maxItemsPerPage = null)
    {
        $pagination = kore_pagination::fromRoute($this);

        if ($maxItemsPerPage !== null)
            $pagination->setMaxPerPage($maxItemsPerPage);

        return $pagination;
    }
}
