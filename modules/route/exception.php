<?php
/**
 * Exceptions throws by kore_route_route, while processing reverse routing.
 *
 */
class kore_route_exception extends Exception
{
    const MISSING_PARAMETER = 1;
}