<?php
/**
 * Generic controller, which provide some standard methods.
 *
 * @package route
 */
class kore_route_controller
{
    const M_HEAD =    0x01,
          M_GET  =    0x02,

          M_POST =    0x04,
          M_PUT  =    0x08,
          M_DELETE =  0x10,

          M_OPTIONS = 0x20,
          M_TRACE =   0x40,
          M_CONNECT = 0x80,

          M_ALL =     0xFF;

    /**
     * Convert a method string to a method constant.
     *
     * @param  string $methodString
     * @return integer
     */
    static public function _getMethod($methodString)
    {
        static $methods = array(
                'HEAD'    => self::M_HEAD,
                'GET'     => self::M_GET,
                'POST'    => self::M_POST,
                'PUT'     => self::M_PUT,
                'DELETE'  => self::M_DELETE,
                'OPTIONS' => self::M_OPTIONS,
                'TRACE'   => self::M_TRACE,
                'CONNECT' => self::M_CONNECT,
                );

        return @$methods[$methodString];
    }

    /**
     * Check that the request method is in given list of methods.
     * Otherwise throws a "forbidden" exception.
     *
     * @param  integer $methodsFlags
     * @throws kore_response_httpStatus
     */
    static public function allowMethods($methodsFlags)
    {
        $currentMethods = self::_getMethod($_SERVER['REQUEST_METHOD']);

        if ($currentMethods & $methodsFlags === 0)
            kore_response_httpStatus::Forbidden403();
    }

    /**
     * Check that the request method is NOT in given list of methods.
     * Otherwise throws a "forbidden" exception.
     *
     * @param  integer $methodsFlags
     * @throws kore_response_httpStatus
     */
    static public function denyMethods($methodsFlags)
    {
        $currentMethods = self::_getMethod($_SERVER['REQUEST_METHOD']);

        if ($currentMethods & $methodsFlags > 0)
            kore_response_httpStatus::Forbidden403();
    }

    /**
     * If we try to call one static method wich don't exists we throw one
     * not found 404 error (in prod & test mode) or one errorException in
     * dev mode
     *
     * @param string $name
     * @param array $arguments
     */
    public static function __callStatic($name, $arguments)
    {
        if(kore::$debug->getEnvironment() === 'dev') {
            $class = get_called_class();
            throw new ErrorException("Can't call this callback $class::$name");
        }

        throw kore_response_httpStatus::NotFound404();
    }

}