<?php
/**
 * Class to manage reponses of type HTTP redirects as other responses.
 */
class kore_response_httpRedirect
{
    /**
     * HTTP return code.
     *
     * @var integer
     */
    protected $_code;

    /**
     * URI target of the redirect.
     *
     * @var string
     */
    protected $_target;

    /**
     * Setup a redirection of type «HTTP 302 Found»
     *
     * @param  string $target
     * @return kore_response_httpRedirect
     */
    static public function found($target)
    {
        return new static(302, $target);
    }

    /**
     * Setup a redirection of type «HTTP 301 Move Permanently»
     *
     * @param  string $target
     * @return kore_response_httpRedirect
     */
    static public function permanently($target)
    {
        return new static(301, $target);
    }

    /**
     * Setup a redirection of type «HTTP 307 Temporary Redirect»
     *
     * @param  string $target
     * @return kore_response_httpRedirect
     */
    static public function temporarily($target)
    {
        return new static(307, $target);
    }

    /**
     * Setup the object
     *
     * @param string  $code
     * @param integer $target
     */
    public function __construct($code, $target)
    {
        $this->_code = $code;

        if ($target instanceOf kore_route_route)
            $this->_target = $target->reverse();
        else
            $this->_target = (string) $target;
    }

    /**
     * Return the wanted HTTP code
     *
     * @return integer
     */
    public function getCode()
    {
        return $this->_code;
    }

    /**
     * Return target of the redirection.
     *
     * @return string
     */
    public function getTarget()
    {
        return $this->_target;
    }

    /**
     * Send the response (so, throw the redirection), and stop execution.
     */
    public function send()
    {
        kore_response_http::redirect($this->_target, $this->_code);
    }
}