<?php
/**
 * Préparation de la réponse via le moteur de template
 *
 * @package response
 *
 * @global boolean kore::$conf->response_autoBuildEtag
 *
 */

/**
 * Préparation de la réponse HTML via le moteur de template
 *
 * @package response
 */
class kore_response_template
{

    /**
     * Initialise l'interprétation du template :
     * - termine la session
     * - ferme toutes les instances de base de données
     * - génère un ETag en fonction des variables du template
     * - envoi les entêtes HTTP
     * - et finalement affiche le template.
     *
     * @param kore_template  $template
     */
    public function send( kore_template $template )
    {
        kore::$debug->benchCheckPoint('main', 'response');

        /**
         * C'est le template qui décide du charset & du content-type
         */
        kore::$conf->response_charset = $template->config->charset;
        kore::$conf->response_contentType = $template->config->contentType;

        if( kore_response_http::getEtag() === NULL and
            kore::$conf->get('response_autoBuildEtag', true) and
            function_exists('hash_init') ) {

            kore_response_http::setEtag($template->buildContextHash());
        }

        if( kore::$conf->get('response_autoCloseSession', true) )
            kore::$session->close();

        if( kore::$conf->get('response_autoCloseDB', true) )
            kore::$db->closeAll();

        kore_response_http::start();

        $template->display();

        /*
         * Si on utilise php-fpm, s'en sert pour terminer le script dès que
         * possible.
         */
        if (function_exists('fastcgi_finish_request'))
            fastcgi_finish_request();
        else
            flush();
    }

}