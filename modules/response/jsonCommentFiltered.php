<?php
/**
 * Préparation de la réponse au format "json-comment-filtered"
 * (cf: dojoToolkit)
 *
 * @package response
 */

/**
 * Prépare la réponse au format "json-comment-filtered" (cf: dojoToolkit).
 *
 * Lors de l'appel de send() la session est fermée, et les
 * entêtes HTTP sont envoyés.
 *
 * @package response
 */
class kore_response_jsonCommentFiltered extends kore_response_json
{
	/**
	 * Instanciation de la classe.
	 */
    public function __construct()
    {
        parent::__construct();

        kore::$conf->response_contentType = 'text/json-comment-filtered';
    }

    /**
     * Envoi les données
     */
    protected function sendData( & $data )
    {
        echo '/*', $data, '*/';
    }

}