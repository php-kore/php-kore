<?php
/**
 * Classe permettant de stocker une collection d'objets, de la manipuler comme
 * un tableau, et éventuellement d'associer des données à chacun de ces objets.
 *
 * A partir de PHP5.4 cette classe pourrait probablement hériter de
 * SplObjectStorage, à condition de surcharger le getHash() afin qu'il conserve
 * le comportement actuel, et en supposant que PHP s'en serve en interne.
 */
class kore_collection implements IteratorAggregate, Countable, ArrayAccess
{
    /**
     * Classe commune à tous les objets de la collection.
     */
    const CLASS_ITEM = null;

    /**
     * Tableau contenant les objets.
     *
     * @var array
     */
    protected $_list = array();

    /**
     * Tableau contenant des données associées aux items.
     *
     * @var array
     */
    protected $_data;

    /**
     * Retourne une instance de kore_collection, éventuellement initialisée
     * depuis un tableau.
     *
     * @param  array $objList
     * @return kore_collection
     */
    public function __construct( array $objList = null )
    {
        if ($objList !== null)
            $this->_list = $objList;
    }

    /**
     * Retourne une chaine identifiant l'objet. Dans le cas de
     * kore_db_mapping_element, on va utiliser le résultat de getKey().
     * Dans les autres cas c'est spl_object_hash() qui sera utilisé.
     *
     * @param  object $object
     * @return mixed
     */
    public function getHash($object)
    {
        if ($object instanceOf kore_db_mapping_element)
            return $object->getKey();

        return spl_object_hash($object);
    }

    /**
     * Retourne les données associées à l'objet courant, ou bien null si aucune
     * donnée n'est associée.
     *
     * @return mixed
     */
    public function getInfo()
    {
        $object = current($this->_list);
        if ($object === false)
            return null;

        return $this->_getData($object);
    }

    /**
     * Retourne les données associées à un objet précis.
     *
     * @param  object $object
     * @return mixed
     */
    private function _getData($object)
    {
        if ($this->_data === null)
            return null;

        $hash = $this->getHash($object);
        if (!array_key_exists($hash, $this->_data))
            return null;

        return $this->_data[$hash];
    }

    /**
     * Vérifie que l'objet soit bien une instance de static::CLASS_ITEM.
     *
     * @param  object $object
     * @throws InvalidArgumentException
     */
    protected function _verifyClass($object)
    {
        $class = static::CLASS_ITEM;

        if (!is_object($object) or ($class and !($object instanceOf $class)))
            throw new InvalidArgumentException(get_class($this)." only accept object of class ".static::CLASS_ITEM);
    }

    /**
     * Inverse l'ordre de la collection.
     */
    public function reverse()
    {
        $this->_list = array_reverse($this->_list, true);
    }

    /**
     * Tri la collection, grâce à une fonction utilisateur (ou une fonction
     * anonyme).
     *
     * @param callable $callback
     */
    public function sortBy(/*callable*/ $callback)
    {
        uasort($this->_list, $callback);
    }

    /**
     * Indique si la collection contient l'item en question.
     *
     * @param  object $object
     * @return boolean
     */
    public function contains( $object )
    {
        $this->_verifyClass($object);

        return array_key_exists($this->getHash($object), $this->_list);
    }

    /**
     * Ajoute un item à la collection, et y associe éventuellement des données.
     *
     * @param object $object
     * @param mixed  $data
     */
    public function attach( $object, $data = null )
    {
        $this->_verifyClass($object);

        $hash = $this->getHash($object);
        $this->_list[$hash] = $object;

        if ($data !== null){
            if ($this->_data === null)
                $this->_data = array();
            $this->_data[$hash] = $data;
        }
    }

    /**
     * Retire un item de la collection.
     *
     * @param object $object
     */
    public function detach( $object )
    {
        $this->_verifyClass($object);
        $hash = $this->getHash($object);
        unset($this->_list[$hash]);
        unset($this->_data[$hash]);
    }

    /**
     * Retourne le premier item de la collection (ou false si elle est vide).
     *
     * @return mixed
     */
    public function first()
    {
        return reset($this->_list);
    }

    /**
     * Retourne le dernier item de la collection (ou false si elle est vide).
     *
     * @return mixed
     */
    public function last()
    {
        return end($this->_list);
    }



    /*
     * Méthodes implémentant Traversable, ce qui permet de faire un foreach sur
     * la collection.
     */

    public function getIterator()
    {
        return new ArrayIterator($this->_list);
    }


    /*
     * Méthodes implémentant Countable, ce qui permet de faire un count($list)
     */

    public function count()
    {
        return count($this->_list);
    }


    /*
     * Méthodes implémentant ArrayAccess, ce qui permet d'accéder aux objets
     * directement de cette façon : $list[$obj]
     */

    public function offsetExists($object)
    {
        return $this->contains($object);
    }

    /**
     * Retourne les *données associées* à l'objet indiqué.
     *
     * @param  object $object
     * @return mixed
     */
    public function offsetGet($object)
    {
        if (!$this->contains($object))
            throw new UnexpectedValueException("can't found that object in " . get_class($this));

        return $this->_getData($object);
    }

    public function offsetSet($object, $data)
    {
        $this->attach($object, $data);
    }

    public function offsetUnset($object)
    {
        $this->detach($object);
    }

    /**
     * Adds all objects-data pairs from a different storage in the current
     * storage.
     *
     * @param kore_collection $list
     */
    public function addAll(kore_collection $list)
    {
        foreach ($list as $object)
            $this->attach($object, $list[$object]);
    }

    /**
     * Removes objects contained in another storage from the current storage.
     *
     * @param kore_collection $list
     */
    public function removeAll(kore_collection $list)
    {
        foreach ($list as $object)
            $this->detach($object);
    }

    /**
     * Method to randomize order of elements in this collection.
     *
     * The PHP's method suffle() is not directly usable on kore_collection's
     * instances because keys are not kept, so methods like contains() or
     * offsetGet() don't work anymore.
     */
    public function shuffle()
    {
        $keys = array_keys($this->_list);
        shuffle($keys);

        $temporaryList = array();
        foreach($keys as $key)
            $temporaryList[$key] = $this->_list[$key];

        $this->_list = $temporaryList;
        unset($temporaryList);
    }

    /**
     * Converti la collection en simple tableau
     *
     * @return array
     */
    public function toArray()
    {
        return $this->_list;
    }

    /**
     * Return all items of the current collection which are not in the given one.
     *
     * @param  kore_collection $otherList
     * @return kore_collection
     */
    public function minus(kore_collection $otherList)
    {
        $result = new static;

        foreach ($this->_list as $key => $object){
            if (!$otherList->contains($object))
                $result->attach($object, $this->_getData($object));
        }

        return $result;
    }

    /**
     * Return all items of the current collection which are also present in the
     * given one.
     *
     * @param  kore_collection $otherList
     * @return kore_collection
     */
    public function intersect(kore_collection $otherList)
    {
        $result = new static;

        foreach ($this->_list as $key => $object){
            if ($otherList->contains($object))
                $result->attach($object, $this->_getData($object));
        }

        return $result;
    }

}
