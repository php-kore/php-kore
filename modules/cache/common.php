<?php

abstract class kore_cache_common
{
    /**
     * Initialisation de l'objet.
     */
    public function __construct()
    {
        kore::$conf->get('cache_defaultTTL', 3600);
        kore::$conf->get('cache_randomizeExpiration', 5);
    }

    /**
     * Retourne une donnée depuis le cache.
     *
     * @param  string  $key
     * @return mixed
     */
    abstract public function get($key);

    /**
     * Insert une donnée dans le cache.
     *
     * @param  string  $key
     * @param  mixed   $value
     * @param  int     $ttl
     * @return boolean
     */
    abstract public function set($key, $value, $ttl = null);

    /**
     * Supprime une donnée du cache.
     *
     * @param  string  $key
     * @return boolean
     */
    abstract public function delete($key);

    /**
     * Vide le cache.
     *
     * @return boolean
     */
    abstract public function deleteAll();

    /**
     * Vérifie l'existance d'une donnée dans le cache.
     *
     * @param  string  $key
     * @return boolean
     */
    public function exists($key)
    {
        return ( $this->get($key) !== false );
    }

    /**
     * Vérifie l'existance de plusieurs données.
     *
     * @param  array   $keys
     * @return array
     */
    public function multiExists(array $keys)
    {
        $results = array();
        foreach( $keys as $key )
            $results[$key] = $this->exists($key);
        return $results;
    }

    /**
     * Retourne plusieurs valeurs depuis le cache.
     *
     * @param  array   $keys
     * @return array
     */
    public function multiGet(array $keys)
    {
        $results = array();
        foreach( $keys as $key )
            $results[$key] = $this->get($key);
        return $results;
    }

    /**
     * Insert plusieurs valeurs en cache.
     *
     * @param  array   $values
     * @param  integer $ttl
     * @param  array
     */
    public function multiSet(array $values, $ttl = null)
    {
        $results = array();
        foreach( $values as $key => $value )
            $results[$key] = $this->set($key, $value, $ttl);
        return $results;
    }

    /**
     * Supprime plusieurs données du cache.
     *
     * @param  array   $values
     * @param  array
     */
    public function multiDelete(array $keys)
    {
        $results = array();
        foreach( $keys as $key )
            $results[$key] = $this->delete($key);
        return $results;
    }

    /**
     * Place une donnée en cache, seulement si elle n'y est pas déjà.
     *
     * @param  string  $key
     * @param  mixed   $value
     * @param  integer $ttl
     * @return boolean
     */
    public function add($key, $value, $ttl = null)
    {
        if( $this->exists($key) )
            return null;
        return $this->set($key, $value, $ttl);
    }

    protected function _getExpiredKey($key)
    {
        return $key .'.expired';
    }

    public function getExpired($key)
    {
        return $this->get($this->_getExpiredKey($key));
    }

    public function markAsExpired($key, $ttl = 600)
    {
        $newKey = $this->_getExpiredKey($key);
        if( ( $data = $this->get($key) ) === false )
            return false;

        $this->set($newKey, $data, $ttl);
        return $this->delete($key);
    }

    public function inc($key, $step = 1)
    {
        $newValue = $this->get($key) + $step;
        $this->set($key, $newValue);
        return $newValue;
    }

    public function dec($key, $step = 1)
    {
        $newValue = $this->get($key) - $step;
        $this->set($key, $newValue);
        return $newValue;
    }
}
