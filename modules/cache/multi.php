<?php
class kore_cache_multi extends kore_cache_common
{
    protected $_backends = array();

    public function __construct()
    {
        parent::__construct();

        $backends = func_get_args();
        if( $backends )
            $this->_backends = $backends;
    }

    public function addBackend(kore_cache_common $backend)
    {
        $this->_backends[] = $backend;
    }

    public function get($key, $ttl = null)
    {
        foreach( $this->_backends as $level => $backend ){
            if( ( $data = $backend->get($key) ) !== false ){

                if( $ttl and $level > 0 ){
                    foreach( $this->_backend as $level2 => $backend2 ){
                        if( $level2 === $level )
                            break;
                        $backend2->set($key, $data, $ttl);
                    }
                }

                return $data;
            }
        }

        return false;
    }

    public function set($key, $value, $ttl = null)
    {
        if( $ttl === null ) $ttl = kore::$conf->cache_defaultTTL;
        $nbBackends = count($this->_backends);

        foreach( $this->_backends as $level => $backend ){
            $localTtl = ($ttl >> ($nbBackends - $level -1));

            $backend->set($key, $value, $localTtl);
        }
    }

    public function multiSet(array $values, $ttl = null)
    {
        if( $ttl === null ) $ttl = kore::$conf->cache_defaultTTL;
        $nbBackends = count($this->_backends);

        $results = array();
        foreach( $this->_backends as $level => $backend ){
            $localTtl = ($ttl >> ($nbBackends - $level -1));

            foreach( $values as $key => $value ){
                $res = $backend->set($key, $value, $localTTl);
                if( $level === 0 )
                    $results[$key] = $res;
            }
        }
        return $results;
    }

    public function delete($key)
    {
        $backends = array_reverse($this->_backends);
        foreach( $backends as $backend )
            $backend->delete($key);
    }

    public function deleteAll()
    {
        $backends = array_reverse($this->_backends);
        foreach( $backends as $backend )
            $backend->deleteAll();
    }

}
