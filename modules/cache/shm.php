<?php

class kore_cache_shm implements kore_cache_common
{
    protected $_pools;
    protected $_semsR;

    public function __construct( $keyFile = null, $count = 4, $memsize = null, $perm = 0666 )
    {
        if( $keyFile === null ) $keyFile = __FILE__;

        $this->_pools = array();

        for( $id = 0; $id < $count; $id++ ){
            $idRes = ftok($keyFile, $id);
            $this->_pools[$id] = shm_attach($idRes, $memsize, $perm);
        }


    }

    protected function _getPool($key)
    {
    }

    protected function _getPoolRessource($key)
    {
        return $this->_pools[$this->_getPool($key)];
    }

    protected function _sortKeysByPool($keys)
    {
        $list = array();
        foreach( $keys as $key ){
            $p = $this->_getPool($key);
            if( !isset($list[$p]) )
                $list[$p] = array($key);
            else
                $list[$p][] = $key;
        }
        return $list;
    }

    public function exists($key)
    {
        if( !is_array($key) )
            return shm_has_var($this->_getPoolRessource($key), $key);

        $results = array();
        foreach( $this->_sortKeysByPool as $p => $keys ){
            foreach( $keys as $k )
                $results[$k] = shm_has_var($this->_pools[$p], $key);
        }

        return $results;
    }

    public function get($key)
    {


    }

    public function set($key, $value, $ttl = null);
    public function multiSet($values, $ttl = null);
    public function add($key, $value, $ttl = null);

    public function delete($key);
    public function deleteAll();

    public function inc($key, $step = 1);
    public function dec($key, $step = 1);
}
