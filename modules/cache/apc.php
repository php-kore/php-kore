<?php

class kore_cache_apc extends kore_cache_common
{
    public function exists($key)
    {
        $bench = kore::$debug->benchInit(__CLASS__, __FUNCTION__.'() '.$key);

        if ( function_exists('apc_exists') )
            return apc_exists($key);
        else
            return (apc_fetch($key) !== false);
    }

    public function get($key)
    {
        $bench = kore::$debug->benchInit(__CLASS__, __FUNCTION__.'() '.$key);

        $res = apc_fetch($key);
        if( $res === false )
            $bench->setFinalStatus('not found');
        return $res;
    }

    public function multiGet(array $keys)
    {
        $bench = kore::$debug->benchInit(__CLASS__, __FUNCTION__.'() ');

        return apc_fetch($keys);
    }

    public function set($key, $value, $ttl = null)
    {
        $bench = kore::$debug->benchInit(__CLASS__, __FUNCTION__.'() '.$key);
        if( $ttl === null ) $ttl = kore::$conf->cache_defaultTTL;

        return apc_store($key, $value, $ttl);
    }

    public function multiSet(array $values, $ttl = null)
    {
        $bench = kore::$debug->benchInit(__CLASS__, __FUNCTION__.'() ');
        if( $ttl === null ) $ttl = kore::$conf->cache_defaultTTL;

        return apc_store($values, null, $ttl);
    }

    public function add($key, $value, $ttl = null)
    {
        $bench = kore::$debug->benchInit(__CLASS__, __FUNCTION__.'() '.$key);
        if( $ttl === null ) $ttl = kore::$conf->cache_defaultTTL;

        return apc_add($key, $value, $ttl);
    }

    public function delete($key)
    {
        $bench = kore::$debug->benchInit(__CLASS__, __FUNCTION__.'() '.$key);

        return apc_delete($key);
    }

    public function deleteAll()
    {
        $bench = kore::$debug->benchInit(__CLASS__, __FUNCTION__.'() '.$key);

        return apc_clear_cache('user');
    }

    public function inc($key, $step = 1)
    {
        $bench = kore::$debug->benchInit(__CLASS__, __FUNCTION__.'() '.$key);

        return apc_inc($key, $step);
    }

    public function dec($key, $step = 1)
    {
        $bench = kore::$debug->benchInit(__CLASS__, __FUNCTION__.'() '.$key);

        return apc_dec($key, $step);
    }
}
