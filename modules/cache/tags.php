<?php
class kore_cache_tags extends kore_cache_common
{
    public $_backend;
    protected $_keyPrefix = 'w/Tags::';
    protected $_tagPrefix = 'tag::';
    protected $_currentTags = array();

    public function __construct( $backend = null )
    {
        parent::__construct();

        kore::$conf->get('cache_tagsTTL', 864000);

        if( $backend )
            $this->setBackend($backend);
        elseif( kore::$conf->cache_tagsDefaultBackend )
            $this->setBackend(kore::$conf->cache_tagsDefaultBackend);
    }

    public function setBackend( $backend )
    {
        if( $backend instanceof kore_cache_common )
            $this->_backend = $backend;
        else
            $this->_backend = new kore_deferInclude(
                    array($this, '_backend'), $backend, 'kore_cache_common');
    }

    protected function _getTagsVersion($tags)
    {
        $toSearch = $results = array();
        foreach( $tags as $tagName ){
            if( isset($this->_currentTags[$tagName]) )
                $results[$tagName] = $this->_currentTags[$tagName];
            else
                $toSearch[] = $this->_tagPrefix . $tagName;
        }
        if( $toSearch ){
            $toSet = array();
            $list = $this->_backend->multiGet($toSearch);

            $prefixLen = strlen($this->_tagPrefix);

            foreach( $list as $tagName => $version ){
                $ourTagName = substr($tagName, $prefixLen);
                if( !$version ){
                    $version = kore::requestTime().'/'.kore::getUniqPid();
                    $toSet[$tagName] = $version;
                }

                $this->_currentTags[$ourTagName] = $version;
                $results[$ourTagName] = $version;
            }

            if( $toSet )
                $this->_backend->multiSet($toSet, kore::$conf->cache_tagsTTL);
        }

        return $results;
    }

    protected function _changeTagsVersion($tags)
    {
        $toSet = array();
        $newVersion = kore::requestTime().'/'.kore::getUniqPid();
        foreach( $tags as $tagName ){
            $this->_currentTags[$tagName] = $newVersion;
            $toSet[$this->_tagPrefix . $tagName] = $newVersion;
        }
        return $this->_backend->multiSet($toSet, kore::$conf->cache_tagsTTL);
    }

    public function get($key)
    {
        $internalKey = $this->_keyPrefix.$key;
        $data = $this->_backend->get($internalKey);
        if( $data === false )
            return false;
        list($data, $tags) = $data;

        $t = $this->_getTagsVersion(array_keys($tags));
        foreach( $t as $tagName => $version )
            if( $version != $tags[$tagName] )
                return false;
        return $data;
    }

    public function set($key, $value, $ttl = null, array $tags = array())
    {
        if( !$tags )
            $t = $tags;
        else
            $t = $this->_getTagsVersion($tags);

        $value = array($value, $t);

        $internalKey = $this->_keyPrefix.$key;
        return $this->_backend->set($internalKey, $value, $ttl);
    }

    public function deleteTags()
    {
        $tags = func_get_args();
        return $this->_changeTagsVersion($tags);
    }

    public function delete($key)
    {
        $internalKey = $this->_keyPrefix.$key;
        return $this->_backend->delete($internalKey);
    }

    public function deleteAll()
    {
        return $this->_backend->deleteAll();
    }

    public function multiSet(array $values, $ttl = null, array $tags = array())
    {
        $results = array();
        foreach( $values as $key => $value )
            $results[$key] = $this->set($key, $value, $ttl, $tags);
        return $results;
    }

    public function add($key, $value, $ttl = null, array $tags = array())
    {
        if( $this->exists($key) )
            return null;
        return $this->set($key, $value, $ttl, $tags);
    }

    public function getExpired($key)
    {
        $internalKey = $this->_keyPrefix.$key;
        $data = $this->_backend->get($internalKey);
        if( $data !== false ){
            list($data, $tags) = $data;
            return $data;
        }

        return $this->get($this->_getExpiredKey($key));
    }

    public function markAsExpired($key, $ttl = 600)
    {
        $newKey = $this->_getExpiredKey($key);
        if( ( $data = $this->get($key) ) === false )
            return false;

        $this->set($newKey, $data, $ttl);
        return $this->delete($key);
    }

    public function inc($key, $step = 1)
    {
        $internalKey = $this->_keyPrefix.$key;
        return $this->_backend->inc($internalKey, $step);
    }

    public function dec($key, $step = 1)
    {
        $internalKey = $this->_keyPrefix.$key;
        return $this->_backend->inc($internalKey, $step);
    }

}
