<?php
/**
 * Fichier d'exemple d'instanciation de kore_db_config
 * (utilisé par kore::$db ).
 *
 * @package db
 */

/**
 * Création d'un objet permettant d'accèder à une base de données.
 *
 * @package db
 */
class dbConfig extends kore_db_config
{

    /**
     * Ouvre une connexion SQL selon le profil $dbProfil.
     *
     * @param string $dbProfil
     * @return kore_db_pdo	(facultatif)
     */
	public function newInstance( $profilName )
    {
        $host = 'localhost';
        $user = $profilName;
        $password = NULL;
        $port = 3306;
        $base = $profilName;

        try {
            switch ($profilName){
            case 'example':
                $instance = new kore_db_pdo(
                        "mysql:dbname=$base;host=$host", $user, 'XXXX' );
                break;
            default:
                throw new DomainException("dbConfig : undefined profile [$profilName]");
            }

            $instance->identifier = $profilName ;
            $instance->setAttribute( PDO::ATTR_EMULATE_PREPARES, true );

        } catch (PDOException $e) {
            $error = kore::$error->instanciateErrorFromException($e);
            $error->setMessage("[$profilName] : (connection) " . $error->getMessage());
            $error->track(E_USER_WARNING);

            $instance = NULL;
        }

        return $instance;
    }
}
