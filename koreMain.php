<?php
/**
 * Kore Framework.
 *
 * Script principal du framework : mise en place du contexte d'exécution
 * général.
 *
 * @package Main
 *
 * @global boolean kore::$conf->sapiName
 *      indique si le script est lancé via CLI, CGI, etc.
 * @global string  kore::$conf->include_globalPath
 *      dossier d'inclusion des classes.
 * @global string  kore::$conf->include_XX_path
 *      dossier d'inclusion des classes de la branche XX.
 * @global string  kore::$conf->errorReporting_visibleClass
 *      classe utilisée pour reporter les erreurs visibles (à l'écran).
 * @global string  kore::$conf->errorReporting_invisibleClass
 *      classe utilisée pour reporter les erreurs invisibles (par mail).
 * @global string  kore::$conf->storageBaseName
 *      nom du dossier à utiliser pour le stockage, si kore_searchStorage() est
 *      utilisée.
 * @global string  kore::$conf->storagePath
 *      dossier où les fichiers seront stockés.
 * @global integer kore::$conf->storageDirMod
 *      droits utilisés pour la création des dossiers.
 *
 */


/**
 * Contient les objets et méthodes standard d'utilisation du framework.
 *
 * Note : cette classe n'est pas instanciable.
 *
 * @package main
 */
class kore
{
    /**
     * Permet la gestion des erreurs.
     *
     * @see kore_error
     * @var kore_errorHandler
     */
    public static $error;

    /**
     * Accès à la configuration du framework.
     *
     * @see kore_conf
     * @var kore_conf
     */
    public static $conf;

    /**
     * Objet dédié au chargement des divers classes. Utilisé de manière
     * transparante grâce à "spl_autoload()".
     *
     * @see kore_loader
     * @var kore_loader
     */
    public static $loader;

    /**
     * Gère l'accès aux variables de session.
     *
     * @see kore_session
     * @var kore_session
     */
    public static $session;

    /**
     * Fourni une instance d'accès à une base de données
     *
     * @see kore_db_config
     * @var kore_db_config
     */
    public static $db;

    /**
     * Fourni un accès à quelques fonctions de débugage
     *
     * @see kore_debug
     * @var kore_debug
     */
    public static $debug;

    /**
     * Fourni un accès unifié à une gestion de caches.
     *
     * @see kore_cache
     * @var kore_cache
     */
    public static $cache;

    /**
     * Fourni un moyen d'aiguiller les URL vers des méthodes dédiées
     *
     * @see kore_router
     * @var kore_router
     */
    public static $router;

    /**
     * Racine du site web.
     * @var string
     */
    private static $_webDir;

    /**
     * Heure de démarrage du framework
     *
     * @see kore::timeStart()
     * @var string
     */
    private static $_timeStart;


    /**
     * Empêche toute instanciation de la classe.
     */
    private function __construct()
    {
    }

    /**
     * Initialise la classe. Inutile de l'appeler dans vos scripts, celle ci
     * étant déjà appelée depuis koreMain.php
     */
    public static function init()
    {
        /*
         * Initialize start time from now, or from REQUEST_TIME_FLOAT if
         * provided (starting by PHP 5.4).
         */
        if (isset($_SERVER['REQUEST_TIME_FLOAT']))
            self::$_timeStart = $_SERVER['REQUEST_TIME_FLOAT'];
        else
            self::$_timeStart = self::time();

        /*
         * Fait en sorte que $_SERVER['REQUEST_TIME'] soit toujours renseigné.
         */
        if (!isset($_SERVER['REQUEST_TIME']))
            $_SERVER['REQUEST_TIME'] = floor(self::$_timeStart);

        self::$conf      = new kore_conf();
        self::$debug     = new kore_debugLight();
        self::$loader    = new kore_loader();

        self::$conf->bindWithIni('error_scream', 'xdebug.scream');
        self::setErrorHandler(new kore_errorHandler());


        /*
         * Pour des raisons de performance, recopie le contenu de getKoreDir()
         * dans une variable de configuration.
         */
        kore::$conf->korePath = kore::getKoreDir();

        kore::$conf->include_korePath = kore::$conf->korePath.'modules/';
        kore::$conf->include_koreTestsPath = kore::$conf->korePath.'tests/';

        /*
         * Utilisation de spl_autoload_register() au lieu de __autoload() afin
         * de conserver le bon fonctionnement du framework dans le cas où il
         * serait utilisé depuis CLI.
         */
        spl_autoload_register(array(self::$loader, 'autoload'));

        self::setDBLoader('kore_db_config');
        self::$session   = new kore_sessionLoader('session');
        self::$cache     = new kore_cache();
        self::$router    = new kore_deferInclude('router', 'kore_route_router');


        /*
         * Detecte le mode d'exécution de PHP. Principalement utilisé pour
         * détecter le lancement via CLI, CGI ou module Apache qui diffèrent sur
         * quelques points.
         */
        $sapi = substr(php_sapi_name(), 0, 3);
        self::$conf->sapiName = $sapi;


        /*
         * Détection de l'environnement de fonctionnement.
         */
        if (isset($_ENV['KORE_ENV']))
            self::$debug->setEnvironment($_ENV['KORE_ENV']);
        elseif (isset($_SERVER['KORE_ENV']))
            self::$debug->setEnvironment($_SERVER['KORE_ENV']);


        /*
         * Pour compatibilité avec PHP 5.4, on force magic_quotes_gpc à "off".
         *
         * Note : il est préférable de désactiver directement ce paramètre dans
         * la configuration de PHP afin d'éviter ce traitement.
         */
        if (ini_get('magic_quotes_gpc'))
            self::disableMagicQuotesGPC();


        /*
         * Configuration du stockage, utilisé entre autre par les classes
         * kore_cache* et kore_template*.
         *
         * La méthode kore::searchStorage() peut être utilisée afin de tenter de
         * détecter automatiquement un chemin de stockage.
         *
         * Important : la variable storagePath _doit_ finir par un slash.
         *
         * Note concernant storageDirMod : l'umask courant affecte les droits
         * qui seront réellement appliqués au dossier. Ainsi avec un umask 022
         * (par défaut sous de nombreux systèmes) même en mettant 0777 le
         * résultat sera 0755.
         */
        self::$conf->storageDirMod   = 0750;
        self::$conf->storagePath     = '/tmp/';


        /*
         * Par défaut, affichage en text/html UTF-8
         */
        self::$conf->bindWithIni('response_charset', 'default_charset');
        self::$conf->response_charset = 'UTF-8';

        /*
         * Le contentType n'est pas synchronisé avec son équivalent "php.ini"
         * afin de permettre une configuration différente en environnement de
         * développement.
         * En particulier le mode text/plain facilite beaucoup le débugage.
         */
        if (self::$debug->getEnvironment() === 'dev')
            ini_set('default_mimetype', 'text/plain');
        self::$conf->response_contentType = 'text/html';
    }

    /**
     * Remplace les gestionnaires d'erreur de PHP par celui de Kore, ou bien un
     * autre qui serait compatible.
     *
     * @param  kore_errorHandler $handler
     * @return kore_errorHandler
     */
    public static function setErrorHandler(kore_errorHandler $handler)
    {
        $oldHandler = self::$error;

        self::$error = $handler;
        set_error_handler(array($handler, 'catchPHPError'));
        set_exception_handler(array($handler, 'catchUncaughtException'));

        return $oldHandler;
    }

    /**
     * Remplace le gestionnaire de connexion aux bases de données par une autre
     * version.
     *
     * @param string/kore_db_config $class
     */
    public static function setDBLoader($class)
    {
        if (is_string($class))
            kore::$db = new kore_dbLoader('db', $class);
        else
            kore::$db = $class;
    }

    /**
     * Renvoi l'heure de démarrage du framework, sous forme de chaine contenant
     * le timestamp unix suivi des microsecondes (utilisable avec la fonction
     * bcsub() par exemple, pour ne pas perdre la précision).
     *
     * @return string
     */
    public static function timeStart()
    {
        return self::$_timeStart;
    }

    /**
     * Renvoi l'heure courante, sous forme de chaine contenant le timestamp unix
     * suivi des microsecondes.
     *
     * Cette fonction n'est fournie qu'à des fins de benchmarking : en effet
     * elle est plus couteuse qu'un simple appel à microtime(true) mais elle
     * retourne un résultat beaucoup plus précis, qui doit ensuite être manipulé
     * avec bcsub().
     *
     * @return string
     */
    public static function time()
    {
        $tmp = explode(' ', microtime());
        return $tmp[1] . substr($tmp[0], 1);
    }

    /**
     * Obtient le timestamp courant.
     * Celui ci ne sera pas mis à jour durant l'exécution du script, mais évite
     * un appel système inutile si on a pas besoin d'un résultat très précis.
     */
    public static function requestTime()
    {
        return $_SERVER['REQUEST_TIME'];
    }

    /**
     * Retourne le dossier "racine" du site web. Il est principalement utilisé
     * par le moteur de template.
     *
     * Cette fonction se base sur la variable $_SERVER['DOCUMENT_ROOT'] qui peut
     * parfois être erronée (en cas d'utilisation de la directive Apache
     * "VirtualDocumentRoot" par exemple). Dans ce cas il est nécessaire de
     * renseigner cette valeur dans votre configuration grâce à la méthode
     * kore::setWebDir().
     *
     * Note : ce dossier se terminera toujours par un slash.
     *
     * @return string
     */
    public static function getWebDir()
    {
        if ( self::$_webDir === NULL ) {
            if (!isset($_SERVER['DOCUMENT_ROOT'])){
                self::$_webDir = './';
            } else {
                self::$_webDir = $_SERVER['DOCUMENT_ROOT'] ;
                if (substr(self::$_webDir, -1) !== '/')
                    self::$_webDir .= '/';
            }
        }
        return self::$_webDir ;
    }

    /**
     * Change la valeur du dossier "racine" du site web.
     *
     * Note : un slash sera ajouté à la fin de la chaine s'il n'est pas déjà
     * présent.
     *
     * @param string $dir
     */
    public static function setWebDir( $dir )
    {
        if ( substr($dir, -1) !== '/' )
            $dir .= '/';
        self::$_webDir = $dir;
    }

    /**
     * Retourne le dossier où est stocké le framework ; principalement utilisé
     * afin de permettre le chargement des différentes classes du framework.
     *
     * Note : ce dossier se terminera toujours par un slash.
     *
     * @return string
     */
    public static function getKoreDir()
    {
        return __DIR__ . '/';
    }

    /**
     * Retourne le dossier où est stocké le script courant.
     *
     * Note : ce dossier se terminera toujours par un slash.
     *
     * @return string
     */
    public static function getScriptDir()
    {
        static $dir = NULL;

        if ( $dir === NULL ) {
            if ( kore::$conf->sapiName !== 'cli' )
                $calledFile = @ $_SERVER['SCRIPT_FILENAME'];
            else {
                $calledFile  = @ $_SERVER['argv'][0];
                if ( ( $tmp = realpath($calledFile) ) !== false )
                    $calledFile = $tmp;
            }

            $dir = dirname($calledFile) . '/' ;
        }

        return $dir;
    }

    /**
     * Retourne le chemin relatif du script courant par rapport au dossier
     * "WebDir". Principalement utilisé par le moteur de template.
     *
     * Note : ce dossier se terminera toujours par un slash.
     *
     * @return string
     */
    public static function getRelativeScriptDir()
    {
        static $dir = NULL;

        if ($dir === NULL) {
            $dir = self::getScriptDir();
            $webDir = self::getWebDir();

            /**
             * Si le script courant ne se trouve pas dans le dossier "web" ;
             * on conserve le chemin complet.
             */
            $len = strlen($webDir);
            if (substr($dir, 0, $len) === $webDir)
                $dir = (string)substr($dir, $len);
        }

        return $dir;
    }

    /**
     * Retourne le nom de la machine courante.
     *
     * @return string
     */
    public static function getHostname()
    {
        static $name = NULL;
        if ( $name === NULL ) $name = php_uname('n');

        return $name;
    }

    /**
     * Recherche un dossier de stockage approprié pour le stockage des fichiers.
     */
    public static function searchStorage()
    {
        $dirsToCheck = array(
            ini_get('upload_tmp_dir'),
            '/var/lib/php5/',
            ini_get('session.save_path'),
            '/tmp/',
            kore::$conf->korePath.'cacheStorage',
            );

        $found = false;

        $storageName = self::$conf->get('storageBaseName', 'kore');

        foreach ( $dirsToCheck as $dir ) {
            if ( empty($dir) ) continue;
            if ( $dir{strlen($dir)-1} !== '/' ) $dir .= '/';
            $dir .=  $storageName.'/' ;

            if ( @ is_dir($dir) ) {
                if ( ! @ is_writeable($dir) )
                    continue;
            }
            elseif ( ! @ mkdir($dir, kore::$conf->storageDirMod) )
                continue;

            $found = true;
            if( kore::$debug->isEnabled() )
                kore::$debug->message('Choosed for storage : '.$dir);

            kore::$conf->storagePath = $dir;
            break;
        }

        if (!$found) {
            kore::$conf->storagePath = '/tmp/';
            kore::$error->track('no valid "storagePath" found',
                    kore_error::SEVERITY_WARNING, $dirsToCheck);
        }

        return kore::$conf->storagePath;
    }

    public static function getStorePath($relativePath)
    {
        return kore::$conf->storagePath . $relativePath;
    }

    public static function store($relativePath, $data, $tmpPrefix = 'kore')
    {
        /*
         * Here, we don't want that errors hidden by the at sign (@) are tracked
         * by the error handler, since they are expected ones.
         */
        $tracking = kore::$conf->error_trackNonReportedErrors;
        kore::$conf->error_trackNonReportedErrors = false;

        $tmpFile = kore::$conf->storagePath."tmp/$tmpPrefix".kore::getUniqPid();
        if (!@file_put_contents($tmpFile, $data)){
            @ mkdir(kore::$conf->storagePath . 'tmp',
                    kore::$conf->storageDirMod, true);

            if (!file_put_contents($tmpFile, $data)){
                /*
                 * Restore error tracking setup.
                 */
                kore::$conf->error_track = $tracking;
                return false;
            }
        }

        if( kore::$conf->storageFileMod )
            chmod($tmpFile, kore::$conf->storageFileMod);
        if( kore::$conf->storageFileGroup )
            chgrp($tmpFile, kore::$conf->storageFileGroup);

        $fullPath = kore::$conf->storagePath . $relativePath;

        if (@rename($tmpFile, $fullPath))
            $result = true;
        elseif( ( $pos = strrpos($fullPath, '/') ) === false )
            $result = false;
        else {
            @ mkdir(substr($fullPath, 0, $pos), kore::$conf->storageDirMod,
                    true);
            $result = rename($tmpFile, $fullPath);
        }

        /*
         * Restore error tracking setup.
         */
        kore::$conf->error_trackNonReportedErrors = $tracking;
        return $result;
    }

    /**
     * Retire tous les "slashes" que la directive "magic_quotes_gpc" a ajouté
     * sur les variables d'entrée.
     */
    private static function disableMagicQuotesGPC()
    {
        if( kore::$debug->isEnabled() )
            kore::$debug->message('Disabling magic_quotes_gpc');

        self::gpcStripWalk($_GET);
        self::gpcStripWalk($_POST);
        self::gpcStripWalk($_COOKIE);
        self::gpcStripWalk($_REQUEST);
    }

    /**
     * Parcours le tableau $array de manière récursive et applique la fonction
     * stripslashes sur toutes les clés et valeurs.
     *
     * @param array $array
     */
    private static function gpcStripWalk( & $array )
    {
        foreach ( $array as $key => $item ) {
            $newKey = stripslashes($key);
            if ( $newKey !== $key ) {
                unset($array[$key]);
                $array[$newKey] = $item;
            }

            if ( is_array($item) === true ) {
                self::gpcStripWalk($array[$newKey]);
            } else {
                $array[$newKey] = stripslashes($item);
            }
        }
    }

    /**
     * Retourne un identifiant unique au processus courant sur la machine
     * courante, afin d'éviter simplement des problèmes d'accès concurrents.
     *
     * @return string
     */
    public static function getUniqPid()
    {
        static $upid;

        if( $upid === null ){
            $upid = getmypid();
            if( function_exists('zend_thread_id') )
                $upid .= '_' . zend_thread_id();
        }

        return $upid;
    }
}


/**
 * Classe minimale se chargeant de transférer les appels à une autre classe au
 * moment de l'initialisation, ce qui permet de charger la classe en question
 * "à la volée" de manière transparente.
 *
 * Pour éviter que les appels à certaines méthodes provoque le chargement, il
 * suffit de surcharger cette classe et de définir les méthodes à ignorer.
 *
 * @package main
 */
class kore_deferInclude
{
    protected $_deferedInstance;
    protected $_deferedClass;
    protected $_deferedVerifyClass;

    public function __construct( $instanceName, $className = NULL, $verifyClass = NULL )
    {
        $this->_deferedInstance = $instanceName;
        $this->_deferedClass = $className;
        $this->_deferedVerifyClass = $verifyClass;
    }

    protected function _replaceDeferInclude()
    {
        $obj = new $this->_deferedClass;

        if ($this->_deferedVerifyClass and
                !$obj instanceOf $this->_deferedVerifyClass){

            if( !is_array($this->_deferedInstance) ) {
                $class = 'kore';
                $property = $this->_deferedInstance;
            } else {
                list($class, $property) = $this->_deferedInstance;
                $class = get_class($class);
            }

            throw new DomainException("$class::\$$property must be an instance of {$this->_deferedVerifyClass}, {$this->_deferedClass} given");
        }

        if( !is_array($this->_deferedInstance) )
            kore::${$this->_deferedInstance} = $obj;
        else {
            list($ctxt, $property) = $this->_deferedInstance;
            $ctxt->$property = $obj;

        }

        return $obj;
    }

    public function __call( $name, $args )
    {
        $obj = $this->_replaceDeferInclude();
        return call_user_func_array(array($obj, $name), $args);
    }

    public function __isset( $name )
    {
        $obj = $this->_replaceDeferInclude();
        return isset($obj->$name);
    }

    public function __unset( $name )
    {
        $obj = $this->_replaceDeferInclude();
        unset($obj->$name);
    }

    public function __set( $name, $value )
    {
        $obj = $this->_replaceDeferInclude();
        $obj->$name = $value;
    }

    public function __get( $name )
    {
        $obj = $this->_replaceDeferInclude();
        return $obj->$name;
    }
}


/**
 * Gère l'accès aux différents paramètres de configuration du framework.
 * La classe est instanciée par "kore".
 *
 * @see kore
 * @package conf
 */
class kore_conf
{
    /**
     * Contient les associations avec la configuration de PHP.
     *
     * @var array
     */
    protected $_iniBind = array();

    /**
     * Récupère la valeur courante d'un paramètre de configuration identifié par
     * $key. Si celui ci n'est pas défini, alors $defaultValue sera
     * retournée (et enregistrée comme valeur par défaut pour les futurs accès).
     *
     * Pour des raisons de performances la valeur est enregistrée directement
     * comme propriété de kore_conf. Si vous n'utilisez pas le paramètre
     * $defaultValue il est préférable d'utiliser l'accès direct de la forme
     * kore::$conf->nomParametre .
     *
     * @param string $key
     * @param mixed $defaultValue
     * @return mixed
     */
    public function get( $key, $defaultValue )
    {
        if ( isset($this->$key) )
            return $this->$key ;
        elseif( isset($this->_iniBind[$key]) )
            return ini_get($this->_iniBind[$key]);
        else {
            $this->$key = $defaultValue;
            return $defaultValue;
        }
    }

    /**
     * Permet un accès direct aux variables de configuration via la syntaxe
     * kore::$conf->nomDuParametre ; qui est environ 2 fois plus rapide que de
     * passer par la méthode kore_conf::get(), mais ne permet pas l'utilisation
     * d'une valeur par défaut.
     *
     * @see kore_conf::get()
     * @param string $key
     * @return mixed
     */
    public function __get( $key )
    {
        /**
         * Si PHP utilise __get() c'est que la propriété n'existe pas ; donc pas
         * besoin de faire de test supplémentaire.
         */

        if( isset($this->_iniBind[$key]) )
            return ini_get($this->_iniBind[$key]);

        $this->$key = NULL;
        return NULL;
    }

    /**
     * Change la valeur d'un paramètre de configuration identifié par $key.
     *
     * La fonction retourne l'ancienne valeur.
     *
     * @param string $key
     * @param mixed $newValue
     * @return mixed
     */
    public function set( $key, $newValue )
    {
        if( isset($this->_iniBind[$key]) )
            $previousValue = ini_set($this->_iniBind[$key], $newValue);
        else {
            if( isset($this->$key) )
                $previousValue = $this->$key;
            else $previousValue = NULL;

            $this->$key = $newValue;
        }

        return $previousValue;
    }

    /**
     * Permet un accès direct à la méthode set() via la syntaxe
     * kore::$conf->nomDuParametre .
     *
     * @see kore_conf::set()
     * @param string $key
     * @param mixed $newValue
     */
    public function __set( $key, $newValue )
    {
        if( isset($this->_iniBind[$key]) )
            ini_set($this->_iniBind[$key], $newValue);
        else $this->$key = $newValue;
    }


    /**
     * Prend en charge le fonctionnement de isset().
     *
     * @param  string  $key
     * @return boolean
     */
    public function __isset( $key )
    {
        return ( isset($this->$key) or isset($this->_iniBind[$key]) );
    }


    /**
     * Associe un paramètre de configuration Kore à une variable de la
     * configuration de PHP.
     *
     * @param string  $confKey
     * @param string  $iniKey
     */
    public function bindWithIni( $confKey, $iniKey )
    {
        $this->_iniBind[$confKey] = $iniKey;

        if( isset($this->$confKey) ) {
            ini_set($iniKey, $this->$confKey);
            unset($this->$confKey);
        }
    }

    /**
     * Retourne un tableau contenant tous les paramètres actuellement
     * renseignés, y compris les variables associées à la configuration de PHP.
     */
    public function dump()
    {
        $dump = array();
        foreach( $this as $key => $value ) {
            if( $key === '_iniBind' ) continue;
            $dump[$key] = $value;
        }
        foreach( $this->_iniBind as $key => $iniKey ){
            $dump[$key] = $iniKey;
        }
        ksort($dump);

        return $dump;
    }

}




/**
 * Gère le chargement des différentes classes.
 * La classe est instanciée par "kore".
 *
 * @see kore
 * @package main
 */
class kore_loader
{
    /**
     * Liste des classes actuellement chargées.
     *
     * @var array
     */
    protected $_list = array();

    /**
     * Vérifie que la classe $className soit chargée ou non.
     *
     * @param  string  $className
     * @return boolean
     */
    public function isLoaded( $className )
    {
        return isset($this->_list[$className]);
    }

    /**
     * Vérifie la présence de la classe $className sur le disque.
     *
     * @param  string  $className
     * @return boolean
     */
    public function exists( $className )
    {
        return is_file($this->getFileName($className));
    }

    /**
     * Retourne le chemin d'accès à une branche spécifique.
     *
     * Si la branche est "kore", alors le chemin sera complété par
     * {@link kore::getKoreDir()}.
     *
     * Sinon le chemin sera complété par le paramètre de configuration
     * 'include_{$branche}Path' si celui ci est renseigné, ou bien
     * 'include_globalPath' si ce n'est pas le cas (ce dernier est initialisé
     * par défaut à {@link kore::getKoreDir()}.
     *
     * @see kore_loader::getFileName()
     * @param  string  $branchName
     * @return string
     */
    public function getBranchPath( $branchName )
    {
        $confName = 'include_'.$branchName.'Path';
        if ( ( $thisBranchPath = kore::$conf->$confName ) !== NULL )
            return $thisBranchPath;

        if ( $branchName !== NULL )
            $branchName .= '/';

        if( ( $globalBranchPath = kore::$conf->include_globalPath ) === NULL )
            kore::$conf->include_globalPath = $globalBranchPath =
                    kore::$conf->include_korePath;

        return $globalBranchPath . $branchName;
    }


    /**
     * Retourne le chemin d'accès au module $className.
     *
     * Pour construire le chemin d'accès, chaque underscore sera remplacé par un
     * slash. La "branche" correspondra à la première partie de $className (le
     * premier dossier).
     *
     * @see kore_conf
     * @see kore_loader::getBranchPath()
     * @param  string  $className
     * @return string
     */
    protected function getFileName( $className )
    {
        $fileName = strtr($className, '_', '/');

        if ( ( $pos = strpos($fileName, '/') ) === false )
            $branch = NULL;
        else {
            $branch = substr($fileName, 0, $pos);
            $fileName = substr($fileName, $pos+1);
        }

        return $this->getBranchPath($branch) . $fileName . '.php';
    }

    /**
     * Charge la classe $className si celle ci ne l'était pas.
     *
     * @param  string  $className
     */
    public function load( $className )
    {
        if ( isset($this->_list[$className]) ) return;
        $this->_list[$className] = false;

        $this->autoload($className);
    }

    /**
     * Charge le module $className.
     *
     * @param string $className
     */
    public function autoload( $className )
    {
        if ( kore::$debug->isEnabled() ) {
            $comment = $className .' ::: '.
                       kore::$debug->benchGetCurrentCheckPoint('main');

            $bench = kore::$debug->benchInit('loading class', $comment);
        }

        include $this->getFileName($className);
        $this->_list[$className] = true;
    }

}


/**
 * Gestion des erreurs.
 * La classe est instanciée par "kore".
 *
 * @see kore::setErrorHandler
 * @package error
 */
class kore_errorHandler
{
    protected $_errorClass = 'kore_error';
    protected $_handlers;

    /**
     * Retourne une instance de kore_error
     *
     * @param  string $message
     * @param  integer $severity
     * @param  mixed $context
     * @param  string $file
     * @param  integer $line
     * @param  integer $code
     * @return kore_error
     */
    public function instanciateError($message, $severity = null, $context = null, $file = null, $line = null, $code = null)
    {
        $errorClass = $this->_errorClass;
        return new $errorClass($message, $severity, $context, $file, $line, $code);
    }

    /**
     * Retourne une instance de kore_error, via une Exception.
     *
     * @param Exception $e
     * @return kore_error
     */
    public function instanciateErrorFromException($e)
    {
        return call_user_func(array($this->_errorClass, 'fromException'), $e);
    }

    /**
     * Deprecated method, kept for compatibility for now.
     *
     * @param integer $severity
     * @param string $facility
     * @param string $message
     * @param mixed  $context
     */
    public function trigger($severity, $facility, $message, $context = null)
    {
        $this->track("$facility: $message", $severity, $context);
    }

    /**
     * Force le traçage d'une erreur.
     *
     * Note : si vous souhaitez déclencher une erreur fatale, préférez
     * l'utilisation d'exceptions.
     *
     * @param string  $message
     * @param integer $severity
     * @param mixed   $context
     * @param string  $file
     * @param integer $line
     */
    public function track($message, $severity = null, $context = null, $file = null, $line = null)
    {
        /*
         * Set the default severity
         */
        if ($severity === null)
            $severity = kore_error::SEVERITY_WARNING;

        /*
         * Convert the severity to a PHP error level.
         */
        if ($severity <= kore_error::SEVERITY_ERROR)
            $phpErrorLevel = E_USER_ERROR;
        elseif ($severity <= kore_error::SEVERITY_WARNING)
            $phpErrorLevel = E_USER_WARNING;
        else
            $phpErrorLevel = E_USER_NOTICE;

        $error = $this->instanciateError($message, $severity, $context, $file, $line);
        $this->trackError($error, $phpErrorLevel);
    }

    /**
     * Force le traçage d'une erreur, basée sur le contenu d'une exception.
     *
     * @param Exception $e
     * @param integer $phpErrorLevel
     */
    public function trackException(Exception $e, $phpErrorLevel = E_USER_NOTICE)
    {
        $error = $this->instanciateErrorFromException($e);
        $this->trackError($error, $phpErrorLevel);
    }

    /**
     * Force le traçage d'une erreur, basée sur le contenu d'une instance de
     * kore_error.
     *
     * @param kore_error $error
     * @param integer $phpErrorLevel
     */
    public function trackError(kore_error $error, $phpErrorLevel = E_USER_NOTICE)
    {
        $this->_handleError($error);

        /*
         * S'il s'agissait d'une erreur "fatale", s'assure que le traitement
         * soit bien interrompu.
         */
        if ($phpErrorLevel & (E_ERROR | E_USER_ERROR | E_RECOVERABLE_ERROR))
            exit(253);
    }


    /**
     * Traite les erreurs générées par PHP.
     *
     * @param int $errno
     * @param string $errstr
     * @param string $errfile
     * @param int $errline
     * @param array $errcontext
     * @return boolean
     */
    public function catchPHPError($errno, $errstr, $errfile = NULL, $errline = NULL, $errcontext = NULL)
    {
        /*
         * Convert the $errno (= PHP error level) to a severity constant.
         */
        switch ($errno) {
            case E_ERROR:
            case E_USER_ERROR:
                $severity = kore_error::SEVERITY_ERROR;
                break;
            case E_NOTICE:
            case E_USER_NOTICE:
                $severity = kore_error::SEVERITY_NOTICE;
                break;
            case E_STRICT:
            case E_DEPRECATED:
            case E_USER_DEPRECATED:
                $severity = kore_error::SEVERITY_INFO;
                break;
            case E_WARNING:
            case E_USER_WARNING:
            default:
                $severity = kore_error::SEVERITY_WARNING;
        }

        $error = $this->instanciateError($errstr, $severity, $errcontext, $errfile,
                $errline, $errno);
        $error->setOrigin(kore_error::ORIGIN_PHP);

        $result = $this->_handleError($error, false);

        /*
         * L'erreur n'a pas encore été traitée ? Alors laisse PHP la prendre en
         * charge.
         */
        if ($result === false)
            return false;

        /*
         * S'il s'agissait d'une erreur "fatale", s'assure que le traitement
         * soit bien interrompu.
         */
        $fatalError = (E_ERROR | E_USER_ERROR | E_RECOVERABLE_ERROR);
        if ( ($errno & $fatalError) or ($error->getCode() & $fatalError) )
            exit(253);
    }


    /**
     * Traite les exceptions non capturées.
     *
     * @param Exception $exception
     */
    public function catchUncaughtException( $e )
    {
        $error = $this->instanciateErrorFromException($e);
        $error->setOrigin(kore_error::ORIGIN_UNCAUGHT_EXCEPTION);
        $error->setMessage('Uncaught ' . $error->getMessage());

        $this->_handleError($error);

        /*
         * S'assure que le traitement soit bien interrompu;
         */
        exit(254);
    }

    /**
     * Traite une erreur et l'envoi aux différents gestionnaires.
     *
     * @param  kore_error $error
     * @param  boolean    $callOwnErrorHandler
     * @return boolean
     */
    protected function _handleError(kore_error $error, $callOwnErrorHandler = true)
    {
        if (kore::$conf->error_scream)
            error_reporting(E_ALL | E_STRICT);

        $reported = false;

        if (is_array($this->_handlers)){
            foreach ($this->_handlers as $handler){
                $done = call_user_func($handler, $error);

                if ($done) $reported = true;
            }
        }

        /*
         * Trace toutes les erreurs dans une propriété de l'objet.
         * Voir aussi : http://fr.php.net/manual/en/function.error-get-last.php
         */
        if (error_reporting() or
            kore::$conf->get('error_trackNonReportedErrors', true))
            $this->_lastError = $error;


        /*
         * Si l'erreur n'a pas encore été traitée, et sauf indication contraire
         * on lui demande de gérée elle même le signalement.
         */
        if (!$reported and error_reporting() and
            ($callOwnErrorHandler or !$error->usePHPDefaultReporting())) {

            $error->report();
            $reported = true;
        }

        return $reported;
    }

    /**
     * Vide le tampon contenant la dernière erreur.
     */
    public function clearLastError()
    {
        $this->_lastError = null;
    }

    /**
     * Retourne la dernière rencontrée.
     *
     * @return kore_error
     */
    public function getLastError()
    {
        $error = $this->_lastError;
        $this->clearLastError();

        return $error;
    }

    /**
     * Ajoute un gestionnaire d'erreur spécifique, et retourne son identifiant
     * interne.
     *
     * @param  callable $handler
     * @return integer
     */
    public function addHandler($handler)
    {
        if (!is_array($this->_handlers))
            $this->_handlers = array();
        $this->_handlers[] = $handler;

        end($this->_handlers);
        return key($this->_handlers);
    }

    /**
     * Supprime un gestionnaire d'erreur spécifique, grâce à son identifiant.
     *
     * @param  integer $idHandler
     * @return boolen
     */
    public function removeHandler($idHandler)
    {
        if (!is_array($this->_handlers) or !isset($this->_handlers[$idHandler]))
            return false;

        unset($this->_handlers[$idHandler]);
        return true;
    }

    /**
     * Replace the default kore_error class by an other one (to add more
     * verbose messages for example).
     *
     * @param string $className
     * @return string
     */
    public function setErrorClass($className)
    {
        $previous = $this->_errorClass;
        $this->_errorClass = $className;
        return $previous;
    }

}


/**
 * Classe minimale d'aide au débugage.
 *
 * @package debug
 */
class kore_debugLight
{
    protected $_environment = 'prod';

    protected $_checkpoint = NULL;

    protected $_debugging = false;

    public $benchClass = 'kore_benchLite';

    public function getEnvironment()
    {
        return $this->_environment;
    }

    public function setEnvironment( $env )
    {
        if (!in_array($env, array('prod', 'test', 'dev'))) {
            kore::$error->track("unknown environment [$env]");

        } elseif ( $this->_environment !== $env ) {

            if( $env === 'dev' ){
                kore_debug::replaceKoreHandler();
                kore::$debug->setEnvironment($env);
            } else {
                $this->_environment = $env;
            }
        }
    }

    public function enableDebugging()
    {
        kore_debug::replaceKoreHandler();
    }

    public function isEnabled()
    {
        return $this->_debugging ;
    }

    public function importCheckPoint( $checkPoint )
    {
        $this->_checkpoint = $checkPoint;
    }

    public function exportCheckPoint()
    {
        return $this->_checkpoint;
    }

    public function benchCheckPoint( $category, $name )
    {
        /**
         * A partir du deuxième appel si le debugging n'est pas activé on annule
         * le traitement. A noter qu'actuellement le premier appel est fait
         * directement depuis koreMain.
         */
        static $count = 0;
        if ( ++$count >= 2 and ( $this->_debugging === false ) )
            return;

        /**
         * Premier appel, initialise la structure en incluant les mesures de
         * départ.
         */
        if ( $this->_checkpoint === NULL ) {
            $this->_checkpoint = array('main' => array());

            if ( $this->canUseXdebug() === true ) {
                $event = new stdClass();
                $event->time = bcsub(kore::timeStart(), xdebug_time_index(), 6);
                $this->_checkpoint['main']['init (xdebug)'] = $event;
            }

            $event = new stdClass();
            $event->time = kore::timeStart();
            $this->_checkpoint['main']['kore'] = $event;
        }

        $event = new stdClass();
        $event->memory     = max( memory_get_usage(), memory_get_usage(true) );
        $event->memoryPeak = max( memory_get_peak_usage(),
                memory_get_peak_usage(true) );

        if ( !isset($this->_checkpoint[$category] ) )
            $this->_checkpoint[$category] = array($name => $event);
        else $this->_checkpoint[$category][$name] = $event;

        $event->time = kore::time();
    }

    public function message( $msg )
    {
        /**
         * Ne fait rien.
         */
    }

    /**
     * Initialise un objet de type kore_bench
     *
     * @param  string  $category
     * @param  string  $comments
     * @return kore_bench
     */
    public function benchInit( $category, $comments = '' )
    {
        return new $this->benchClass( $category, $comments );
    }

    protected function canUseXdebug()
    {
        static $use = NULL;
        if ($use === NULL)
            $use = function_exists('xdebug_time_index');
        return $use;
    }

    public function __destruct()
    {
        if( kore::$conf->trackSlowScript > 0 ) {
            $scriptTime = bcsub(kore::time(), kore::timeStart());

            if( $scriptTime > kore::$conf->trackSlowScript ){
                $scriptTime = round($scriptTime, 3);
                $context = array(
                    'execution time' => $scriptTime . 's',
                    'connection status' => connection_status() );

                if( $this->_debugging ) {
                    $context['debug messages'] = $this->getMessages();
                    $context['debug bench'] = $this->benchGet();
                }

                kore::$error->track("slow script [{$scriptTime}s])",
                        kore_error::SEVERITY_INFO, $context);
            }
        }
    }

}

/**
 * Classe de benchmark minimale.
 *
 * Il s'agit ici de la version utilisée en production, elle ne fait donc rien
 * mais est nécessaire pour assurer la compatibilité entre les environnements
 * prod, test et dev.
 */
class kore_benchLite
{
    public function __construct( $category, $comments = '' )
    {
    }

    public function setFinalStatus( $status )
    {
    }

    public function keep()
    {
    }
}


/**
 * Charge le module de gestion kore::$db à la volée.
 */
class kore_dbLoader extends kore_deferInclude
{
    public function isActive( $dbProfil )
    {
        return false;
    }

    public function close( $dbProfil )
    {
    }

    public function eventClosed($dbProfil)
    {
    }

    public function closeAll()
    {
    }

    public function instanceList()
    {
        return array();
    }
}

/**
 * Charge le module de gestion kore::$session à la volée.
 */
class kore_sessionLoader extends kore_deferInclude
{
    protected function _replaceDeferInclude()
    {
        $this->_deferedClass = kore::$conf->get('session_class','kore_session');
        return parent::_replaceDeferInclude();
    }

    public function close()
    {
    }

    /**
     * Indique que le module de session n'est pas chargé.
     *
     * @return boolean
     */
    public function isEnabled()
    {
        return false;
    }
}

/**
 *
 */
class kore_cache
{
    public function __set($idGroup, $cacheClass)
    {
        if( !is_object($cacheClass) ){
            $this->$idGroup = new kore_deferInclude(
                array($this, $idGroup), $cacheClass, 'kore_cache_common');
        } elseif( !($cacheClass instanceOf kore_cache_common) and
                  !($cacheClass instanceOf kore_deferInclude) ) {

            throw new DomainException("kore_cache::\$$idGroup should only store instance of kore_cache_common, ".get_class($cacheClass)." given");

        } else {
            $this->$idGroup = $cacheClass;
        }
    }

    public function addCache($idGroup, $cacheClass)
    {
        return $this->__set($idGroup, $cacheClass);
    }
}



/**
 * Initialise le framework.
 */
kore::init();


/**
 * Trace le changement de "zone", pour le débugage.
 */
kore::$debug->benchCheckPoint('main', 'launcher');
