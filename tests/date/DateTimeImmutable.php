<?php
class koreTests_date_DateTimeImmutable extends PHPUnit_Framework_TestCase
{
    public $timeZone;
    public $currentDate = "2012-12-27 16:24:08";
    public $originalTimezone;

    public function setUp()
    {
        $this->timeZone = new DateTimeZone("Asia/Tokyo");
        ini_set('date.timezone', 'Europe/London');

        parent::setUp();
    }

    public function tearDown()
    {
        kore::$conf->set('timezone', $this->originalTimezone);
    }

    protected function _dump($actual, $expected)
    {
        list($a, $b, $c) = $actual;
        list($d, $e, $f) = $expected;

        $this->assertSame($a->format('Y-m-d H:i:s e'), $d);
        $this->assertSame($b->format('Y-m-d H:i:s e'), $e);
        $this->assertSame($c->format('Y-m-d H:i:s e'), $f);
    }

    public function testModify()
    {
        $v = new kore_php_DateTimeImmutable($this->currentDate);
        $z = $v;
        $x = $z->modify("+2 days");
        $this->_dump(array($v, $z, $x), array(
                '2012-12-27 16:24:08 Europe/London',
                '2012-12-27 16:24:08 Europe/London',
                '2012-12-29 16:24:08 Europe/London'));

        $v = date_create($this->currentDate);
        $z = $v;
        $x = $z->modify("+2 days");
        $this->_dump(array($v, $z, $x), array(
                '2012-12-29 16:24:08 Europe/London',
                '2012-12-29 16:24:08 Europe/London',
                '2012-12-29 16:24:08 Europe/London',));
    }

    public function testAdd()
    {
        $v = new kore_php_DateTimeImmutable($this->currentDate);
        $z = $v;
        $x = $z->add(new DateInterval("P2DT2S"));
        $this->_dump(array($v, $z, $x), array(
                '2012-12-27 16:24:08 Europe/London',
                '2012-12-27 16:24:08 Europe/London',
                '2012-12-29 16:24:10 Europe/London'));

        $v = date_create($this->currentDate);
        $z = $v;
        $x = $z->add(new DateInterval("P2DT2S"));
        $this->_dump(array($v, $z, $x), array(
                '2012-12-29 16:24:10 Europe/London',
                '2012-12-29 16:24:10 Europe/London',
                '2012-12-29 16:24:10 Europe/London',));

    }

    public function testSub()
    {
        $v = new kore_php_DateTimeImmutable($this->currentDate);
        $z = $v;
        $x = $z->sub(new DateInterval("P2DT2S"));
        $this->_dump(array($v, $z, $x), array(
                '2012-12-27 16:24:08 Europe/London',
                '2012-12-27 16:24:08 Europe/London',
                '2012-12-25 16:24:06 Europe/London',
        ));

        $v = date_create($this->currentDate);
        $z = $v;
        $x = $z->sub(new DateInterval("P2DT2S"));
        $this->_dump(array($v, $z, $x),  array(
                '2012-12-25 16:24:06 Europe/London',
                '2012-12-25 16:24:06 Europe/London',
                '2012-12-25 16:24:06 Europe/London',));
    }

    public function testSetTimezone()
    {
        $v = new kore_php_DateTimeImmutable($this->currentDate);
        $z = $v;
        $x = $z->setTimezone($this->timeZone);
        $this->_dump(array($v, $z, $x), array(
                '2012-12-27 16:24:08 Europe/London',
                '2012-12-27 16:24:08 Europe/London',
                '2012-12-28 01:24:08 Asia/Tokyo'));

        $v = date_create($this->currentDate);
        $z = $v;
        $x = $z->setTimezone($this->timeZone);
        $this->_dump(array($v, $z, $x), array(
                '2012-12-28 01:24:08 Asia/Tokyo',
                '2012-12-28 01:24:08 Asia/Tokyo',
                '2012-12-28 01:24:08 Asia/Tokyo'));

        $v = new kore_php_DateTimeImmutable($this->currentDate);
        $z = $v;
        $x = $z->setTimezone($this->timeZone);
        $this->_dump(array($v, $z, $x), array(
                '2012-12-27 16:24:08 Europe/London',
                '2012-12-27 16:24:08 Europe/London',
                '2012-12-28 01:24:08 Asia/Tokyo'));
    }

    public function testSetTime()
    {
        $v = new kore_php_DateTimeImmutable($this->currentDate);
        $z = $v;
        $x = $z->setTime(5, 7, 19);
        $this->_dump(array($v, $z, $x), array(
                '2012-12-27 16:24:08 Europe/London',
                '2012-12-27 16:24:08 Europe/London',
                '2012-12-27 05:07:19 Europe/London'));

        $v = date_create($this->currentDate);
        $z = $v;
        $x = $z->setTime(5, 7, 19);
        $this->_dump(array($v, $z, $x), array(
                '2012-12-27 05:07:19 Europe/London',
                '2012-12-27 05:07:19 Europe/London',
                '2012-12-27 05:07:19 Europe/London'));
    }

    public function testSetDate()
    {
        $v = new kore_php_DateTimeImmutable($this->currentDate);
        $z = $v;
        $x = $z->setDate(5, 7, 19);
        $this->_dump(array($v, $z, $x), array(
                '2012-12-27 16:24:08 Europe/London',
                '2012-12-27 16:24:08 Europe/London',
                '0005-07-19 16:24:08 Europe/London'));

        $v = date_create($this->currentDate);
        $z = $v;
        $x = $z->setDate(5, 7, 19);
        $this->_dump(array($v, $z, $x), array(
                '0005-07-19 16:24:08 Europe/London',
                '0005-07-19 16:24:08 Europe/London',
                '0005-07-19 16:24:08 Europe/London'));
    }

    public function testSetISODate()
    {
        $v = new kore_php_DateTimeImmutable($this->currentDate);
        $z = $v;
        $x = $z->setIsoDate(2012, 2, 6);
        $this->_dump(array($v, $z, $x), array(
                '2012-12-27 16:24:08 Europe/London',
                '2012-12-27 16:24:08 Europe/London',
                '2012-01-14 16:24:08 Europe/London'));

        $v = date_create($this->currentDate);
        $z = $v;
        $x = $z->setIsoDate(2012, 2, 6);
        $this->_dump(array($v, $z, $x), array(
                '2012-01-14 16:24:08 Europe/London',
                '2012-01-14 16:24:08 Europe/London',
                '2012-01-14 16:24:08 Europe/London'));
    }

    public function testSetTimestamp()
    {
        $v = new kore_php_DateTimeImmutable($this->currentDate);
        $z = $v;
        $x = $z->setTimestamp(2012234222);
        $this->_dump(array($v, $z, $x), array(
                '2012-12-27 16:24:08 Europe/London',
                '2012-12-27 16:24:08 Europe/London',
                '2033-10-06 18:57:02 Europe/London'));

        $v = date_create($this->currentDate);
        $z = $v;
        $x = $z->setTimestamp(2012234222);
        $this->_dump(array($v, $z, $x), array(
                '2033-10-06 18:57:02 Europe/London',
                '2033-10-06 18:57:02 Europe/London',
                '2033-10-06 18:57:02 Europe/London'
        ));
    }

}