<?php
require __DIR__ . '/../koreMain.php';

/*
 * PHPUnit use ob_* to catch errors, so we need that PHP use STDOUT and not
 * STDERR.
 */
ini_set('display_errors', true);
ini_set('log_errors', false);
error_reporting(E_ALL);

kore::$debug->benchCheckPoint('main', 'unitTest');