<?php
class koreTests_date extends PHPUnit_Framework_TestCase
{
    static protected $_currentTimeZone;

    static public function setUpBeforeClass()
    {
        static::$_currentTimeZone = date_default_timezone_get();
    }

    static public function  tearDownAfterClass()
    {
        date_default_timezone_set(static::$_currentTimeZone);
    }

    /**
     * (dataProvider) provide a list of some timezones to check
     *
     * @return array
     */
    public function getSomeTimeZones()
    {
        return array(
                [ 'Pacific/Enderbury' ],       // UTC-12
                [ 'Pacific/Kiritimati' ],      // UTC-14

                [ 'Pacific/Niue' ],            // UTC+13
                [ 'Asia/Anadyr' ],             // UTC+12

                [ 'Europe/Paris' ],
                [ 'Europe/London' ],
                [ 'Europe/Monaco' ],
                [ 'Europe/Dublin' ],

                [ 'UTC' ],
        );
    }

    /**
     * (dataProvider) provide the list of all timezones handled by PHP
     *
     * @return array
     */
    public function getAllTimeZones()
    {
        $list = array();

        foreach (DateTimeZone::listIdentifiers() as $timeZone)
            $list[] = array($timeZone);
        return $list;
    }

    /**
     * (dataProvider) provide a whole list of days
     *
     * @return array
     */
    public function getAllDays()
    {
        $days = array();

        for ($year = 1970; $year <= 2037; $year++){
            $leapYear = (($year%4) === 0);

            for ($month = 1; $month <= 12; $month++){
                $lastDay = 31;

                if ($month === 2){
                    $lastDay = ($leapYear ? 29 : 28);

                } elseif ($month <= 7 and ($month % 2) === 0){
                    $lastDay = 30;

                } elseif ($month > 7 and ($month % 2) === 1) {
                    $lastDay = 30;
                }

                for ($day = 1; $day <= $lastDay; $day++){
                    $days[]= array($year, $month, $day);
                }
            }
        }

        return $days;
    }

    /**
     * (dataProvider) provide a list of days where there is a transition in at
     * least one timezone.
     *
     * @return array
     */
    public function getTransitionsDays()
    {
        $days = array();

        foreach ($this->getSomeTimeZones() as $timeZoneArray) {
            $tz = new DateTimeZone($timeZoneArray[0]);

            foreach ($tz->getTransitions(0) as $transition){
                $date = substr($transition['time'], 0, 10);
                $days[$date] = explode('-', $date);
            }
        }
        ksort($days);

        return $days;
    }

    /**
     * (dataProvider) provide a whole list of days
     *
     * @return array
     */
    public function getDaysAndTimeZones()
    {
        $list = array();

        $timezones = $this->getSomeTimeZones();
        $days = $this->getTransitionsDays();

        foreach ($days as $day){
            foreach ($timezones as $timeZoneArray) {
                $entry = $day;
                $entry[] = $timeZoneArray[0];
                $list[] = $entry;
            }
        }

        return $list;
    }

    /**
     * Check the fromMask() behavior with different timezones
     *
     * @param integer $year
     * @param integer $month
     * @param integer $day
     * @param string  $timezone
     *
     * @dataProvider getDaysAndTimeZones
     */
    public function testFromMask($year, $month, $day, $timezone)
    {
        date_default_timezone_set($timezone);

        $source = $year.'-'.str_pad($month, 2, '0', STR_PAD_LEFT).'-'.
                str_pad($day, 2, '0', STR_PAD_LEFT);

        $date = kore_date::fromMask($source, 'Y-M-D');
        $this->assertEquals($source . ' 00:00:00', $date->sqltime);

        $source .= ' 23:59:59';
        $date = kore_date::fromMask($source, 'Y-M-D H:I:S');
        $this->assertEquals($source, $date->sqltime);
    }

    /**
     * Check that the $days property is consistent between timezones : the same
     * date should have the same resulting $days, independtly of the timezone
     *
     * @param integer $year
     * @param integer $month
     * @param integer $day
     *
     * @dataProvider getTransitionsDays
     */
    public function testDays($year, $month, $day)
    {
        $wanted = null;

        foreach ($this->getSomeTimeZones() as $timeZoneArray){
            $timeZone = $timeZoneArray[0];
            date_default_timezone_set($timeZone);

            $date = kore_date::fromMask("$year-$month-$day", 'Y-M-D');

            if ($wanted === null)
                $wanted = $date->days;
            else
                $this->assertSame($date->days, $wanted, "days property error on $timeZone");
        }
    }

    /**
     * Check that the $days property have the expected value.
     * Note : this test is slow.
     *
     * @param string $timezone
     *
     * @dataProvider getSomeTimeZones
     */
    public function testDaysValue($timezone)
    {
        date_default_timezone_set($timezone);

        foreach ($this->getAllDays() as $idx => $arrayDate){
            list($year, $month, $day) = $arrayDate;

            $source = "$year-$month-$day";

            $date = kore_date::fromMask("$year-$month-$day", 'Y-M-D');
            $this->assertSame($date->days, $idx, "date $source should have \$days=$idx");
        }
    }

    /**
     * Check that the add function make the right calculate on date.
     */
    public function testAdd()
    {
        foreach ($this->getAllDays() as $idx => $arrayDate){
            list($year, $month, $day) = $arrayDate;

            $hour    = str_pad(rand(0, 23), 2, "0", STR_PAD_LEFT);
            $source  = "$year-$month-$day ".$hour.":00:00";

            $addHour = rand(1,100);

            $date    = kore_date::fromMask($source, 'Y-M-D H:I:S');

            $date    = $date->add(kore_date::hour, $addHour);

            $newDate = mktime($hour + $addHour, 0, 0, $month, $day, $year);

            $this->assertSame($date->timestamp, $newDate);
        }
    }

    /**
     * Check the fromDay() behavior with different timezones
     *
     * @param integer $year
     * @param integer $month
     * @param integer $day
     * @param string  $timezone
     *
     * @dataProvider getDaysAndTimeZones
     */
    public function testFromDay($year, $month, $day, $timezone)
    {
        date_default_timezone_set($timezone);

        $source = $year.'-'.str_pad($month, 2, '0', STR_PAD_LEFT).'-'.
                str_pad($day, 2, '0', STR_PAD_LEFT);

        $date = kore_date::fromMask($source, 'Y-M-D');
        $this->assertEquals($date->sql, $source, 'fromMask() error');

        $newDate = kore_date::fromDays($date->days);

        $this->assertEquals($date->sql, $newDate->sql,
                "dates differ after conversion in days ({$date->days}) on $timezone");
    }

    /**
     * Check the sqltime properties with different timezones, from SQL dates.
     *
     * @param integer $year
     * @param integer $month
     * @param integer $day
     * @param string  $timezone
     *
     * @dataProvider getDaysAndTimeZones
     */
    public function testSQLTimeFromSQL($year, $month, $day, $timezone)
    {
        date_default_timezone_set($timezone);

        $source = $year.'-'.str_pad($month, 2, '0', STR_PAD_LEFT).'-'.
                str_pad($day, 2, '0', STR_PAD_LEFT);

        $date = kore_date::fromSQL($source);
        $this->assertEquals($date->sqltime, $source . ' 00:00:00',
                'error on sqltime property');
    }

    /**
     * Check that format() and formatSimple() works on the expected timezone,
     * when the kore_date timezone is not the PHP timezone.
     *
     * @param integer $year
     * @param integer $month
     * @param integer $day
     * @param string  $timezone
     *
     * @dataProvider getDaysAndTimeZones
     */
    public function testFormatTimezone($year, $month, $day, $timezone)
    {
        $source = $year.'-'.str_pad($month, 2, '0', STR_PAD_LEFT).'-'.
                str_pad($day, 2, '0', STR_PAD_LEFT).' 12:34:56';

        $date = kore_date::fromDateTime(new DateTime($source,
                new DateTimeZone($timezone)));

        /*
         * In case of time-saving ranges, the source date is fixed by PHP, so we
         * need to use it as our reference.
         */
        $source = $date->datetime->format('Y-m-d H:i:s');

        foreach ($this->getSomeTimeZones() as $tz){
            date_default_timezone_set($tz[0]);

            $this->assertEquals(
                    $source,
                    $date->formatSimple('Y-m-d H:i:s'),
                    'formatSimple() with system in '.$tz[0]);

            $this->assertEquals(
                    $source,
                    $date->format('%Y-%m-%d %H:%M:%S'),
                    'format() with system in '.$tz[0]);
        }
    }
}